<?php 
    
    /**
     * TAKA+ v1.0
     * @package takaplus
     * @date 5.1.2016
     * @version 0.0.1
     * @website 
     *
     * FILE NÀY GIÚP NHẬN DIỆN TỰ ĐỘNG VỊ TRÍ
     * TƯƠNG ĐỐI CỦA TAKA+ TRONG THƯ MỤC HOSTING
     * 
     * BẠN CÓ THỂ THÊM CÁC THIẾT LẬP TÙY CHỌN NẾU CẦN Ở CUỐI FILE
    */
    
    /**
     * Thư mục gốc chứa bộ code
     *
     * CHỈ DÙNG CHO CÁC HÀM CỦA PHP
    */
    define( 'TP_ROOT', __DIR__ );
    
        
    /**
     * Tự động nhận diện tên miền
     * 
     * VIỆC NÀY GIÚP VIỆC CHUYỂN ĐỔI HOSTING
     * DỄ DÀNG VÀ CHÍNH XÁC HƠN
    */
    define( 'TP_DOMAIN', strtolower( substr( $_SERVER["SERVER_PROTOCOL"], 0, strpos( $_SERVER["SERVER_PROTOCOL"],'/'))) . '://' . $_SERVER['HTTP_HOST'] . '/' );
    
    /**
     * Viết các cấu hình của bạn bên dưới dòng này
    */

    
    /**
     * Kết thúc cấu hình taka+
     *
     * CODING OR DOING AN EXCEPTION ...
    */
?>