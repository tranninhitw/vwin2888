-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 23, 2019 at 02:36 PM
-- Server version: 5.7.26
-- PHP Version: 7.1.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vwin2888`
--

-- --------------------------------------------------------

--
-- Table structure for table `tp_admins`
--

CREATE TABLE IF NOT EXISTS `tp_admins` (
  `admin_id` int(11) NOT NULL,
  `admin_username` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_fullname` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_mobile` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_ips` text COLLATE utf8_unicode_ci,
  `admin_last_session` int(11) DEFAULT NULL,
  `admin_blocked` tinyint(1) DEFAULT '0',
  `admin_resetpass_string` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tp_admins`
--

INSERT INTO `tp_admins` (`admin_id`, `admin_username`, `admin_password`, `admin_fullname`, `admin_email`, `admin_mobile`, `admin_ips`, `admin_last_session`, `admin_blocked`, `admin_resetpass_string`) VALUES
(1, 'admin', '$2a$08$w.l.tIWPu9UHrEV0u2SfHu2HnzEZkme7vuZygACsIqTZHDxxcvVia', 'Tu Tran', 'txt95.kg@gmail.com', '0948223995', NULL, NULL, 0, '1fc50ef239cca0478b5df3e65e3fbc2c');

-- --------------------------------------------------------

--
-- Table structure for table `tp_advertises`
--

CREATE TABLE IF NOT EXISTS `tp_advertises` (
  `advertise_id` int(11) NOT NULL,
  `advertise_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `advertise_link` text COLLATE utf8_unicode_ci,
  `advertise_target` varchar(20) COLLATE utf8_unicode_ci DEFAULT '_self',
  `advertise_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `advertise_click_amount` int(11) DEFAULT '0',
  `advertise_start_time` int(10) unsigned DEFAULT NULL,
  `advertise_image` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tp_advertises`
--

INSERT INTO `tp_advertises` (`advertise_id`, `advertise_name`, `advertise_link`, `advertise_target`, `advertise_title`, `advertise_click_amount`, `advertise_start_time`, `advertise_image`) VALUES
(6, 'Google', 'http://google.com', '_blank', 'Nội dung khi rê vào', 0, 1566546917, '["Noidungkhirevao-1.jpeg"]');

-- --------------------------------------------------------

--
-- Table structure for table `tp_cates`
--

CREATE TABLE IF NOT EXISTS `tp_cates` (
  `cate_id` int(11) NOT NULL,
  `cate_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cate_parent` int(11) DEFAULT NULL,
  `cate_page_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cate_seo_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cate_meta_description` text COLLATE utf8_unicode_ci,
  `cate_meta_keywords` text COLLATE utf8_unicode_ci,
  `cate_is_serie` enum('yes','no') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tp_cates`
--

INSERT INTO `tp_cates` (`cate_id`, `cate_name`, `cate_parent`, `cate_page_title`, `cate_seo_url`, `cate_meta_description`, `cate_meta_keywords`, `cate_is_serie`) VALUES
(1566398152, 'CASINO', 0, 'CASINO', 'ca-si-no', 'CASINO', 'CASINO', 'yes'),
(1566398192, 'Blog', 0, 'Blog Hướng Dẫn Toàn Tập Về Nhà Cái Win2888', 'win2888-blog', 'Hướng Dẫn Giải đáp từ a đến z các vấn đề thường gặp tại Win2888', 'ĐĂNG KÝ TÀI KHOẢN', 'yes'),
(1566398250, 'DỰ ĐOÁN - CHỐT SỐ', 0, 'DỰ ĐOÁN - CHỐT SỐ', 'du-doan-chot-so', 'DỰ ĐOÁN - CHỐT SỐ', 'DỰ ĐOÁN - CHỐT SỐ', 'yes'),
(1566398262, 'SOI KÈO BÓNG ĐÁ', 0, 'SOI KÈO BÓNG ĐÁ', 'soi-keo-bong-da', 'SOI KÈO BÓNG ĐÁ', 'SOI KÈO BÓNG ĐÁ', 'yes'),
(1566398273, 'Lô Đề Online', 0, 'KINH NGHIỆM Lô Đề Online', 'lo-de-online', 'KINH NGHIỆM Lô Đề Online', 'KINH NGHIỆM', 'yes'),
(1566563600, 'Soi Cầu XSMB', 1566398250, 'Soi Cầu XSMB', 'soi-cau-xsmb', 'Soi cầu XSMB Miễn Phí', 'soi cau xsmb', 'yes'),
(1566563676, 'Soi Cầu XSMN', 1566398250, 'Soi Cầu XSMN', 'soi-cau-xsmn', 'Soi cầu xsmn chính xác nhất', 'soi cau xsmn', 'yes'),
(1566564169, 'Soi Cầu XSMT', 1566398250, 'Soi Cầu XSMT', 'soi-cau-xsmt', 'Soi Cầu XSMT', 'Soi Cầu XSMT', 'yes'),
(1566564939, 'Sổ Mơ Lô Đề', 0, 'Giải Mã Giấc Mơ Lô Đề Chính Xác Nhất', 'so-mo-lo-de', 'Giải Mã Giấc Mơ Lô Đề Chính Xác Nhất', 'so mo lo de', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `tp_chats`
--

CREATE TABLE IF NOT EXISTS `tp_chats` (
  `chat_id` int(11) NOT NULL,
  `chat_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chat_begin_time` int(11) DEFAULT NULL,
  `chat_finish_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tp_comments`
--

CREATE TABLE IF NOT EXISTS `tp_comments` (
  `comment_id` int(11) NOT NULL,
  `comment_url` text COLLATE utf8_unicode_ci,
  `comment_parent` int(11) DEFAULT NULL,
  `comment_contents` text COLLATE utf8_unicode_ci,
  `comment_time` int(10) unsigned DEFAULT NULL,
  `comment_author_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment_author_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment_like_amount` int(10) unsigned DEFAULT '0',
  `comment_publish` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tp_comments`
--

INSERT INTO `tp_comments` (`comment_id`, `comment_url`, `comment_parent`, `comment_contents`, `comment_time`, `comment_author_name`, `comment_author_email`, `comment_like_amount`, `comment_publish`) VALUES
(1, 'huong-dan-dang-ky-mo-tai-khoan-moi-tai-win2888-chinh-xacwd', 0, 'adas', 1566541885, 'Tu Tran', 'txt95.kg@gmail.com', 0, 1),
(2, 'huong-dan-dang-ky-mo-tai-khoan-moi-tai-win2888-chinh-xacwd', 1, 'das', 1566541891, 'Tu Tran', 'txt95.kg@gmail.com', 0, 1),
(3, 'malesuada-bibendum-arcu-vitae-elementum-curabitur-vitae-lobortis-scelerisque-fermentum-dui-faucibus-orci-eu-lobortis-elementum-ni', 0, 'vip', 1566549272, 'chotdep', 'chotde@gmail.com', 0, 1),
(4, 'malesuada-bibendum-arcu-vitae-elementum-curabitur-vitae-lobortis-scelerisque-fermentum-dui-faucibus-orci-eu-lobortis-elementum-ni', 0, 'hay', 1566549279, 'chotdep', 'chotde@gmail.com', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tp_conversations`
--

CREATE TABLE IF NOT EXISTS `tp_conversations` (
  `conversation_id` int(11) NOT NULL,
  `chat_id` int(11) DEFAULT NULL,
  `conversation_content` text COLLATE utf8_unicode_ci,
  `conversation_time` int(11) DEFAULT NULL,
  `conversation_state` enum('sent','received','seen') COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tp_marketing_email`
--

CREATE TABLE IF NOT EXISTS `tp_marketing_email` (
  `marketing_email_id` int(11) NOT NULL,
  `marketing_email_time` int(10) unsigned DEFAULT NULL,
  `marketing_email_contents` text COLLATE utf8_unicode_ci,
  `marketing_email_subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `marketing_email_recievers` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tp_menu`
--

CREATE TABLE IF NOT EXISTS `tp_menu` (
  `menu_id` int(11) NOT NULL,
  `menu_text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `menu_parent_id` int(11) NOT NULL,
  `menu_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `menu_order` int(11) NOT NULL,
  `menu_hidden` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tp_menu`
--

INSERT INTO `tp_menu` (`menu_id`, `menu_text`, `menu_parent_id`, `menu_link`, `menu_order`, `menu_hidden`) VALUES
(1566488006, 'CASINO', 0, 'ca-si-no', 0, 0),
(1566488021, 'ĐĂNG KÝ TÀI KHOẢN', 0, 'dang-ky-tai-khoan', 2, 0),
(1566488058, 'DỰ ĐOÁN - CHỐT SỐ', 0, 'du-doan-chot-so', 3, 0),
(1566488072, 'SOI KÈO BÓNG ĐÁ', 0, 'soi-keo-bong-da', 5, 0),
(1566488082, 'KINH NGHIỆM', 0, 'kinh-nghiem', 6, 0),
(1566488245, 'Cá cược online', 1566488006, 'ca-cuoc-online', 1, 0),
(1566566596, 'Soi Cầu XSMB', 1566488058, 'soi-cau-xsmb', 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tp_posts`
--

CREATE TABLE IF NOT EXISTS `tp_posts` (
  `post_id` int(11) NOT NULL,
  `admin_id` int(11) DEFAULT NULL,
  `cate_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `post_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `post_page_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `post_contents` text COLLATE utf8_unicode_ci,
  `post_compact` text COLLATE utf8_unicode_ci,
  `post_meta_description` text COLLATE utf8_unicode_ci,
  `post_meta_keywords` text COLLATE utf8_unicode_ci,
  `post_seo_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `post_allow_comment` tinyint(1) DEFAULT '1',
  `post_automatic` tinyint(1) DEFAULT '0',
  `post_thumbnail` text COLLATE utf8_unicode_ci,
  `post_time` int(10) unsigned DEFAULT NULL,
  `post_view_count` int(10) unsigned DEFAULT '0',
  `post_time_to_post` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tp_posts`
--

INSERT INTO `tp_posts` (`post_id`, `admin_id`, `cate_id`, `post_title`, `post_page_title`, `post_contents`, `post_compact`, `post_meta_description`, `post_meta_keywords`, `post_seo_url`, `post_allow_comment`, `post_automatic`, `post_thumbnail`, `post_time`, `post_view_count`, `post_time_to_post`) VALUES
(20, 1, ',1566398152', 'Aliquam auctor tellus sed ullamcorper sollicitudin.', 'aa', '<p>aa</p>', 'aaa', 'â', 'aa', 'aliquam-auctor-tellus-sed-ullamcorper-sollicitudin', 0, 1, '["aliquam-auctor-tellus-sed-ullamcorper-sollicitudin-1.jpeg"]', 1566547067, 1, 1565881020),
(22, 1, ',1566398192', 'Aliquam auctor tellus sed ullamcorper sollicitudin.', 'aa', '<p>aaa</p>', 'a', 'aa', 'aa', 'aaaaa', 0, 1, '["aaaaa-1.jpeg"]', 1566547748, 1, 1567090800),
(23, 1, ',,1566398192', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Fames ac turpis egestas maecenas. Diam in arcu cursus euismod quis viverra nibh cras. Est ullamcorper eget nulla facilisi. Egestas erat imperdiet sed euismod nisi porta. Vel eros donec ac odio tempor orci dapibus ultrices. Leo in vitae turpis massa sed elementum tempus egestas sed. Vestibulum sed arcu non odio euismod lacinia. Eget nunc lobortis mattis aliquam faucibus. Sit amet nisl purus in. Arcu non odio euismod lacinia at. Mi eget mauris pharetra et ultrices. Montes nascetur ridiculus mus mauris vitae ultricies leo. Et ultrices neque ornare aenean euismod elementum nisi quis eleifend. Faucibus scelerisque eleifend donec pretium vulputate sapien nec. Non consectetur a erat nam. Aliquet lectus proin nibh nisl condimentum. Eget felis eget nunc lobortis mattis. Et ultrices neque ornare aenean euismod elementum nisi quis. Fermentum dui faucibus in ornare quam viverra orci sagittis.</p>\r\n<p>Vestibulum rhoncus est pellentesque elit ullamcorper dignissim cras tincidunt. Orci ac auctor augue mauris augue neque gravida in fermentum. Interdum velit laoreet id donec ultrices tincidunt arcu non. Pretium lectus quam id leo in vitae turpis massa. Rhoncus est pellentesque elit ullamcorper dignissim. Ultricies integer quis auctor elit. Eros donec ac odio tempor orci. Convallis convallis tellus id interdum. Ipsum dolor sit amet consectetur adipiscing elit ut. Sed viverra tellus in hac habitasse platea dictumst. Adipiscing commodo elit at imperdiet. Orci ac auctor augue mauris augue neque gravida. Aenean sed adipiscing diam donec adipiscing. Non consectetur a erat nam at lectus. At augue eget arcu dictum. Ornare quam viverra orci sagittis eu volutpat odio facilisis. Justo donec enim diam vulputate ut pharetra. Bibendum arcu vitae elementum curabitur vitae.</p>\r\n<p>Varius quam quisque id diam vel quam elementum. Et tortor at risus viverra adipiscing at. Id porta nibh venenatis cras sed felis. Scelerisque varius morbi enim nunc faucibus a pellentesque sit. Senectus et netus et malesuada fames ac turpis egestas sed. Ut morbi tincidunt augue interdum velit euismod in pellentesque. Neque laoreet suspendisse interdum consectetur libero id faucibus nisl tincidunt. At in tellus integer feugiat. Adipiscing bibendum est ultricies integer quis auctor elit sed vulputate. Duis at consectetur lorem donec massa sapien faucibus. Sociis natoque penatibus et magnis dis parturient montes nascetur. In vitae turpis massa sed elementum tempus egestas.</p>\r\n<p>Ac feugiat sed lectus vestibulum mattis ullamcorper velit. Fames ac turpis egestas sed tempus urna et. Cras pulvinar mattis nunc sed blandit libero volutpat sed. Lectus proin nibh nisl condimentum id venenatis a condimentum. Viverra mauris in aliquam sem. Id faucibus nisl tincidunt eget nullam non nisi est sit. Donec adipiscing tristique risus nec feugiat. Quisque id diam vel quam elementum pulvinar etiam. Ullamcorper dignissim cras tincidunt lobortis feugiat. Id cursus metus aliquam eleifend mi. Amet tellus cras adipiscing enim eu turpis egestas. In iaculis nunc sed augue lacus viverra vitae. Non tellus orci ac auctor. Tellus elementum sagittis vitae et leo duis ut diam. Blandit libero volutpat sed cras ornare arcu.</p>\r\n<p>Pellentesque massa placerat duis ultricies lacus sed turpis. Nullam non nisi est sit amet facilisis. Nibh ipsum consequat nisl vel pretium lectus. Nunc congue nisi vitae suscipit tellus mauris. Eget sit amet tellus cras. Ac orci phasellus egestas tellus rutrum tellus. Et sollicitudin ac orci phasellus egestas tellus rutrum tellus pellentesque. Tortor condimentum lacinia quis vel eros donec ac. Lectus magna fringilla urna porttitor rhoncus. Sit amet massa vitae tortor condimentum lacinia quis vel. Gravida arcu ac tortor dignissim convallis aenean et tortor at. Consectetur adipiscing elit duis tristique sollicitudin nibh. Dapibus ultrices in iaculis nunc sed augue lacus viverra.</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-sed-do-eiusmod-tempor-incididunt-ut-labore-et-dolore-magna-aliqua-', 1, 0, '["lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-sed-do-eiusmod-tempor-incididunt-ut-labore-et-dolore-magna-aliqua--1.jpeg"]', 1566547850, 7, NULL),
(24, 1, ',1566398250', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Consequat semper viverra nam libero justo laoreet sit. Amet est placerat in egestas erat imperdiet sed euismod. Consequat interdum varius sit amet mattis. Duis tristique sollicitudin nibh sit amet commodo nulla. Pretium quam vulputate dignissim suspendisse in est ante. Adipiscing commodo elit at imperdiet dui. Ridiculus mus mauris vitae ultricies leo. Enim diam vulputate ut pharetra sit amet aliquam id. Sagittis eu volutpat odio facilisis mauris.</p>\r\n<p>Cras tincidunt lobortis feugiat vivamus at. Sed viverra tellus in hac habitasse platea dictumst. Mattis pellentesque id nibh tortor id. Mauris pellentesque pulvinar pellentesque habitant morbi tristique. Amet facilisis magna etiam tempor orci. Egestas sed sed risus pretium quam. Enim ut tellus elementum sagittis vitae. Tortor at auctor urna nunc id cursus. Proin sagittis nisl rhoncus mattis rhoncus urna neque viverra. Consectetur purus ut faucibus pulvinar elementum. Ultricies integer quis auctor elit sed vulputate mi sit amet.</p>\r\n<p>Ultrices in iaculis nunc sed augue. Amet tellus cras adipiscing enim eu turpis egestas pretium aenean. Ac turpis egestas sed tempus urna. Tellus pellentesque eu tincidunt tortor aliquam nulla. At risus viverra adipiscing at in tellus. Ullamcorper a lacus vestibulum sed arcu non odio. Augue neque gravida in fermentum et sollicitudin. Tristique senectus et netus et malesuada fames ac. Purus faucibus ornare suspendisse sed nisi lacus sed viverra tellus. Sodales ut eu sem integer vitae justo eget. Dui id ornare arcu odio ut. Turpis egestas sed tempus urna. Nibh praesent tristique magna sit amet. Ipsum consequat nisl vel pretium lectus quam id leo in. Vel eros donec ac odio tempor orci dapibus ultrices. Eget nulla facilisi etiam dignissim. Nunc aliquet bibendum enim facilisis gravida neque convallis a cras. Auctor urna nunc id cursus metus. Natoque penatibus et magnis dis parturient. Aliquam sem fringilla ut morbi tincidunt augue.</p>\r\n<p>Vulputate odio ut enim blandit volutpat maecenas volutpat blandit aliquam. Turpis egestas sed tempus urna et pharetra pharetra massa. Tristique et egestas quis ipsum suspendisse ultrices. Habitant morbi tristique senectus et netus et. Posuere morbi leo urna molestie at. In nulla posuere sollicitudin aliquam ultrices sagittis orci a. Nibh praesent tristique magna sit. Non consectetur a erat nam at lectus urna duis convallis. Purus in mollis nunc sed id semper risus in hendrerit. Lorem ipsum dolor sit amet consectetur adipiscing elit pellentesque habitant. Odio ut enim blandit volutpat maecenas volutpat blandit aliquam. Dignissim cras tincidunt lobortis feugiat vivamus.</p>\r\n<p>Felis donec et odio pellentesque diam volutpat commodo. Scelerisque fermentum dui faucibus in ornare quam. Ut pharetra sit amet aliquam id diam maecenas. Cras ornare arcu dui vivamus arcu. Sed cras ornare arcu dui vivamus arcu felis. Sit amet est placerat in egestas. Magna fermentum iaculis eu non diam phasellus vestibulum. Urna nunc id cursus metus aliquam. Blandit aliquam etiam erat velit scelerisque in. Est ullamcorper eget nulla facilisi etiam dignissim diam quis enim. Id diam vel quam elementum pulvinar etiam non quam lacus. Massa eget egestas purus viverra accumsan in nisl nisi scelerisque. Est ante in nibh mauris cursus mattis molestie a iaculis. Lorem donec massa sapien faucibus et molestie ac feugiat. Suspendisse sed nisi lacus sed viverra tellus in hac habitasse. Nisi porta lorem mollis aliquam ut porttitor leo a. Id ornare arcu odio ut sem. Scelerisque eleifend donec pretium vulputate sapien nec sagittis. Urna molestie at elementum eu facilisis sed. Eu mi bibendum neque egestas congue.</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua', 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-sed-do-eiusmod-tempor-incididunt-ut-labore-et-dolore-magna-aliqua', 1, 0, '["lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-sed-do-eiusmod-tempor-incididunt-ut-labore-et-dolore-magna-aliqua-1.jpeg"]', 1566547891, 1, NULL),
(26, 1, ',1566398262', 'lorem ipsum lorem ipsum', 'lorem ipsum lorem ipsum', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Iaculis at erat pellentesque adipiscing commodo elit at imperdiet. Donec et odio pellentesque diam volutpat. Facilisis leo vel fringilla est ullamcorper eget nulla. Donec ac odio tempor orci dapibus. Tristique nulla aliquet enim tortor. Non odio euismod lacinia at. Mauris rhoncus aenean vel elit scelerisque mauris pellentesque. Amet consectetur adipiscing elit ut. Amet consectetur adipiscing elit duis tristique sollicitudin. Sed felis eget velit aliquet sagittis id consectetur purus. Consequat interdum varius sit amet mattis vulputate enim. Lectus magna fringilla urna porttitor. Proin nibh nisl condimentum id venenatis a condimentum vitae. Diam quam nulla porttitor massa id neque. Vulputate mi sit amet mauris commodo quis imperdiet massa. Vel turpis nunc eget lorem.</p>\r\n<p>Nunc scelerisque viverra mauris in. Lacus laoreet non curabitur gravida arcu ac tortor dignissim. Nibh praesent tristique magna sit amet purus gravida. Tincidunt ornare massa eget egestas purus viverra accumsan. Enim ut sem viverra aliquet eget. Aliquam ut porttitor leo a. Elementum pulvinar etiam non quam lacus suspendisse faucibus interdum posuere. Sit amet volutpat consequat mauris nunc. Nullam ac tortor vitae purus faucibus ornare suspendisse sed nisi. Aenean vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant. Consectetur lorem donec massa sapien faucibus et. Interdum consectetur libero id faucibus nisl. Mauris in aliquam sem fringilla ut morbi tincidunt. Id velit ut tortor pretium viverra. Sem fringilla ut morbi tincidunt augue interdum velit euismod in. Velit sed ullamcorper morbi tincidunt ornare massa.</p>\r\n<p>Sed egestas egestas fringilla phasellus faucibus scelerisque. Morbi tristique senectus et netus et malesuada fames ac. Nam aliquam sem et tortor consequat. Vivamus arcu felis bibendum ut tristique et egestas. Pellentesque adipiscing commodo elit at imperdiet dui accumsan sit. Euismod elementum nisi quis eleifend. Eget magna fermentum iaculis eu non. Leo duis ut diam quam nulla. Ut aliquam purus sit amet. Duis ut diam quam nulla porttitor massa.</p>\r\n<p>Odio eu feugiat pretium nibh ipsum consequat nisl. Dignissim convallis aenean et tortor at. Nibh cras pulvinar mattis nunc sed blandit libero volutpat sed. Magna fringilla urna porttitor rhoncus dolor purus non enim praesent. Tempus iaculis urna id volutpat lacus laoreet non curabitur. Mauris pellentesque pulvinar pellentesque habitant morbi. Morbi tristique senectus et netus et malesuada fames. Convallis posuere morbi leo urna molestie. Eu tincidunt tortor aliquam nulla facilisi cras. Auctor eu augue ut lectus arcu bibendum at varius.</p>\r\n<p>Malesuada bibendum arcu vitae elementum curabitur vitae. Lobortis scelerisque fermentum dui faucibus. Orci eu lobortis elementum nibh tellus molestie nunc non blandit. Velit egestas dui id ornare arcu odio. Vehicula ipsum a arcu cursus vitae congue mauris. Aliquam sem fringilla ut morbi. Quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus. Quam viverra orci sagittis eu volutpat. Nunc eget lorem dolor sed viverra ipsum nunc. Purus sit amet volutpat consequat. Tincidunt vitae semper quis lectus nulla at volutpat diam. Maecenas accumsan lacus vel facilisis volutpat est velit egestas dui.</p>', 'lorem ipsum lorem ipsum', 'lorem ipsum lorem ipsum', 'lorem ipsum lorem ipsum', 'lorem-ipsum-lorem-ipsum', 1, 0, '["lorem-ipsum-lorem-ipsum-1.jpeg"]', 1566548050, 3, NULL),
(27, 1, ',1566398273', 'lorem ipsum lorem ipsum 1234567', 'lorem-ipsum-lorem-ipsum-1234567', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Iaculis at erat pellentesque adipiscing commodo elit at imperdiet. Donec et odio pellentesque diam volutpat. Facilisis leo vel fringilla est ullamcorper eget nulla. Donec ac odio tempor orci dapibus. Tristique nulla aliquet enim tortor. Non odio euismod lacinia at. Mauris rhoncus aenean vel elit scelerisque mauris pellentesque. Amet consectetur adipiscing elit ut. Amet consectetur adipiscing elit duis tristique sollicitudin. Sed felis eget velit aliquet sagittis id consectetur purus. Consequat interdum varius sit amet mattis vulputate enim. Lectus magna fringilla urna porttitor. Proin nibh nisl condimentum id venenatis a condimentum vitae. Diam quam nulla porttitor massa id neque. Vulputate mi sit amet mauris commodo quis imperdiet massa. Vel turpis nunc eget lorem.</p>\r\n<p>Nunc scelerisque viverra mauris in. Lacus laoreet non curabitur gravida arcu ac tortor dignissim. Nibh praesent tristique magna sit amet purus gravida. Tincidunt ornare massa eget egestas purus viverra accumsan. Enim ut sem viverra aliquet eget. Aliquam ut porttitor leo a. Elementum pulvinar etiam non quam lacus suspendisse faucibus interdum posuere. Sit amet volutpat consequat mauris nunc. Nullam ac tortor vitae purus faucibus ornare suspendisse sed nisi. Aenean vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant. Consectetur lorem donec massa sapien faucibus et. Interdum consectetur libero id faucibus nisl. Mauris in aliquam sem fringilla ut morbi tincidunt. Id velit ut tortor pretium viverra. Sem fringilla ut morbi tincidunt augue interdum velit euismod in. Velit sed ullamcorper morbi tincidunt ornare massa.</p>\r\n<p>Sed egestas egestas fringilla phasellus faucibus scelerisque. Morbi tristique senectus et netus et malesuada fames ac. Nam aliquam sem et tortor consequat. Vivamus arcu felis bibendum ut tristique et egestas. Pellentesque adipiscing commodo elit at imperdiet dui accumsan sit. Euismod elementum nisi quis eleifend. Eget magna fermentum iaculis eu non. Leo duis ut diam quam nulla. Ut aliquam purus sit amet. Duis ut diam quam nulla porttitor massa.</p>\r\n<p>Odio eu feugiat pretium nibh ipsum consequat nisl. Dignissim convallis aenean et tortor at. Nibh cras pulvinar mattis nunc sed blandit libero volutpat sed. Magna fringilla urna porttitor rhoncus dolor purus non enim praesent. Tempus iaculis urna id volutpat lacus laoreet non curabitur. Mauris pellentesque pulvinar pellentesque habitant morbi. Morbi tristique senectus et netus et malesuada fames. Convallis posuere morbi leo urna molestie. Eu tincidunt tortor aliquam nulla facilisi cras. Auctor eu augue ut lectus arcu bibendum at varius.</p>\r\n<p>Malesuada bibendum arcu vitae elementum curabitur vitae. Lobortis scelerisque fermentum dui faucibus. Orci eu lobortis elementum nibh tellus molestie nunc non blandit. Velit egestas dui id ornare arcu odio. Vehicula ipsum a arcu cursus vitae congue mauris. Aliquam sem fringilla ut morbi. Quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus. Quam viverra orci sagittis eu volutpat. Nunc eget lorem dolor sed viverra ipsum nunc. Purus sit amet volutpat consequat. Tincidunt vitae semper quis lectus nulla at volutpat diam. Maecenas accumsan lacus vel facilisis volutpat est velit egestas dui.</p>', 'lorem ipsum lorem ipsum 1234567 lorem ipsum lorem ipsum 1234567', 'lorem-ipsum-lorem-ipsum-1234567', 'lorem-ipsum-lorem-ipsum-1234567', 'lorem-ipsum-lorem-ipsum-1234567', 1, 0, '["lorem-ipsum-lorem-ipsum-1234567-1.jpeg"]', 1566548103, 0, NULL),
(28, 1, ',,1566398435,1566398273,1566398262,1566398250,1566398192,1566398152', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed d', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed d', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Iaculis at erat pellentesque adipiscing commodo elit at imperdiet. Donec et odio pellentesque diam volutpat. Facilisis leo vel fringilla est ullamcorper eget nulla. Donec ac odio tempor orci dapibus. Tristique nulla aliquet enim tortor. Non odio euismod lacinia at. Mauris rhoncus aenean vel elit scelerisque mauris pellentesque. Amet consectetur adipiscing elit ut. Amet consectetur adipiscing elit duis tristique sollicitudin. Sed felis eget velit aliquet sagittis id consectetur purus. Consequat interdum varius sit amet mattis vulputate enim. Lectus magna fringilla urna porttitor. Proin nibh nisl condimentum id venenatis a condimentum vitae. Diam quam nulla porttitor massa id neque. Vulputate mi sit amet mauris commodo quis imperdiet massa. Vel turpis nunc eget lorem.</p>\r\n<p>Nunc scelerisque viverra mauris in. Lacus laoreet non curabitur gravida arcu ac tortor dignissim. Nibh praesent tristique magna sit amet purus gravida. Tincidunt ornare massa eget egestas purus viverra accumsan. Enim ut sem viverra aliquet eget. Aliquam ut porttitor leo a. Elementum pulvinar etiam non quam lacus suspendisse faucibus interdum posuere. Sit amet volutpat consequat mauris nunc. Nullam ac tortor vitae purus faucibus ornare suspendisse sed nisi. Aenean vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant. Consectetur lorem donec massa sapien faucibus et. Interdum consectetur libero id faucibus nisl. Mauris in aliquam sem fringilla ut morbi tincidunt. Id velit ut tortor pretium viverra. Sem fringilla ut morbi tincidunt augue interdum velit euismod in. Velit sed ullamcorper morbi tincidunt ornare massa.</p>\r\n<p>Sed egestas egestas fringilla phasellus faucibus scelerisque. Morbi tristique senectus et netus et malesuada fames ac. Nam aliquam sem et tortor consequat. Vivamus arcu felis bibendum ut tristique et egestas. Pellentesque adipiscing commodo elit at imperdiet dui accumsan sit. Euismod elementum nisi quis eleifend. Eget magna fermentum iaculis eu non. Leo duis ut diam quam nulla. Ut aliquam purus sit amet. Duis ut diam quam nulla porttitor massa.</p>\r\n<p>Odio eu feugiat pretium nibh ipsum consequat nisl. Dignissim convallis aenean et tortor at. Nibh cras pulvinar mattis nunc sed blandit libero volutpat sed. Magna fringilla urna porttitor rhoncus dolor purus non enim praesent. Tempus iaculis urna id volutpat lacus laoreet non curabitur. Mauris pellentesque pulvinar pellentesque habitant morbi. Morbi tristique senectus et netus et malesuada fames. Convallis posuere morbi leo urna molestie. Eu tincidunt tortor aliquam nulla facilisi cras. Auctor eu augue ut lectus arcu bibendum at varius.</p>\r\n<p>Malesuada bibendum arcu vitae elementum curabitur vitae. Lobortis scelerisque fermentum dui faucibus. Orci eu lobortis elementum nibh tellus molestie nunc non blandit. Velit egestas dui id ornare arcu odio. Vehicula ipsum a arcu cursus vitae congue mauris. Aliquam sem fringilla ut morbi. Quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus. Quam viverra orci sagittis eu volutpat. Nunc eget lorem dolor sed viverra ipsum nunc. Purus sit amet volutpat consequat. Tincidunt vitae semper quis lectus nulla at volutpat diam. Maecenas accumsan lacus vel facilisis volutpat est velit egestas dui.</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed d', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed d', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed d', 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-sed-d', 1, 0, '["lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-sed-d-1.jpeg"]', 1566548170, 3, NULL),
(29, 1, ',1566398192', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Iaculis at erat pellentesque adipiscing commodo elit at imperdiet. Donec et odio pellentesque diam volutpat. Facilisis leo vel fringilla est ullamcorper eget nulla. Donec ac odio tempor orci dapibus. Tristique nulla aliquet enim tortor. Non odio euismod lacinia at. Mauris rhoncus aenean vel elit scelerisque mauris pellentesque. Amet consectetur adipiscing elit ut. Amet consectetur adipiscing elit duis tristique sollicitudin. Sed felis eget velit aliquet sagittis id consectetur purus. Consequat interdum varius sit amet mattis vulputate enim. Lectus magna fringilla urna porttitor. Proin nibh nisl condimentum id venenatis a condimentum vitae. Diam quam nulla porttitor massa id neque. Vulputate mi sit amet mauris commodo quis imperdiet massa. Vel turpis nunc eget lorem.</p>\r\n<p>Nunc scelerisque viverra mauris in. Lacus laoreet non curabitur gravida arcu ac tortor dignissim. Nibh praesent tristique magna sit amet purus gravida. Tincidunt ornare massa eget egestas purus viverra accumsan. Enim ut sem viverra aliquet eget. Aliquam ut porttitor leo a. Elementum pulvinar etiam non quam lacus suspendisse faucibus interdum posuere. Sit amet volutpat consequat mauris nunc. Nullam ac tortor vitae purus faucibus ornare suspendisse sed nisi. Aenean vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant. Consectetur lorem donec massa sapien faucibus et. Interdum consectetur libero id faucibus nisl. Mauris in aliquam sem fringilla ut morbi tincidunt. Id velit ut tortor pretium viverra. Sem fringilla ut morbi tincidunt augue interdum velit euismod in. Velit sed ullamcorper morbi tincidunt ornare massa.</p>\r\n<p>Sed egestas egestas fringilla phasellus faucibus scelerisque. Morbi tristique senectus et netus et malesuada fames ac. Nam aliquam sem et tortor consequat. Vivamus arcu felis bibendum ut tristique et egestas. Pellentesque adipiscing commodo elit at imperdiet dui accumsan sit. Euismod elementum nisi quis eleifend. Eget magna fermentum iaculis eu non. Leo duis ut diam quam nulla. Ut aliquam purus sit amet. Duis ut diam quam nulla porttitor massa.</p>\r\n<p>Odio eu feugiat pretium nibh ipsum consequat nisl. Dignissim convallis aenean et tortor at. Nibh cras pulvinar mattis nunc sed blandit libero volutpat sed. Magna fringilla urna porttitor rhoncus dolor purus non enim praesent. Tempus iaculis urna id volutpat lacus laoreet non curabitur. Mauris pellentesque pulvinar pellentesque habitant morbi. Morbi tristique senectus et netus et malesuada fames. Convallis posuere morbi leo urna molestie. Eu tincidunt tortor aliquam nulla facilisi cras. Auctor eu augue ut lectus arcu bibendum at varius.</p>\r\n<p>Malesuada bibendum arcu vitae elementum curabitur vitae. Lobortis scelerisque fermentum dui faucibus. Orci eu lobortis elementum nibh tellus molestie nunc non blandit. Velit egestas dui id ornare arcu odio. Vehicula ipsum a arcu cursus vitae congue mauris. Aliquam sem fringilla ut morbi. Quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus. Quam viverra orci sagittis eu volutpat. Nunc eget lorem dolor sed viverra ipsum nunc. Purus sit amet volutpat consequat. Tincidunt vitae semper quis lectus nulla at volutpat diam. Maecenas accumsan lacus vel facilisis volutpat est velit egestas dui.</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ', 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-sed-do-', 1, 0, '["lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-sed-do--1.jpeg"]', 1566548240, 6, NULL),
(30, 1, ',1566398250', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dol', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dol', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Iaculis at erat pellentesque adipiscing commodo elit at imperdiet. Donec et odio pellentesque diam volutpat. Facilisis leo vel fringilla est ullamcorper eget nulla. Donec ac odio tempor orci dapibus. Tristique nulla aliquet enim tortor. Non odio euismod lacinia at. Mauris rhoncus aenean vel elit scelerisque mauris pellentesque. Amet consectetur adipiscing elit ut. Amet consectetur adipiscing elit duis tristique sollicitudin. Sed felis eget velit aliquet sagittis id consectetur purus. Consequat interdum varius sit amet mattis vulputate enim. Lectus magna fringilla urna porttitor. Proin nibh nisl condimentum id venenatis a condimentum vitae. Diam quam nulla porttitor massa id neque. Vulputate mi sit amet mauris commodo quis imperdiet massa. Vel turpis nunc eget lorem.</p>\r\n<p>Nunc scelerisque viverra mauris in. Lacus laoreet non curabitur gravida arcu ac tortor dignissim. Nibh praesent tristique magna sit amet purus gravida. Tincidunt ornare massa eget egestas purus viverra accumsan. Enim ut sem viverra aliquet eget. Aliquam ut porttitor leo a. Elementum pulvinar etiam non quam lacus suspendisse faucibus interdum posuere. Sit amet volutpat consequat mauris nunc. Nullam ac tortor vitae purus faucibus ornare suspendisse sed nisi. Aenean vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant. Consectetur lorem donec massa sapien faucibus et. Interdum consectetur libero id faucibus nisl. Mauris in aliquam sem fringilla ut morbi tincidunt. Id velit ut tortor pretium viverra. Sem fringilla ut morbi tincidunt augue interdum velit euismod in. Velit sed ullamcorper morbi tincidunt ornare massa.</p>\r\n<p>Sed egestas egestas fringilla phasellus faucibus scelerisque. Morbi tristique senectus et netus et malesuada fames ac. Nam aliquam sem et tortor consequat. Vivamus arcu felis bibendum ut tristique et egestas. Pellentesque adipiscing commodo elit at imperdiet dui accumsan sit. Euismod elementum nisi quis eleifend. Eget magna fermentum iaculis eu non. Leo duis ut diam quam nulla. Ut aliquam purus sit amet. Duis ut diam quam nulla porttitor massa.</p>\r\n<p>Odio eu feugiat pretium nibh ipsum consequat nisl. Dignissim convallis aenean et tortor at. Nibh cras pulvinar mattis nunc sed blandit libero volutpat sed. Magna fringilla urna porttitor rhoncus dolor purus non enim praesent. Tempus iaculis urna id volutpat lacus laoreet non curabitur. Mauris pellentesque pulvinar pellentesque habitant morbi. Morbi tristique senectus et netus et malesuada fames. Convallis posuere morbi leo urna molestie. Eu tincidunt tortor aliquam nulla facilisi cras. Auctor eu augue ut lectus arcu bibendum at varius.</p>\r\n<p>Malesuada bibendum arcu vitae elementum curabitur vitae. Lobortis scelerisque fermentum dui faucibus. Orci eu lobortis elementum nibh tellus molestie nunc non blandit. Velit egestas dui id ornare arcu odio. Vehicula ipsum a arcu cursus vitae congue mauris. Aliquam sem fringilla ut morbi. Quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus. Quam viverra orci sagittis eu volutpat. Nunc eget lorem dolor sed viverra ipsum nunc. Purus sit amet volutpat consequat. Tincidunt vitae semper quis lectus nulla at volutpat diam. Maecenas accumsan lacus vel facilisis volutpat est velit egestas dui.</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dol', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dol', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dol', 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-sed-do-eiusmod-tempor-incididunt-ut-labore-et-dol', 1, 0, '["lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-sed-do-eiusmod-tempor-incididunt-ut-labore-et-dol-1.jpeg"]', 1566548282, 4, NULL),
(31, 1, ',1566398152', 'Malesuada bibendum arcu vitae elementum curabitur vitae. Lobortis scelerisque fermentum dui faucibus. Orci eu lobortis elementum ni', 'malesuada-bibendum-arcu-vitae-elementum-curabitur-vitae.-lobortis-scelerisque-fermentum-dui-faucibus.-orci-eu-lobortis-elementum-ni', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Iaculis at erat pellentesque adipiscing commodo elit at imperdiet. Donec et odio pellentesque diam volutpat. Facilisis leo vel fringilla est ullamcorper eget nulla. Donec ac odio tempor orci dapibus. Tristique nulla aliquet enim tortor. Non odio euismod lacinia at. Mauris rhoncus aenean vel elit scelerisque mauris pellentesque. Amet consectetur adipiscing elit ut. Amet consectetur adipiscing elit duis tristique sollicitudin. Sed felis eget velit aliquet sagittis id consectetur purus. Consequat interdum varius sit amet mattis vulputate enim. Lectus magna fringilla urna porttitor. Proin nibh nisl condimentum id venenatis a condimentum vitae. Diam quam nulla porttitor massa id neque. Vulputate mi sit amet mauris commodo quis imperdiet massa. Vel turpis nunc eget lorem.</p>\r\n<p>Nunc scelerisque viverra mauris in. Lacus laoreet non curabitur gravida arcu ac tortor dignissim. Nibh praesent tristique magna sit amet purus gravida. Tincidunt ornare massa eget egestas purus viverra accumsan. Enim ut sem viverra aliquet eget. Aliquam ut porttitor leo a. Elementum pulvinar etiam non quam lacus suspendisse faucibus interdum posuere. Sit amet volutpat consequat mauris nunc. Nullam ac tortor vitae purus faucibus ornare suspendisse sed nisi. Aenean vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant. Consectetur lorem donec massa sapien faucibus et. Interdum consectetur libero id faucibus nisl. Mauris in aliquam sem fringilla ut morbi tincidunt. Id velit ut tortor pretium viverra. Sem fringilla ut morbi tincidunt augue interdum velit euismod in. Velit sed ullamcorper morbi tincidunt ornare massa.</p>\r\n<p>Sed egestas egestas fringilla phasellus faucibus scelerisque. Morbi tristique senectus et netus et malesuada fames ac. Nam aliquam sem et tortor consequat. Vivamus arcu felis bibendum ut tristique et egestas. Pellentesque adipiscing commodo elit at imperdiet dui accumsan sit. Euismod elementum nisi quis eleifend. Eget magna fermentum iaculis eu non. Leo duis ut diam quam nulla. Ut aliquam purus sit amet. Duis ut diam quam nulla porttitor massa.</p>\r\n<p>Odio eu feugiat pretium nibh ipsum consequat nisl. Dignissim convallis aenean et tortor at. Nibh cras pulvinar mattis nunc sed blandit libero volutpat sed. Magna fringilla urna porttitor rhoncus dolor purus non enim praesent. Tempus iaculis urna id volutpat lacus laoreet non curabitur. Mauris pellentesque pulvinar pellentesque habitant morbi. Morbi tristique senectus et netus et malesuada fames. Convallis posuere morbi leo urna molestie. Eu tincidunt tortor aliquam nulla facilisi cras. Auctor eu augue ut lectus arcu bibendum at varius.</p>\r\n<p>Malesuada bibendum arcu vitae elementum curabitur vitae. Lobortis scelerisque fermentum dui faucibus. Orci eu lobortis elementum nibh tellus molestie nunc non blandit. Velit egestas dui id ornare arcu odio. Vehicula ipsum a arcu cursus vitae congue mauris. Aliquam sem fringilla ut morbi. Quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus. Quam viverra orci sagittis eu volutpat. Nunc eget lorem dolor sed viverra ipsum nunc. Purus sit amet volutpat consequat. Tincidunt vitae semper quis lectus nulla at volutpat diam. Maecenas accumsan lacus vel facilisis volutpat est velit egestas dui.</p>', 'Malesuada bibendum arcu vitae elementum curabitur vitae. Lobortis scelerisque fermentum dui faucibus. Orci eu lobortis elementum ni', 'malesuada-bibendum-arcu-vitae-elementum-curabitur-vitae.-lobortis-scelerisque-fermentum-dui-faucibus.-orci-eu-lobortis-elementum-ni', 'malesuada-bibendum-arcu-vitae-elementum-curabitur-vitae.-lobortis-scelerisque-fermentum-dui-faucibus.-orci-eu-lobortis-elementum-ni', 'malesuada-bibendum-arcu-vitae-elementum-curabitur-vitae-lobortis-scelerisque-fermentum-dui-faucibus-orci-eu-lobortis-elementum-ni', 1, 0, '["malesuada-bibendum-arcu-vitae-elementum-curabitur-vitae-lobortis-scelerisque-fermentum-dui-faucibus-orci-eu-lobortis-elementum-ni-1.jpeg"]', 1566548401, 10, 0),
(32, 1, ',1566398262,1566398273', 'dipiscing commodo elit at imperdiet. Donec et odio pellentesque diam ', 'dipiscing commodo elit at imperdiet. Donec et odio pellentesque diam ', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Iaculis at erat pellentesque adipiscing commodo elit at imperdiet. Donec et odio pellentesque diam volutpat. Facilisis leo vel fringilla est ullamcorper eget nulla. Donec ac odio tempor orci dapibus. Tristique nulla aliquet enim tortor. Non odio euismod lacinia at. Mauris rhoncus aenean vel elit scelerisque mauris pellentesque. Amet consectetur adipiscing elit ut. Amet consectetur adipiscing elit duis tristique sollicitudin. Sed felis eget velit aliquet sagittis id consectetur purus. Consequat interdum varius sit amet mattis vulputate enim. Lectus magna fringilla urna porttitor. Proin nibh nisl condimentum id venenatis a condimentum vitae. Diam quam nulla porttitor massa id neque. Vulputate mi sit amet mauris commodo quis imperdiet massa. Vel turpis nunc eget lorem.</p>\r\n<p>Nunc scelerisque viverra mauris in. Lacus laoreet non curabitur gravida arcu ac tortor dignissim. Nibh praesent tristique magna sit amet purus gravida. Tincidunt ornare massa eget egestas purus viverra accumsan. Enim ut sem viverra aliquet eget. Aliquam ut porttitor leo a. Elementum pulvinar etiam non quam lacus suspendisse faucibus interdum posuere. Sit amet volutpat consequat mauris nunc. Nullam ac tortor vitae purus faucibus ornare suspendisse sed nisi. Aenean vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant. Consectetur lorem donec massa sapien faucibus et. Interdum consectetur libero id faucibus nisl. Mauris in aliquam sem fringilla ut morbi tincidunt. Id velit ut tortor pretium viverra. Sem fringilla ut morbi tincidunt augue interdum velit euismod in. Velit sed ullamcorper morbi tincidunt ornare massa.</p>\r\n<p>Sed egestas egestas fringilla phasellus faucibus scelerisque. Morbi tristique senectus et netus et malesuada fames ac. Nam aliquam sem et tortor consequat. Vivamus arcu felis bibendum ut tristique et egestas. Pellentesque adipiscing commodo elit at imperdiet dui accumsan sit. Euismod elementum nisi quis eleifend. Eget magna fermentum iaculis eu non. Leo duis ut diam quam nulla. Ut aliquam purus sit amet. Duis ut diam quam nulla porttitor massa.</p>\r\n<p>Odio eu feugiat pretium nibh ipsum consequat nisl. Dignissim convallis aenean et tortor at. Nibh cras pulvinar mattis nunc sed blandit libero volutpat sed. Magna fringilla urna porttitor rhoncus dolor purus non enim praesent. Tempus iaculis urna id volutpat lacus laoreet non curabitur. Mauris pellentesque pulvinar pellentesque habitant morbi. Morbi tristique senectus et netus et malesuada fames. Convallis posuere morbi leo urna molestie. Eu tincidunt tortor aliquam nulla facilisi cras. Auctor eu augue ut lectus arcu bibendum at varius.</p>\r\n<p>Malesuada bibendum arcu vitae elementum curabitur vitae. Lobortis scelerisque fermentum dui faucibus. Orci eu lobortis elementum nibh tellus molestie nunc non blandit. Velit egestas dui id ornare arcu odio. Vehicula ipsum a arcu cursus vitae congue mauris. Aliquam sem fringilla ut morbi. Quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus. Quam viverra orci sagittis eu volutpat. Nunc eget lorem dolor sed viverra ipsum nunc. Purus sit amet volutpat consequat. Tincidunt vitae semper quis lectus nulla at volutpat diam. Maecenas accumsan lacus vel facilisis volutpat est velit egestas dui.</p>', 'dipiscing commodo elit at imperdiet. Donec et odio pellentesque diam ', 'dipiscing commodo elit at imperdiet. Donec et odio pellentesque diam ', 'dipiscing commodo elit at imperdiet. Donec et odio pellentesque diam ', 'dipiscing-commodo-elit-at-imperdiet-donec-et-odio-pellentesque-diam-', 1, 0, '["dipiscing-commodo-elit-at-imperdiet-donec-et-odio-pellentesque-diam--1.jpeg"]', 1566548465, 0, NULL),
(33, 1, ',1566398262', 'mod tempor incididunt ut labore et dolore magna aliqua. Iaculis at erat pellentesque adipiscing commodo ', 'mod-tempor-incididunt-ut-labore-et-dolore-magna-aliqua.-iaculis-at-erat-pellentesque-adipiscing-commodo-', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Iaculis at erat pellentesque adipiscing commodo elit at imperdiet. Donec et odio pellentesque diam volutpat. Facilisis leo vel fringilla est ullamcorper eget nulla. Donec ac odio tempor orci dapibus. Tristique nulla aliquet enim tortor. Non odio euismod lacinia at. Mauris rhoncus aenean vel elit scelerisque mauris pellentesque. Amet consectetur adipiscing elit ut. Amet consectetur adipiscing elit duis tristique sollicitudin. Sed felis eget velit aliquet sagittis id consectetur purus. Consequat interdum varius sit amet mattis vulputate enim. Lectus magna fringilla urna porttitor. Proin nibh nisl condimentum id venenatis a condimentum vitae. Diam quam nulla porttitor massa id neque. Vulputate mi sit amet mauris commodo quis imperdiet massa. Vel turpis nunc eget lorem.</p>\r\n<p>Nunc scelerisque viverra mauris in. Lacus laoreet non curabitur gravida arcu ac tortor dignissim. Nibh praesent tristique magna sit amet purus gravida. Tincidunt ornare massa eget egestas purus viverra accumsan. Enim ut sem viverra aliquet eget. Aliquam ut porttitor leo a. Elementum pulvinar etiam non quam lacus suspendisse faucibus interdum posuere. Sit amet volutpat consequat mauris nunc. Nullam ac tortor vitae purus faucibus ornare suspendisse sed nisi. Aenean vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant. Consectetur lorem donec massa sapien faucibus et. Interdum consectetur libero id faucibus nisl. Mauris in aliquam sem fringilla ut morbi tincidunt. Id velit ut tortor pretium viverra. Sem fringilla ut morbi tincidunt augue interdum velit euismod in. Velit sed ullamcorper morbi tincidunt ornare massa.</p>\r\n<p>Sed egestas egestas fringilla phasellus faucibus scelerisque. Morbi tristique senectus et netus et malesuada fames ac. Nam aliquam sem et tortor consequat. Vivamus arcu felis bibendum ut tristique et egestas. Pellentesque adipiscing commodo elit at imperdiet dui accumsan sit. Euismod elementum nisi quis eleifend. Eget magna fermentum iaculis eu non. Leo duis ut diam quam nulla. Ut aliquam purus sit amet. Duis ut diam quam nulla porttitor massa.</p>\r\n<p>Odio eu feugiat pretium nibh ipsum consequat nisl. Dignissim convallis aenean et tortor at. Nibh cras pulvinar mattis nunc sed blandit libero volutpat sed. Magna fringilla urna porttitor rhoncus dolor purus non enim praesent. Tempus iaculis urna id volutpat lacus laoreet non curabitur. Mauris pellentesque pulvinar pellentesque habitant morbi. Morbi tristique senectus et netus et malesuada fames. Convallis posuere morbi leo urna molestie. Eu tincidunt tortor aliquam nulla facilisi cras. Auctor eu augue ut lectus arcu bibendum at varius.</p>\r\n<p>Malesuada bibendum arcu vitae elementum curabitur vitae. Lobortis scelerisque fermentum dui faucibus. Orci eu lobortis elementum nibh tellus molestie nunc non blandit. Velit egestas dui id ornare arcu odio. Vehicula ipsum a arcu cursus vitae congue mauris. Aliquam sem fringilla ut morbi. Quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus. Quam viverra orci sagittis eu volutpat. Nunc eget lorem dolor sed viverra ipsum nunc. Purus sit amet volutpat consequat. Tincidunt vitae semper quis lectus nulla at volutpat diam. Maecenas accumsan lacus vel facilisis volutpat est velit egestas dui.</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Iaculis at erat pellentesque adipiscing commodo elit at imperdiet. Donec et odio pellentesque diam volutpat. Facilisis leo vel fringilla est ullamcorper eget nulla. Donec ac odio tempor orc', 'mod-tempor-incididunt-ut-labore-et-dolore-magna-aliqua.-iaculis-at-erat-pellentesque-adipiscing-commodo-', 'mod-tempor-incididunt-ut-labore-et-dolore-magna-aliqua.-iaculis-at-erat-pellentesque-adipiscing-commodo-', 'mod-tempor-incididunt-ut-labore-et-dolore-magna-aliqua-iaculis-at-erat-pellentesque-adipiscing-commodo-', 1, 0, '["mod-tempor-incididunt-ut-labore-et-dolore-magna-aliqua-iaculis-at-erat-pellentesque-adipiscing-commodo--1.jpeg"]', 1566548536, 2, NULL),
(34, 1, ',1566398273', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore ma', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Iaculis at erat pellentesque adipiscing commodo elit at imperdiet. Donec et odio pellentesque diam volutpat. Facilisis leo vel fringilla est ullamcorper eget nulla. Donec ac odio tempor orci dapibus. Tristique nulla aliquet enim tortor. Non odio euismod lacinia at. Mauris rhoncus aenean vel elit scelerisque mauris pellentesque. Amet consectetur adipiscing elit ut. Amet consectetur adipiscing elit duis tristique sollicitudin. Sed felis eget velit aliquet sagittis id consectetur purus. Consequat interdum varius sit amet mattis vulputate enim. Lectus magna fringilla urna porttitor. Proin nibh nisl condimentum id venenatis a condimentum vitae. Diam quam nulla porttitor massa id neque. Vulputate mi sit amet mauris commodo quis imperdiet massa. Vel turpis nunc eget lorem.</p>\r\n<p>Nunc scelerisque viverra mauris in. Lacus laoreet non curabitur gravida arcu ac tortor dignissim. Nibh praesent tristique magna sit amet purus gravida. Tincidunt ornare massa eget egestas purus viverra accumsan. Enim ut sem viverra aliquet eget. Aliquam ut porttitor leo a. Elementum pulvinar etiam non quam lacus suspendisse faucibus interdum posuere. Sit amet volutpat consequat mauris nunc. Nullam ac tortor vitae purus faucibus ornare suspendisse sed nisi. Aenean vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant. Consectetur lorem donec massa sapien faucibus et. Interdum consectetur libero id faucibus nisl. Mauris in aliquam sem fringilla ut morbi tincidunt. Id velit ut tortor pretium viverra. Sem fringilla ut morbi tincidunt augue interdum velit euismod in. Velit sed ullamcorper morbi tincidunt ornare massa.</p>\r\n<p>Sed egestas egestas fringilla phasellus faucibus scelerisque. Morbi tristique senectus et netus et malesuada fames ac. Nam aliquam sem et tortor consequat. Vivamus arcu felis bibendum ut tristique et egestas. Pellentesque adipiscing commodo elit at imperdiet dui accumsan sit. Euismod elementum nisi quis eleifend. Eget magna fermentum iaculis eu non. Leo duis ut diam quam nulla. Ut aliquam purus sit amet. Duis ut diam quam nulla porttitor massa.</p>\r\n<p>Odio eu feugiat pretium nibh ipsum consequat nisl. Dignissim convallis aenean et tortor at. Nibh cras pulvinar mattis nunc sed blandit libero volutpat sed. Magna fringilla urna porttitor rhoncus dolor purus non enim praesent. Tempus iaculis urna id volutpat lacus laoreet non curabitur. Mauris pellentesque pulvinar pellentesque habitant morbi. Morbi tristique senectus et netus et malesuada fames. Convallis posuere morbi leo urna molestie. Eu tincidunt tortor aliquam nulla facilisi cras. Auctor eu augue ut lectus arcu bibendum at varius.</p>\r\n<p>Malesuada bibendum arcu vitae elementum curabitur vitae. Lobortis scelerisque fermentum dui faucibus. Orci eu lobortis elementum nibh tellus molestie nunc non blandit. Velit egestas dui id ornare arcu odio. Vehicula ipsum a arcu cursus vitae congue mauris. Aliquam sem fringilla ut morbi. Quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus. Quam viverra orci sagittis eu volutpat. Nunc eget lorem dolor sed viverra ipsum nunc. Purus sit amet volutpat consequat. Tincidunt vitae semper quis lectus nulla at volutpat diam. Maecenas accumsan lacus vel facilisis volutpat est velit egestas dui.</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-sed-do-eiusmod-tempor-incididunt-ut-labore-et-dolore-ma', 1, 0, '["lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-sed-do-eiusmod-tempor-incididunt-ut-labore-et-dolore-ma-1.jpeg"]', 1566548590, 4, NULL),
(35, 1, ',,1566563600', 'Soi Cầu XSMB 22/8/2019', 'test bài viết', '<p>Soi cầu XSMB 22/8/2019</p>', 'test Soi Cầu XSMB 22/8/2019', 'test bài viết', 'test bài viết', 'soi-cau-xsmb-2282019', 0, 0, '["testtest-1.jpeg"]', 1566564345, 5, 0),
(36, 1, ',1566563676', 'Soi cầu xsmn 22/8/2019', 'Soi cầu xsmn 22/8/2019', '<p>Soi cầu xsmn 22/8/2019</p>', 'Soi cầu xsmn 22/8/2019', 'Soi cầu xsmn 22/8/2019', 'Soi cầu xsmn 22/8/2019', 'soi-cau-xsmn-2282019', 0, 0, '["soi-cau-xsmn-2282019-1.jpeg"]', 1566563738, 3, NULL),
(37, 1, ',1566564169', 'Soi cầu XSMT 22/8/2019', 'Soi cầu XSMT 22/8/2019', '<p>Soi cầu XSMT 22/8/2019</p>', 'Soi cầu XSMT 22/8/2019', 'Soi cầu XSMT 22/8/2019', 'Soi cầu XSMT 22/8/2019', 'soi-cau-xsmt-2282019', 1, 0, '["soi-cau-xsmt-2282019-1.jpeg"]', 1566564332, 7, NULL);
INSERT INTO `tp_posts` (`post_id`, `admin_id`, `cate_id`, `post_title`, `post_page_title`, `post_contents`, `post_compact`, `post_meta_description`, `post_meta_keywords`, `post_seo_url`, `post_allow_comment`, `post_automatic`, `post_thumbnail`, `post_time`, `post_view_count`, `post_time_to_post`) VALUES
(38, 1, ',1566564169', 'Soi cầu XSMT 23/8/2019', 'soi-cau-xsmt-23/8/2019', '<p><em><strong>Soi cầu XSMT miễn ph&iacute; mỗi ng&agrave;y tại Win2888</strong></em>&nbsp;do c&aacute;c chuy&ecirc;n gia&nbsp;<strong>soi cầu Win2888</strong>&nbsp;thực hiện bằng c&aacute;c phương ph&aacute;p soi cầu bạch thủ si&ecirc;u chuẩn miễn ph&iacute;&nbsp;<strong>Soi Cầu Miền Trung</strong>. C&aacute;c bạn kh&ocirc;ng cần phải mua số đ&aacute;nh l&ocirc; đề h&agrave;ng ng&agrave;y, h&atilde;y tham khảo Admin chốt số miền trung tại Win2888.</p>\r\n<p><iframe src="//www.youtube.com/embed/pBM8G3xZCZw" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>\r\n<p><iframe src="//www.youtube.com/embed/pBM8G3xZCZw" width="560" height="314" allowfullscreen="allowfullscreen"></iframe></p>\r\n<h2><strong>Soi cầu XSMT miễn ph&iacute; mỗi ng&agrave;y tại Win2888</strong></h2>\r\n<figure id="attachment_126" class="wp-caption aligncenter"><img class="wp-image-126 size-full b-loaded" title="Soi cầu XSMT miễn ph&iacute; mỗi ng&agrave;y tại Win2888" src="https://soicauwin2888.com/wp-content/uploads/2018/06/soi-cau-xsmt-mien-phi-moi-ngay-tai-win2888.jpg" sizes="(max-width: 1000px) 100vw, 1000px" srcset="https://soicauwin2888.com/wp-content/uploads/2018/06/soi-cau-xsmt-mien-phi-moi-ngay-tai-win2888.jpg 1000w, https://soicauwin2888.com/wp-content/uploads/2018/06/soi-cau-xsmt-mien-phi-moi-ngay-tai-win2888-300x200.jpg 300w, https://soicauwin2888.com/wp-content/uploads/2018/06/soi-cau-xsmt-mien-phi-moi-ngay-tai-win2888-768x512.jpg 768w" alt="Soi cầu XSMT miễn ph&iacute; mỗi ng&agrave;y tại Win2888" width="1000" height="667" />\r\n<figcaption class="wp-caption-text">Soi cầu XSMT miễn ph&iacute; mỗi ng&agrave;y tại Win2888</figcaption>\r\n</figure>\r\n<p><strong>L&ocirc; đề miền trung</strong>&nbsp;hiện tại c&oacute; đến 14 đ&agrave;i xổ số thay phi&ecirc;n nhau cho kết quả h&agrave;ng ng&agrave;y. Ch&uacute;ng ta c&oacute; rất nhiều sự lựa chọn đ&agrave;i để đ&aacute;nh l&ocirc; đề. Cũng ch&iacute;nh v&igrave; qu&aacute; nhiều sự lựa chọn c&aacute;c đ&agrave;i để đ&aacute;nh, v&igrave; vậy khi t&igrave;m kiếm những cặp số chuẩn nhất kh&ocirc;ng phải điều dễ d&agrave;ng.</p>\r\n<p>Hiểu được vấn đề n&agrave;y, c&aacute;c&nbsp;<strong>chuy&ecirc;n gia soi cầu Win2888</strong>&nbsp;đ&atilde; thực hiện&nbsp;<strong>Soi Cầu XSMT</strong>&nbsp;miễn ph&iacute; mỗi ng&agrave;y để c&aacute;c bạn c&oacute; thể tham khảo những cặp số đẹp nhất. Ngay b&ecirc;n dưới, mời c&aacute;c bạn c&ugrave;ng tham khảo chuy&ecirc;n mục Soi cầu miền Trung của ch&uacute;ng t&ocirc;i.</p>\r\n<p><strong>Xem ngay:</strong>&nbsp;<strong>Soi cầu XSMT &ndash; Chốt số miền Trung h&agrave;ng ng&agrave;y Miễn Ph&iacute;.</strong></p>\r\n<h3><strong>Đ&aacute;nh l&ocirc; đề online Win2888</strong></h3>\r\n<p>Nh&agrave; c&aacute;i l&ocirc; đề Win2888 lu&ocirc;n sẵn s&agrave;ng đ&oacute;n ch&agrave;o th&agrave;nh vi&ecirc;n mới đ&aacute;nh l&ocirc; đề miền Trung trực tuyến. Với tỉ lệ thắng l&ecirc;n đến đề 1 ăn 99,5 si&ecirc;u lợi nhuận. Admin chắc chắn c&aacute;c bạn sẽ kh&oacute; l&ograve;ng bỏ qua khi chơi l&ocirc; đề.</p>\r\n<p><img class="aligncenter size-full wp-image-125 b-loaded" src="https://soicauwin2888.com/wp-content/uploads/2018/06/dang-ky-win2888.png" alt="đăng k&yacute; win2888" width="300" height="83" /></p>\r\n<p><strong>Xem th&ecirc;m:&nbsp;Hướng dẫn đ&aacute;nh l&ocirc; đề online Win2888 1 ăn 99,5</strong></p>\r\n<p>B&ecirc;n cạnh đ&oacute;, để hỗ trợ anh em kiếm tiền triệu mỗi ng&agrave;y th&igrave; Admin cũng chia sẻ c&aacute;ch l&agrave;m chủ l&ocirc; thầu đề 100% kiếm tiền dễ d&agrave;ng c&ugrave;ng&nbsp;<strong>nh&agrave; c&aacute;i L&ocirc; đề Win2888</strong>. Mời anh em xem chi tiết b&ecirc;n dưới.</p>\r\n<p><strong>✅&nbsp;Bạn muốn Thầu Đề nhận hoa hồng Khủng =&gt; Đăng k&yacute; tại đ&acirc;y.&nbsp;✅</strong></p>\r\n<h3><strong>Kết Th&uacute;c</strong></h3>\r\n<p><strong>Soi Cầu XSMT miễn ph&iacute; mỗi ng&agrave;y tại Win2888</strong>&nbsp;l&agrave; c&ocirc;ng việc thường nhật của c&aacute;c chuy&ecirc;n gia&nbsp;<strong>soi cầu Win2888</strong>. Trong b&agrave;i viết n&agrave;y Admin đ&atilde; hướng dẫn chi tiết c&aacute;c bạn c&aacute;ch để t&igrave;m kiếm c&aacute;c cặp số đẹp đ&aacute;nh đề miền Trung. B&ecirc;n cạnh đ&oacute; l&agrave; giới thiệu&nbsp;<strong>Win2888</strong>&nbsp;đ&aacute;nh l&ocirc; đề si&ecirc;u lợi nhuận. Ch&uacute;c c&aacute;c bạn may mắn thắng lớn.</p>', 'Soi cầu XSMT 22/8/2019', 'soi-cau-xsmt-23/8/2019', 'soi-cau-xsmt-23/8/2019', 'soi-cau-xsmt-2382019', 1, 0, '["soi-cau-xsmt-2382019-1.jpeg"]', 1566566340, 11, 0),
(39, 1, ',1566398192', 'Hướng dẫn nạp tiền tại Win2888', 'Hướng dẫn nạp tiền tại Win2888', '<p><em><strong>Hướng dẫn nạp tiền tại Win2888</strong></em>&nbsp;sau đ&acirc;y sẽ gi&uacute;p anh em biết c&aacute;ch l&agrave;m chủ hệ thống, tr&aacute;nh được những sai s&oacute;t đ&aacute;ng tiếc. V&igrave; thế, d&ugrave; bận mấy đi chăng nữa, h&atilde;y đọc thật kỹ bạn nh&eacute;!</p>\r\n<h2><strong>Hướng dẫn nạp tiền tại Win2888 chỉ trong 5 ph&uacute;t đơn giản, ch&iacute;nh x&aacute;c</strong></h2>\r\n<figure id="attachment_277" class="wp-caption aligncenter"><img class="wp-image-277 size-full b-loaded" title="Hướng dẫn nạp tiền tại Win2888" src="https://swin2888.com/wp-content/uploads/2018/05/huong-dan-nap-tien-tai-win2888.jpg" sizes="(max-width: 991px) 100vw, 991px" srcset="https://swin2888.com/wp-content/uploads/2018/05/huong-dan-nap-tien-tai-win2888.jpg 991w, https://swin2888.com/wp-content/uploads/2018/05/huong-dan-nap-tien-tai-win2888-300x150.jpg 300w, https://swin2888.com/wp-content/uploads/2018/05/huong-dan-nap-tien-tai-win2888-768x383.jpg 768w" alt="Hướng dẫn nạp tiền Win2888" width="991" height="494" />\r\n<figcaption class="wp-caption-text">Hướng dẫn nạp tiền Win2888</figcaption>\r\n</figure>\r\n<p>Để c&oacute; thể&nbsp;<strong>nạp tiền v&agrave;o hệ thống Win2888</strong>&nbsp;c&oacute; rất nhiều c&aacute;ch:</p>\r\n<h2><strong><em>Hướng dẫn nạp tiền Win2888</em></strong></h2>\r\n<ul>\r\n<li>C&aacute;ch thứ nhất, bạn truy cập v&agrave;o trang&nbsp;<strong>win2888.com</strong>&nbsp;sau đ&oacute; t&igrave;m đến mục tr&ograve; chuyện. Tại đ&acirc;y bạn c&oacute; thể xin số t&agrave;i khoản của nh&acirc;n vi&ecirc;n trực hệ thống.</li>\r\n</ul>\r\n<figure id="attachment_281" class="wp-caption aligncenter"><img class="size-full wp-image-281 b-loaded" src="https://swin2888.com/wp-content/uploads/2018/05/huong-dan-nap-tien-win2888.jpg" sizes="(max-width: 468px) 100vw, 468px" srcset="https://swin2888.com/wp-content/uploads/2018/05/huong-dan-nap-tien-win2888.jpg 468w, https://swin2888.com/wp-content/uploads/2018/05/huong-dan-nap-tien-win2888-283x300.jpg 283w" alt="Chat now với nh&acirc;n vi&ecirc;n hỗ trợ trực tuyến win2888" width="468" height="496" />\r\n<figcaption class="wp-caption-text">Chat now với nh&acirc;n vi&ecirc;n hỗ trợ trực tuyến Win2888</figcaption>\r\n</figure>\r\n<ul>\r\n<li>C&aacute;ch thứ hai, bạn gọi điện v&agrave;o số hotline của hệ thống để xin số t&agrave;i khoản.&nbsp;(VN) +84 12 3333 2888 / +84 16 7979 2888 / +84 16 7978 2888 / +84 12 8838 2888</li>\r\n</ul>\r\n<h3><strong>Gồm 3 c&aacute;ch nạp tiền v&agrave;o Win2888.</strong></h3>\r\n<ol>\r\n<li><em>Chuyển tiền trực tiếp quầy giao dịch ng&acirc;n h&agrave;ng địa phương.</em></li>\r\n<li><em>Chuyển tiền trực tiếp tại trụ c&acirc;y ATM</em></li>\r\n<li><em>Chuyển tiền internet banking</em></li>\r\n</ol>\r\n<p>Sau khi bạn c&oacute; số t&agrave;i khoản chuyển tiền sang nh&agrave; c&aacute;i đ&atilde; cung cấp cho bạn số t&agrave;i khoản th&igrave; c&oacute; 3 h&igrave;nh thức như đ&atilde; kể tr&ecirc;n.</p>\r\n<ul>\r\n<li>Sau đ&oacute;,bạn đăng nhập v&agrave;o hệ thống, chọn số dư nạp tiền v&agrave; tiến h&agrave;nh điền c&aacute;c th&ocirc;ng tin: mật khẩu, số tiền nạp,&hellip;theo y&ecirc;u cầu của hệ thống.</li>\r\n</ul>\r\n<figure id="attachment_283" class="wp-caption aligncenter"><img class="size-full wp-image-283 b-loaded" src="https://swin2888.com/wp-content/uploads/2018/05/dang-nhap-nap-tien-win2888.jpg" sizes="(max-width: 889px) 100vw, 889px" srcset="https://swin2888.com/wp-content/uploads/2018/05/dang-nhap-nap-tien-win2888.jpg 889w, https://swin2888.com/wp-content/uploads/2018/05/dang-nhap-nap-tien-win2888-300x59.jpg 300w, https://swin2888.com/wp-content/uploads/2018/05/dang-nhap-nap-tien-win2888-768x151.jpg 768w" alt="Đăng nhập giao diện nạp ti&ecirc;n v&agrave;o Win2888" width="889" height="175" />\r\n<figcaption class="wp-caption-text">Đăng nhập giao diện nạp ti&ecirc;n v&agrave;o Win2888</figcaption>\r\n</figure>\r\n<ul>\r\n<li>Ở phần mật khẩu: bạn điền mật khẩu m&igrave;nh d&ugrave;ng để đăng nhập v&agrave;o hệ thống.</li>\r\n<li>Ở phần tiền tệ hệ thống đ&atilde; chọn sẵn l&agrave; VND như khi bạn&nbsp;<strong>đăng k&yacute; l&agrave;m th&agrave;nh vi&ecirc;n Win2888&nbsp;</strong>khai b&aacute;o.</li>\r\n<li>Ở&nbsp; phần số dự: Số tiền bạn nạp về t&agrave;i khoản. V&iacute; dụ 300.000đ bạn điền l&agrave; 300 ( bỏ bớt 3 con số 0 ).</li>\r\n<li>Phương thức thanh to&aacute;n: Bạn chuyển sang &ldquo;Ng&acirc;n h&agrave;ng địa phương&rdquo;.</li>\r\n<li>Ng&acirc;n h&agrave;ng của bạn: Chọn ng&acirc;n h&agrave;ng vietcombank.</li>\r\n<li>Số t&agrave;i khoản ng&acirc;n h&agrave;ng của bạn: Nhập số t&agrave;i khoản của người gửi.</li>\r\n<li>T&ecirc;n t&agrave;i khoản ng&acirc;n h&agrave;ng của bạn: Họ v&agrave; t&ecirc;n người đi gửi.</li>\r\n<li>T&ecirc;n t&agrave;i khoản C&ocirc;ng Ty: Họ t&ecirc;n t&agrave;i khoản ng&acirc;n h&agrave;ng b&ecirc;n Win2888 đ&atilde; cung cấp cho bạn khi chuyển.</li>\r\n<li>Ghi ch&uacute;: Nội dung chuyển tiền sang nh&agrave; c&aacute;i ( vidu: l&uacute;c chuyển bạn để nội dung l&agrave;&nbsp;nap tien win2888&nbsp;bạn điền v&agrave;o &ocirc; ghi ch&uacute;)</li>\r\n</ul>\r\n<p>Sau khi điền đầy đủ th&ocirc;ng tin ch&iacute;nh x&aacute;c bạn bấm &ldquo;<strong>Gửi</strong>&ldquo;. Bạn chờ khoảng 3 đến 5 ph&uacute;t l&agrave; tiền sẽ tự động v&agrave;o t&agrave;i khoản</p>\r\n<p>Bạn h&atilde;y cẩn trọng trong việc khai b&aacute;c c&aacute;c th&ocirc;ng tin. Điều n&agrave;y sẽ gi&uacute;p bản th&acirc;n tr&aacute;nh được c&aacute;c rắc rối khi nhầm lẫn xảy ra, khiến việc chơi cược bị cản trở v&igrave; những l&yacute; do kh&ocirc;ng đ&aacute;ng c&oacute;.</p>\r\n<h3><strong>V&iacute; dụ cụ thể để anh em hiểu hơn về thao t&aacute;c nạp tiền tr&ecirc;n Win2888</strong></h3>\r\n<ul>\r\n<li>Bạn đăng nhập v&agrave;o hệ thống như đ&atilde; hướng dẫn kể tr&ecirc;n.</li>\r\n<li>Chẳng hạn bạn muốn nạp 300.000đ từ t&agrave;i khoản Vietcombank.</li>\r\n<li>Bạn tiến h&agrave;nh điền 300 v&agrave;o &ocirc; số tiền nạp.</li>\r\n<li>Sau đ&oacute; bạn khai b&aacute;o t&ecirc;n ng&acirc;n h&agrave;ng l&agrave; Vietcombank, kế tiếp l&agrave; số t&agrave;i khoản v&agrave; t&ecirc;n tải khoản.</li>\r\n<li>Cuối c&ugrave;ng đừng qu&ecirc;n đề số điện thoại v&agrave; nhấn gửi để hệ thống x&aacute;c nhận.</li>\r\n</ul>\r\n<p>Chỉ trong v&ograve;ng thời gian từ 3-5 ph&uacute;t bạn đ&atilde; c&oacute; thể nhận được th&ocirc;ng b&aacute;o nạp tiền th&agrave;nh c&ocirc;ng. Hiện tại Win2888 c&oacute; li&ecirc;n kết với rất nhiều ng&acirc;n h&agrave;ng địa phương. Do đ&oacute;, bạn c&oacute; thể y&ecirc;n t&acirc;m về độ su&ocirc;n sẻ của giao dịch.</p>\r\n<h4><em><strong>Kết luận:</strong></em></h4>\r\n<p>Hơn thế nữa, Win2888 l&agrave; một trong những nh&agrave; c&aacute;i uy t&iacute;n bậc nhất hiện nay. V&igrave; thế, hệ thống đảm bảo độ minh bạch, c&ocirc;ng bằng gi&uacute;p kh&aacute;ch chơi c&oacute; được sự sảng kho&aacute;i trong qu&aacute; tr&igrave;nh chơi cược.</p>\r\n<p>H&agrave;ng triệu anh em đang c&oacute; những trải nghiệm tuyệt vời tr&ecirc;n hệ thống. Tin rằng bạn cũng kh&ocirc;ng phải ngoại lệ trong số đ&oacute;.</p>\r\n<p>Mong rằng với c&aacute;c th&ocirc;ng tin&nbsp;<strong>hướng dẫn nạp tiền v&agrave; r&uacute;t tiền tr&ecirc;n Win2888&nbsp;</strong>vừa rồi, bạn sẽ nhanh ch&oacute;ng ho&agrave;n tất c&aacute;c điều kiện cần v&agrave; đủ để c&oacute; nhiều trải nghiệm th&uacute; vị tại&nbsp;<strong>SWin2888.com</strong>.</p>', 'Hướng dẫn nạp tiền tại Win2888', 'Hướng dẫn nạp tiền tại Win2888', 'Hướng dẫn nạp tiền tại Win2888', 'huong-dan-nap-tien-tai-win2888', 1, 0, '["huong-dan-nap-tien-tai-win2888-1.jpeg"]', 1566565320, 5, NULL),
(40, 1, ',1566398192', 'Hướng Dẫn Đăng Ký Mở Tài Khoản Mới Tại Win2888 Chính Xác', 'Hướng Dẫn Đăng Ký Mở Tài Khoản Mới Tại Win2888 Chính Xác', '<p><strong><em>Đăng k&yacute; t&agrave;i khoản Win2888 nhanh ch&oacute;ng, dễ d&agrave;ng chỉ với v&agrave;i thao t&aacute;c đơn giản, bạn đ&atilde; l&agrave; th&agrave;nh vi&ecirc;n c&oacute; cơ hội tham gia chơi v&agrave; nhận qu&agrave; khủng c&ugrave;ng Nh&agrave; C&aacute;i Win2888.</em></strong></p>\r\n<h2><strong>Đăng K&yacute; T&agrave;i Khoản Win2888 Năm 2019 Mới Nhất</strong></h2>\r\n<p>Li&ecirc;n tục nhận nhiều c&acirc;u hỏi từ kh&aacute;ch h&agrave;ng về vấn đề&nbsp;<em><strong>đăng k&yacute; t&agrave;i </strong></em><em><strong>k</strong></em><em><strong>hoản tr&ecirc;n Win2888 </strong></em>như: L&agrave;m thế n&agrave;o để đăng k&yacute; th&agrave;nh vi&ecirc;n tại Nh&agrave; C&aacute;i Win2888?, Đăng k&yacute; t&agrave;i khoản Win2888 c&oacute; mất ph&iacute;? Hay đăng k&yacute; Win2888 mất bao l&acirc;u? Tất tần tật sẽ được ch&uacute;ng t&ocirc;i giải đ&aacute;p ngay sau đ&acirc;y.</p>\r\n<p>Chỉ mất chưa đầy 5 ph&uacute;t, bạn đ&atilde; trở th&agrave;nh người chơi tại <a href="https://vwin2888.com/" target="_blank" rel="nofollow noopener noreferrer"><em><strong>Nh&agrave; C&aacute;i Win2888</strong></em></a>. Bằng c&aacute;ch thực hiện đầy đủ 3 bước dưới đ&acirc;y:</p>\r\n<p><strong>Bước 1</strong>: Bạn vui l&ograve;ng Click v&agrave;o link&nbsp;<strong>ĐĂNG K&Yacute; WIN2888</strong>&nbsp;b&ecirc;n dưới</p>\r\n<p style="text-align: center;"><a href="http://www.win2888.com/?af6840&amp;lang=vn/" target="_blank" rel="nofollow noopener noreferrer"><img class="td-animation-stack-type0-1 aligncenter wp-image-2408" src="https://skywin2888.com/wp-content/uploads/2018/01/dang-ky-win2888.gif" alt="dang-ky-win2888" width="353" height="79" /></a></p>\r\n<p>H&atilde;y Click Link=&gt; Click chọn g&oacute;c tr&aacute;i b&ecirc;n m&agrave;n h&igrave;nh =&gt; Chọn quốc kỳ Việt Nam, ngay lập tức sẽ chuyển sang ng&ocirc;n ngữ Tiếng Việt, sẽ dễ d&agrave;ng sử dụng hơn.</p>\r\n<p><strong>Bước 2</strong>: Cung cấp đầy đủ th&ocirc;ng tin c&aacute; nh&acirc;n v&agrave;o Form đăng k&yacute;. Cụ thể:</p>\r\n<p><img class="size-large wp-image-114" src="https://vwin2888.com/wp-content/uploads/2019/07/dang-ky-win2888-1-1024x674.png" alt="" width="1020" height="671" /> bảng đăng k&yacute; win2888[/caption]</p>\r\n<p><strong>+ Họ v&agrave; T&ecirc;n</strong>: Điền họ t&ecirc;n đầy đủ của bạn (NHỚ VIẾT KH&Ocirc;NG DẤU NH&Eacute;)</p>\r\n<p>+&nbsp;<strong>Ng&agrave;y sinh</strong>: Cung cấp ch&iacute;nh x&aacute;c ng&agrave;y sinh c&aacute; nh&acirc;n của bạn, điều n&agrave;y rất cần thiết nhất l&agrave; khi xảy ra sự cố: Qu&ecirc;n mật khẩu, kh&oacute;a t&agrave;i khoản.</p>\r\n<p>+&nbsp;<strong>T&ecirc;n đăng nhập</strong>: Tự cho m&igrave;nh một ID đăng nhập của ri&ecirc;ng m&igrave;nh (Bạn n&ecirc;n chọn ID m&igrave;nh th&iacute;ch, dễ nhớ nhất để tiện đăng nhập nh&eacute;)</p>\r\n<p>+&nbsp;<strong>Mật khẩu</strong>: Đặt tổi thiểu 8 k&iacute; tự, trong đ&oacute; c&oacute; &iacute;t nhất 1 số v&agrave; 1 chữ. V&iacute; dụ: 1234abcd</p>\r\n<p>+&nbsp;<strong>X&aacute;c nhận mật khẩu</strong>: Nhập lại mật khẩu vừa đặt để x&aacute;c nhận th&ecirc;m lần nữa.</p>\r\n<p><strong>+ Số điện thoại</strong>: Bạn nhập số điện thoại thường xuy&ecirc;n li&ecirc;n lạc được (tốt nhất l&agrave; nhập số điện thoại bạn đang d&ugrave;ng nh&eacute;!)</p>\r\n<p>+&nbsp;<strong>Tiền tệ</strong>: L&agrave; người Việt, tốt nhất n&ecirc;n chọn VNĐ để dễ sử dụng</p>\r\n<p>+&nbsp;<strong>Send Verify Code</strong>: Bạn bấm v&agrave;o n&uacute;t n&agrave;y để nhận m&atilde; code qua SMS (tin nhắn sẽ về số điện thoại bạn đ&atilde; nhập ở tr&ecirc;n) từ Nh&agrave; C&aacute;i Win2888. Bạn sẽ nhận được một m&atilde; k&iacute;ch hoạt th&ocirc;ng qua SMS gồm 4 con số như h&igrave;nh minh họa sau:<img class="aligncenter wp-image-3763 size-full td-animation-stack-type0-1" src="https://skywin2888.com/wp-content/uploads/2019/01/code-win2888.jpg" sizes="(max-width: 539px) 100vw, 539px" srcset="https://skywin2888.com/wp-content/uploads/2019/01/code-win2888.jpg 539w, https://skywin2888.com/wp-content/uploads/2019/01/code-win2888-300x162.jpg 300w" alt="ma-win2888" width="539" height="291" /></p>\r\n<p><em>Nhập m&atilde; k&iacute;ch hoạt t&agrave;i khoản Win2888</em></p>\r\n<p><strong>+ Verify Code</strong>: Điền ch&iacute;nh x&aacute;c 4 chữ số m&agrave; Nh&agrave; C&aacute;i Win2888 đ&atilde; gửi qua SMS điện thoại.</p>\r\n<p>+&nbsp;<strong>M&atilde; Đại L&yacute;</strong>:&nbsp;<strong>AF6840</strong>&nbsp;l&agrave;&nbsp;<em><strong>M&atilde; đại l&yacute;</strong></em>&nbsp;Win2888. Đ&acirc;y l&agrave; m&atilde; mặc định do Nh&agrave; C&aacute;i Win2888 cung cấp. Do đ&oacute;, bạn phải nhớ ch&iacute;nh x&aacute;c m&atilde; đại l&yacute; Win2888 để nhận nhiều ưu đ&atilde;i bất ngờ từ Nh&agrave; C&aacute;i nh&eacute;.</p>\r\n<h2><strong>5 Ưu Đ&atilde;i Hấp Dẫn Khi L&agrave; Th&agrave;nh Vi&ecirc;n Ch&iacute;nh Thức Tại Nh&agrave; C&aacute;i Win2888</strong></h2>\r\n<p>+ KH&Ocirc;NG tốn bất k&igrave; khoản ph&iacute; n&agrave;o khi tham gia đăng k&yacute; th&agrave;nh vi&ecirc;n Win2888</p>\r\n<p>+ Hưởng mức hoa hồng cao nhất 1 ăn 99,5 khi tham gia chơi tại Nh&agrave; C&aacute;i&nbsp;<em><strong><a href="https://vi.wikipedia.org/wiki/S%C3%B2ng_b%E1%BA%A1c" target="_blank" rel="nofollow noopener noreferrer">Win2888</a></strong></em></p>\r\n<p>+ T&iacute;ch lũy điểm thưởng, nhận ngay cơ hội tham gia đổi qu&agrave; với nhiều phần qu&agrave; cực gi&aacute; trị:</p>\r\n<ul>\r\n<li>C&aacute;c d&ograve;ng Iphone: Iphone 6, 6s, 7, 8, 8 Plus, X,&hellip;</li>\r\n<li>Trang sức gi&aacute; trị: Nhẫn kim cương, nhẫn hột xo&agrave;n, nhẫn v&agrave;ng,&hellip;</li>\r\n<li>C&aacute;c d&ograve;ng xe m&aacute;y: SH150i, SH300i, Vario 150, Click125i, Honda Lead, Honda MSX, Honda Airblade đời mới nhất,&hellip;</li>\r\n</ul>\r\n<p>+ Khuyến m&atilde;i 50% c&aacute;c g&oacute;i nạp hay nh&acirc;n đ&ocirc;i t&agrave;i khoản c&aacute;c kiểu. Tuyệt vời đ&uacute;ng kh&ocirc;ng n&agrave;o?</p>\r\n<p>+ Nhận ngay cơ hội tham gia chơi c&ugrave;ng Nh&agrave; C&aacute;i Win2888 từ soi cầu,&nbsp;<a href="https://vi.wikipedia.org/wiki/Tr%C3%B2_ch%C6%A1i_b%C3%A0i" target="_blank" rel="nofollow noopener noreferrer"><em><strong>đ&aacute;nh b&agrave;i online</strong></em></a>,&nbsp;<em><strong><a href="https://vi.wikipedia.org/wiki/X%E1%BB%95_s%E1%BB%91" target="_blank" rel="nofollow noopener noreferrer">xổ số</a></strong></em>&nbsp;miền Bắc, xổ số miền Nam đến c&aacute; cược b&oacute;ng đ&aacute;,&hellip; với mức hoa hồng cao chưa từng c&oacute;<a href="http://www.win2888.com/?af6840&amp;lang=vn/" target="_blank" rel="nofollow noopener noreferrer"><img class="wp-image-115 size-full" src="https://vwin2888.com/wp-content/uploads/2019/07/tai-sao-choi-win2888.gif" alt="" width="825" height="623" /></a> Tại sao n&ecirc;n lựa chọn Win2888?</p>\r\n<p>Hi vọng với những hướng dẫn đăng k&yacute; Win2888 m&agrave; Vwin2888 cung cấp, chắc chắn sẽ gi&uacute;p &iacute;ch cho c&aacute;c anh em. Vậy c&ograve;n đợi chờ g&igrave; nữa m&agrave; kh&ocirc;ng nhanh tay đăng k&yacute; th&agrave;nh vi&ecirc;n tại Nh&agrave; C&aacute;i Win2888 để tham gia chơi v&agrave; nhận ngay cơ hội rinh tiền bạc triệu v&agrave;o t&agrave;i khoản chỉ trong t&iacute;ch tắc.</p>\r\n<p>Sau đ&oacute; anh em h&atilde;y&nbsp;<a href="http://www.win2888.com/" target="_blank" rel="nofollow noopener noreferrer"><em><strong>Đăng nhập Win2888</strong></em></a>&nbsp;v&agrave; tận hưởng c&aacute;c tr&ograve; chơi cũng như dịch vụ nơi đ&acirc;y nh&eacute;. Nếu chưa r&otilde; c&aacute;ch đăng nhập th&igrave; anh em h&atilde;y v&agrave;o&nbsp;<em><strong>Hướng dẫn đăng nhập Win2888</strong></em>&nbsp;để được hướng dẫn chi tiết hơn.</p>\r\n<p style="text-align: center;"><img class="aligncenter wp-image-5004 td-animation-stack-type0-1" src="https://skywin2888.com/wp-content/uploads/2019/02/chi-dan-.gif" alt="chi-dan-win2888" width="150" height="90" /></p>\r\n<p><strong>Nhấn đăng k&yacute; Win2888 ngay để tham gia chơi soi cầu online 1 ăn 99,5 rất dễ d&agrave;ng v&agrave; hấp dẫn</strong></p>\r\n<p style="text-align: center;"><a href="http://www.win2888.com/?af6840" target="_blank" rel="nofollow noopener noreferrer"><img class="td-animation-stack-type0-1 aligncenter wp-image-2408" src="https://skywin2888.com/wp-content/uploads/2018/01/dang-ky-win2888.gif" alt="" width="353" height="79" /></a></p>', 'Hướng Dẫn Đăng Ký Mở Tài Khoản Mới Tại Win2888 Chính Xác', 'Hướng Dẫn Đăng Ký Mở Tài Khoản Mới Tại Win2888 Chính Xác', 'Hướng Dẫn Đăng Ký Mở Tài Khoản Mới Tại Win2888 Chính Xác', 'huong-dan-dang-ky-mo-tai-khoan-moi-tai-win2888-chinh-xac', 1, 0, '["huong-dan-dang-ky-mo-tai-khoan-moi-tai-win2888-chinh-xac-1.jpeg"]', 1566565739, 10, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tp_promotion_email`
--

CREATE TABLE IF NOT EXISTS `tp_promotion_email` (
  `mail_id` int(11) NOT NULL,
  `mail_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tp_promotion_email`
--

INSERT INTO `tp_promotion_email` (`mail_id`, `mail_name`) VALUES
(19, '0347793192@gmail.com'),
(9, 'dp0613@yandex.com'),
(20, 'honganh2888@gmail.com'),
(8, 'nguyenkhanhtung580@gmail.com'),
(1, 'tranninhitw@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `tp_settings`
--

CREATE TABLE IF NOT EXISTS `tp_settings` (
  `setting_id` int(11) NOT NULL,
  `setting_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `setting_value` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tp_settings`
--

INSERT INTO `tp_settings` (`setting_id`, `setting_name`, `setting_value`) VALUES
(1, 'ads', '[{"item_id":1559497553038,"item_image":"[\\"ahdau-2-1.jpeg\\"]","item_title":"ahdau","item_alt":"uaduha","item_link":"http:\\/\\/abc.com"},{"item_id":1559497553039,"item_image":"[\\"audhuah-1.jpeg\\"]","item_title":"audhuah","item_alt":"aid","item_link":"http:\\/\\/anjd.com"}]'),
(2, 'default_avatar', '["default_avatar-1.jpeg"]'),
(3, 'company_information', '{"footer_content":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ullamcorper, risus vitae dapibus pharetra, odio purus ultrices justo, ut varius urna eros vitae risus. Nullam non viverra dolor, non accumsan nulla.","ga_code":"UA-146174015-1","main_logo":"[\\"logo_main-1.png\\"]","footer_logo":"[\\"logo_footer-1.png\\"]","game_1":"[\\"game_1-1.png\\"]","game_2":"[\\"game_2-1.png\\"]","game_3":"[\\"game_3-1.png\\"]","game_4":"[\\"banner_s-1.jpeg\\"]"}');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tp_admins`
--
ALTER TABLE `tp_admins`
  ADD PRIMARY KEY (`admin_id`),
  ADD UNIQUE KEY `admin_username` (`admin_username`),
  ADD UNIQUE KEY `admin_email` (`admin_email`),
  ADD UNIQUE KEY `admin_mobile` (`admin_mobile`),
  ADD UNIQUE KEY `admin_resetpass_string` (`admin_resetpass_string`);

--
-- Indexes for table `tp_advertises`
--
ALTER TABLE `tp_advertises`
  ADD PRIMARY KEY (`advertise_id`),
  ADD UNIQUE KEY `advertise_name` (`advertise_name`);

--
-- Indexes for table `tp_cates`
--
ALTER TABLE `tp_cates`
  ADD PRIMARY KEY (`cate_id`),
  ADD UNIQUE KEY `cate_seo_url` (`cate_seo_url`),
  ADD UNIQUE KEY `c_unique` (`cate_name`,`cate_parent`);

--
-- Indexes for table `tp_chats`
--
ALTER TABLE `tp_chats`
  ADD PRIMARY KEY (`chat_id`);

--
-- Indexes for table `tp_comments`
--
ALTER TABLE `tp_comments`
  ADD PRIMARY KEY (`comment_id`);

--
-- Indexes for table `tp_conversations`
--
ALTER TABLE `tp_conversations`
  ADD PRIMARY KEY (`conversation_id`);

--
-- Indexes for table `tp_marketing_email`
--
ALTER TABLE `tp_marketing_email`
  ADD PRIMARY KEY (`marketing_email_id`);

--
-- Indexes for table `tp_menu`
--
ALTER TABLE `tp_menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `tp_posts`
--
ALTER TABLE `tp_posts`
  ADD PRIMARY KEY (`post_id`),
  ADD UNIQUE KEY `post_seo_url` (`post_seo_url`);

--
-- Indexes for table `tp_promotion_email`
--
ALTER TABLE `tp_promotion_email`
  ADD PRIMARY KEY (`mail_id`),
  ADD UNIQUE KEY `mail_name` (`mail_name`);

--
-- Indexes for table `tp_settings`
--
ALTER TABLE `tp_settings`
  ADD PRIMARY KEY (`setting_id`),
  ADD UNIQUE KEY `setting_name` (`setting_name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tp_admins`
--
ALTER TABLE `tp_admins`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tp_advertises`
--
ALTER TABLE `tp_advertises`
  MODIFY `advertise_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tp_chats`
--
ALTER TABLE `tp_chats`
  MODIFY `chat_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tp_comments`
--
ALTER TABLE `tp_comments`
  MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tp_conversations`
--
ALTER TABLE `tp_conversations`
  MODIFY `conversation_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tp_marketing_email`
--
ALTER TABLE `tp_marketing_email`
  MODIFY `marketing_email_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tp_posts`
--
ALTER TABLE `tp_posts`
  MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `tp_promotion_email`
--
ALTER TABLE `tp_promotion_email`
  MODIFY `mail_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `tp_settings`
--
ALTER TABLE `tp_settings`
  MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
