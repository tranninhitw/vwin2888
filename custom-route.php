<?php
    /**
     * TAKA+ v1.0
     * @package takaplus
     * @date 12.1.2016
     * @version 0.0.1
     * @website
     *
     * THÊM CÁC ROUTE CỦA BẠN BÊN DƯỚI
     *
    */

    $route -> add('', 'public/home.home/index', 'public/home');
    $route -> add_dynamic('SELECT post_seo_url FROM tp_posts;', 'public/home.detail/index', 'public/home');
    $route -> add_dynamic('SELECT cate_seo_url FROM tp_cates;', 'public/home.category/index', 'public/home');

    // Comment plugin
    $route -> add( 'include/14', 'comment/comment-handle.inc.php' );




    /**
     * Kết thúc phần custom route
     *
     * CODING OR DOING AN EXCEPTION ...
    */
?>
