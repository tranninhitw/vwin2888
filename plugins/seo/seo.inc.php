<?php
    $default = array(
        'title' => 'Soi kèo, giải mã giấc mơ | VWin2888',
        'description' => 'Trang hướng dẫn cá cược tại Win2888',
        'keywords' => 'win2888, cá cược, bóng đá, soi kèo, giải mã giấc mơ'
    );
    $seoData = isset($_SESSION['seo']) ? $_SESSION['seo'] : $default;
    $html = "
        <title>{$seoData['title']}</title>
        <meta name='description' content='{$seoData['description']}' />
        <meta name='keywords' content='{$seoData['keywords']}' />
        <meta property='og:title' content='{$seoData['title']}' />
        <meta property='og:type' content='{$seoData['keywords']}' />
        <meta property='og:description' content='{$seoData['description']}' />
    ";

    echo $html;
?>
