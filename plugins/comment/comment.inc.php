<?php
    $exec = new Exec( HOST, USER, PASS, DBNAME );
    if( isset( $_SESSION['tk_user_data'] ) ) {
        $user = unserialize( $_SESSION['tk_user_data'] );
        $fullname = $user['admin_fullname'];
        $username = $user['admin_username'];
        $email = $user['admin_email'];
        $readonly = 'readonly';
    } else {
        $fullname = $username = $email = $readonly = '';
    }

    $data_img = array(
        'setting_name' => 'default_avatar'
    );
    $sql = "SELECT setting_value FROM tp_settings WHERE setting_name = :setting_name;";
    $img = $exec -> get( $sql, $data_img );
    $avatar = $img[0]['setting_value'];
    $avatar = json_decode( $avatar, true );
    $avatar = $avatar[0];
    $comment_url = $_GET['params'];
    $name = ( $fullname == '' ) ? $username : $fullname ;
    $html = '
    <div class="row">
        <div class="col-12 chat-box">
            <h2 class="comment-heading">Để lại ý kiến của bạn:</h2>
            <form class="js_comment-form" action="" method="post">
                <input type="hidden" name="comment_url" value="' . $comment_url . '" />
                <input type="hidden" name="comment_parent" value=0 />
                <input type="text" name="comment_author_name" value="' . $name . '" placeholder="Nhập tên" ' . $readonly . ' required/>
                <input type="email" name="comment_author_email" value="' . $email . '" placeholder="Nhập email" ' . $readonly . ' required oninvalid="this.setCustomValidity(\'Vui lòng điền email\')" oninput="this.setCustomValidity(\'\')"/>
                <textarea placeholder="Nhập ý kiến của bạn" name="comment_contents" required></textarea>
                <input type="submit" name="submit" value="Gửi" disabled/>
            </form>
        </div>
    </div>
    <div class="js_comment-list">
    ';

    $data_post = array(
        'comment_url' => $comment_url
    );
    $sql = "SELECT * FROM tp_comments WHERE comment_url = :comment_url AND comment_parent = 0 AND comment_publish = 1;";
    $cmt_arr = $exec -> get( $sql, $data_post );
    foreach( $cmt_arr as $key => $c ) {
        $html .= '
            <div class="row">
                <div class="col-12 comment mt-20">
                    <div class="row">
                        <div class="col-sm-12 col-md-6 comment-content">
                            <div class="avatar">
                                <img src="' . TP_REL_ROOT . '/uploads/public/' . $avatar . '" alt="avatar"/>
                            </div>
                            <div class="info">
                                <span class="user">' . $c['comment_author_name'] . '</span>
                                <span class="time"><i>' . date( "H:i:s d-m-Y", $c['comment_time'] ) . '</i></span>
                            </div>
                            ' . $c['comment_contents'] . '
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 reply">
                            <span><i>Trả lời</i></span>
                        </div>
                    </div>
                    <div class="reply-box">
                        <div class="row reply-input">
                            <div class="col-12 chat-box-reply">
                                <form class="js_comment-form" action="" method="post">
                                    <input type="hidden" name="comment_url" value="' . $comment_url . '" />
                                    <input type="hidden" name="comment_parent" value="' . $c['comment_id'] . '" />
                                    <input type="text" name="comment_author_name" value="' . $fullname . '" placeholder="Nhập tên" ' . $readonly . ' required/>
                                    <input type="email" name="comment_author_email" value="' . $email . '" placeholder="Nhập email" ' . $readonly . ' required oninvalid="this.setCustomValidity(\'Vui lòng điền email\')" oninput="this.setCustomValidity(\'\')"/>
                                    <textarea placeholder="Nhập bình luận" name="comment_contents" required></textarea>
                                    <input type="submit" name="submit" value="Gửi" disabled/>
                                </form>
                            </div>
                        </div>';
                        $data_cmt_child = array(
                            'comment_id' => $c['comment_id']
                        );
                        $sql = "SELECT * FROM tp_comments WHERE comment_parent = :comment_id AND comment_publish = 1;";
                        $cmt_child = $exec -> get( $sql, $data_cmt_child );
                        foreach( $cmt_child as $key => $cc ) {
                            $html .= '
                                <div class="row">
                                    <div class="col-12 comment-reply">
                                        <div class="avatar-reply">
                                            <img src="' . TP_REL_ROOT . '/uploads/public/' . $avatar . '" alt="avatar"/>
                                        </div>
                                        <div class="info-reply">
                                            <span class="user">' . $cc['comment_author_name'] . '</span>
                                            <span class="time"><i>' . date( "H:i:s d-m-Y", $cc['comment_time'] ) . '</i></span>
                                        </div>
                                        ' . $cc['comment_contents'] . '
                                    </div>
                                </div>
                            ';
                        };
                        $html .= '
                    </div>
                </div>
            </div>
        ';
    };

    echo $html . '</div>';
?>
