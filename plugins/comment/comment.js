$( document ).ready( function() {
    $( 'textarea' ).each( function() {
        this.setAttribute( 'style', 'height:' + ( this.scrollHeight) + 'px;overflow-y:hidden;' );
    } ).on( 'input', function() {
        this.style.height = 'auto';
        this.style.height = ( this.scrollHeight ) + 'px';
    });

    $( document ).on( 'input change', '.chat-box input, .chat-box-reply input, .chat-box textarea, .chat-box-reply textarea', function(){
        $( this ).parent().find(":submit").removeAttr('disabled');
    });

    $( document ).on( 'click', '.reply span', function() {
        $( this ).parent().parent().next().find( '.reply-input' ).show();
    } );

    $(document).on('submit', '.js_comment-form', function(e) {
        e.preventDefault();

        $.ajax({
            url: 'include/14',
            type: 'POST',
            data: $(this).serialize(),
            success: function() {
                $(".js_comment-list").empty().load(location.href + ' .js_comment-list > *');
            }
        });
    });
} )
