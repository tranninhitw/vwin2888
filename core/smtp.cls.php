<?php
    class Smtp {

        public function send( $to, $obj, $msg ) {
            if( TP_DISABLE_SMTP ) {
                return mail( $to, $obj, $msg );
            }

            // Config params
            $cfgParams = unserialize(TP_MAIL_SMTP_PARAMS);

            // Instantiation and passing `true` enables exceptions
            require(TP_REL_CORE . '/PHPMailer/src/Exception.php');
            require(TP_REL_CORE . '/PHPMailer/src/PHPMailer.php');
            require(TP_REL_CORE . '/PHPMailer/src/SMTP.php');

            $mail = new \PHPMailer\PHPMailer\PHPMailer(true);

            try {
                //Server settings
                $mail->SMTPDebug = 0;
                $mail->isSMTP();
                $mail->CharSet = 'UTF-8';
                $mail->Encoding = 'base64';
                $mail->SMTPOptions = array('ssl' => array('verify_peer_name' => false));
                $mail->Host       = gethostbyname($cfgParams['host']);
                $mail->SMTPAuth   = true;
                $mail->Username   = $cfgParams['username'];
                $mail->Password   = $cfgParams['password'];
                $mail->SMTPSecure = $cfgParams['secure'];
                $mail->Port       = $cfgParams['port'];

                //Recipients
                $mail->setFrom($cfgParams['from'], 'VWin2888');
                $mail->addAddress($to);
                $mail->addReplyTo($cfgParams['replyto'], 'VWin2888');
                // $mail->addCC('cc@example.com');
                // $mail->addBCC('bcc@example.com');

                // Attachments
                // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
                // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

                // Content
                $mail->isHTML(true);
                $mail->Subject = $obj;
                $mail->Body    = $msg;
                $mail->AltBody = strip_tags($msg);

                $mail->send();

                return true;
            } catch (Exception $e) {
                echo "Lỗi gửi mail: {$mail->ErrorInfo}";
                return false;
            }
        }

    }
?>
