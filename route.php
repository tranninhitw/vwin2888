<?php
    /**
     * TAKA+ v1.0
     * @package takaplus
     * @date 12.1.2016
     * @version 0.0.1
     * @website
     *
     * FILE CỦA HỆ THỐNG
     * TUYỆT ĐỐI KHÔNG CHỈNH SỬA
     *
    */

    $route = new Route();

    /**
     * Load các custom route
    */
    require_once( 'custom-route.php' );

    /**
     * Route of autho
    */
    $route -> add( 'nguoidung/dangnhap', 'system/autho.login/index', 'system/autho' ); //Login page
    $route -> add( 'nguoidung/quenmatkhau', 'system/autho.resetpass/index', 'system/autho' );
    $route -> add( 'nguoidung/doimatkhau', 'system/autho.changepass/index', 'system/autho' );
    $route -> add( 'admin/dangxuat', 'system/autho.login/logout', 'system/autho' );

    /**
     * Route of cpanel
    */
    //Overview
    $route -> add( 'admin', 'system/cpanel.overview/index', 'system/cpanel' );

    //Post
    $route -> add( 'admin/noidung/baiviet', 'system/cpanel.content.post/index', 'system/cpanel' );
    $route -> add( 'admin/noidung/binhluan', 'system/cpanel.content.comment/index', 'system/cpanel' );
    $route -> add( 'admin/noidung/danhmuc', 'system/cpanel.content.category/index', 'system/cpanel' );
    $route -> add( 'admin/noidung/emailmarketing', 'system/cpanel.content.email/index', 'system/cpanel' );

    //System
    $route -> add( 'admin/caidat/caidatchung', 'system/cpanel.setting.general/index', 'system/cpanel' );
    $route -> add( 'admin/caidat/taikhoan', 'system/cpanel.setting.account/index', 'system/cpanel' );
    $route -> add( 'admin/caidat/quangcao', 'system/cpanel.setting.ads/index', 'system/cpanel' );
    $route -> add( 'admin/caidat/menu', 'system/cpanel.setting.menu/index', 'system/cpanel' );

    //Personal
    $route -> add( 'admin/canhan', 'system/cpanel.personal/index', 'system/cpanel' );

    /**
     * Route of *.inc.php
     */

    // autho
    $route -> add( 'include/3', 'system/autho.login/index.inc.php' );
    $route -> add( 'include/4', 'system/autho.resetpass/index.inc.php' );
    $route -> add( 'include/5', 'system/autho.changepass/index.inc.php' );

    // Uploader
    $route -> add( 'include/6', 'system/uploader/uploader.inc.php' );

    // cpanel.content.category
    $route -> add( 'include/1', 'system/cpanel.content.category/save.inc.php' );
    $route -> add( 'include/2', 'system/cpanel.content.category/delete.inc.php' );

    // cpanel.content.post
    $route -> add( 'include/26', 'system/cpanel.content.post/save.inc.php' );
    $route -> add( 'include/29', 'system/cpanel.content.post/delete.inc.php' );
    $route -> add( 'include/30', 'system/cpanel.content.post/list.inc.php' );

    // cpanel.content.comment
    $route -> add( 'include/31', 'system/cpanel.content.comment/list.inc.php' );
    $route -> add( 'include/32', 'system/cpanel.content.comment/block.inc.php' );

    // cpanel.setting.general
    $route -> add( 'include/44', 'system/cpanel.setting.general/save.inc.php' );

    // cpanel.setting.account
    $route -> add( 'include/37', 'system/cpanel.setting.account/save.inc.php' );
    $route -> add( 'include/38', 'system/cpanel.setting.account/block.inc.php' );
    $route -> add( 'include/45', 'system/cpanel.setting.account/delete.inc.php' );

    // cpanel.setting.ads
    $route -> add( 'include/39', 'system/cpanel.setting.ads/delete.inc.php' );
    $route -> add( 'include/40', 'system/cpanel.setting.ads/save.inc.php' );

    // cpanel.setting.menu
    $route -> add( 'include/8', 'system/cpanel.setting.menu/save.inc.php' );
    $route -> add( 'include/9', 'system/cpanel.setting.menu/delete.inc.php' );

    // cpanel.content.email
    $route -> add( 'include/10', 'system/cpanel.content.email/sendmail.inc.php' );
    $route -> add( 'include/11', 'system/cpanel.content.email/sendtestmail.inc.php' );
    $route -> add( 'include/12', 'system/cpanel.content.email/save.inc.php' );
    $route -> add( 'include/13', 'system/cpanel.content.email/delete.inc.php' );

    $route -> scan();
?>
