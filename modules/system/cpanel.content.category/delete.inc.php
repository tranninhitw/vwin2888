<?php
    $exec = new Exec( HOST, USER, PASS, DBNAME );

    if( isset( $_POST['cate_id'] ) ) {

        $data = array(
            'cate_id' => (int)$_POST['cate_id']
        );
        $sql = "DELETE FROM tp_cates WHERE cate_id = :cate_id OR cate_parent = :cate_id;";
        $r = $exec -> exec( $sql, $data );
        $r ? print( '1|Xóa thành công' ) : print( '0|Có lỗi xảy ra' );
    }
?>
