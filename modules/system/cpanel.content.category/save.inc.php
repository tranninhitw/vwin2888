<?php
    if( isset( $_POST['data'] ) ) {

        //Embed class
        $exec = new Exec( HOST, USER, PASS, DBNAME );

        //Save data
        foreach( $_POST['data'] as $key => $arr ) {

            //Get data and remove all special characters
            $cateName = $arr['cate_name'];
            $cateParent = $arr['cate_parent'];
            $dataCateId = $arr['data_cate_id'];
            $cateId = $arr['cate_id'];
            $catePageTitle = $arr['cate_seo_page_title'];
            $cateUrl = $arr['cate_seo_url'];
            $cateMetaDes = $arr['cate_seo_meta_description'];
            $cateMetaKey = $arr['cate_seo_meta_keywords'];
            $cateIsSerie = $arr['cate_is_serie'];

            $cateName = strip_tags( $cateName );
            $catePageTitle = strip_tags( $catePageTitle );
            $cateUrl = strip_tags( $cateUrl );
            $cateMetaDes = strip_tags( $cateMetaDes );
            $cateMetaKey = strip_tags( $cateMetaKey );
            $cateParent = (int)$cateParent;
            $dataCateId = (int)$dataCateId;
            $cateId = (int)$cateId;

            //Insert when $cateId === 0
            $data = array(
                ':cate_id' => $cateId,
                ':cate_name' => $cateName,
                ':cate_parent' => $cateParent,
                ':cate_page_title' => $catePageTitle,
                ':cate_seo_url' => $cateUrl,
                ':cate_meta_description' => $cateMetaDes,
                ':cate_meta_keywords' => $cateMetaKey,
                ':cate_is_serie' => $cateIsSerie
            );

            if( $dataCateId == 0 ) {
                $sql = "INSERT INTO tp_cates( cate_id, cate_name, cate_parent, cate_page_title, cate_seo_url, cate_meta_description, cate_meta_keywords, cate_is_serie ) VALUES( :cate_id, :cate_name, :cate_parent, :cate_page_title, :cate_seo_url, :cate_meta_description, :cate_meta_keywords, :cate_is_serie );";
                $r = $exec -> add( $sql, $data );
            }
            //Update when $cateId <> 0
            else {
                $sql = "UPDATE tp_cates SET cate_name = :cate_name, cate_parent = :cate_parent, cate_page_title = :cate_page_title, cate_seo_url = :cate_seo_url, cate_meta_description = :cate_meta_description, cate_meta_keywords = :cate_meta_keywords, cate_is_serie = :cate_is_serie WHERE cate_id = :cate_id ;";
                $r = $exec -> exec( $sql, $data );
            }
        }
        if( !$r )
            echo '0|Trùng tên danh mục';
        else
            echo '1|Lưu thành công';
    }
?>
