<?php
    $exec = new Exec( HOST, USER, PASS, DBNAME );

    if( isset( $_POST['cate_id'] ) ) {

        $data = array(
            ':menu_id' => (int)$_POST['cate_id']
        );
        $sql = "DELETE FROM tp_menu WHERE menu_id = :menu_id OR menu_parent_id = :menu_id;";
        $r = $exec -> exec( $sql, $data );
        $r ? print( '1|Xóa thành công' ) : print( '0|Có lỗi xảy ra' );
    }
?>
