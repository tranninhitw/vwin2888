<?php
    $exec = new Exec( HOST, USER, PASS, DBNAME );

    $sql = "SELECT * FROM tp_menu ORDER BY menu_order ASC;";
    $cates = $exec -> get( $sql );

    /**
     * Fn to build cate tree
    */

    $cates = find_children( $cates );
    $html = '<ol class="sortable ui-sortable cate-list">';
    $html .= build_tree( $cates );
    $html .= '</ol>';
    echo $html;


    function find_children( $cates, $parentId = 0 ) {
        $tree = array();
        foreach( $cates as $key => $cate ) {
            if( $cate['menu_parent_id'] == $parentId ) {
                array_push( $tree, array(
                    'cate_info' => $cate,
                    'cate_children' => find_children( $cates, $cate['menu_id'] )
                ) );
            }
        }
        return $tree;
    }

    function build_tree( $tree ) {
        $html = '';
        foreach( $tree as $key => $branch ) {
            $childTree = '<ol>';
            $childTree .= build_tree( $branch['cate_children'] );
            $childTree .= '</ol>';
            $cate = $branch['cate_info'];
            $checked = $cate['menu_hidden'] == '0' ? ' has-checked" checked' : '"';
            $cateName = trim( $cate['menu_text'] );
            $html .= '
                <li id="cate_' . $cate['menu_id'] . '" data-cate-id="' . $cate['menu_id'] . '">
                    <div>
                        <span class="cate-caret">
                            <i class="fa fa-caret-right" aria-hidden="true"></i>
                        </span>
                        <span class="cate-name">' . $cateName . '</span>
                        <span class="cate-delete" data-cate-id="' . $cate['menu_id'] . '"><i class="fa fa-times" aria-hidden="true"></i></span>
                        <div>
                            <table class="form-tables">
                                <tr>
                                    <td>Tên menu (*)</td>
                                    <td>
                                        <input class="inputs normal-inputs required-inputs cate-textbox cate-name-textbox" type="text" placeholder="Tên menu" value="' . $cateName . '" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        URL (*)
                                    </td>
                                    <td>
                                        <input type="text" class="inputs required-inputs cate-textbox" placeholder="Nhập url menu (tương đối hoặc tuyệt đối)" value="' . $cate['menu_link'] . '" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Hiện ra</td>
                                    <td>
                                        <input id="enable' . $cate['menu_id'] . '" type="checkbox" class="checkboxes cate-textbox ' . $checked . ' />
                                        <label for="enable' . $cate['menu_id'] . '"></label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    ' . $childTree . '
                </li>
            ';
        }
        $html = str_replace( '<ol></ol>', '', $html );
        return $html;
    }

?>
