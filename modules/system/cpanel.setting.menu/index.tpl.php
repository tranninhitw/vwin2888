{@style}
<section class="module-wrapper">
    <section class="module-toolbar">
        <h3 class="module-heading">Hệ thống / Menu <small>Cpanel</small></h3>
        <div class="module-button-area">
            <span class="buttons normal-buttons cate-addnew-btn"><i class="fa fa-plus" aria-hidden="true"></i> Thêm</span>
            <span class="buttons disabled-buttons cate-cancel-btn"><i class="fa fa-undo" aria-hidden="true"></i> Hủy</span>
        </div>
    </section>
    <section class="module-content">
        <div class="module-addnew cate-addnew">
            <h3>Nhập thông tin để tạo menu, sau đó sắp xếp và lưu:</h3>
            <span class="buttons disabled-buttons cate-name-save-btn"><i class="fa fa-floppy-o"></i> Lưu</span>
            <table class="form-tables form-tables-large">
                <tr>
                    <td>Tên menu (*)</td>
                    <td>
                        <input class="inputs normal-inputs required-inputs cate-textbox cate-name-textbox" type="text" placeholder="Nhập tên menu" />
                    </td>
                </tr>
                <tr>
                    <td>
                        URL (*)
                    </td>
                    <td>
                        <input type="text" class="inputs required-inputs cate-textbox" placeholder="Nhập url menu (tương đối hoặc tuyệt đối)" />
                    </td>
                </tr>
                <tr>
                    <td>Hiện ra</td>
                    <td>
                        <input id="enable" type="checkbox" class="checkboxes cate-textbox" />
                        <label for="enable"></label>
                    </td>
                </tr>
            </table>
        </div>
        <div class="cate-list-wrapper">
            <div class="module-button-area">
                <span class="buttons disabled-buttons cate-save-order-btn save-btn"><i class="fa fa-floppy-o"></i> Lưu</span>
                <span class="buttons disabled-buttons cate-undo-order-btn undo-btn"><i class="fa fa-undo" aria-hidden="true"></i> Hủy</span>
            </div>
            <h3><b>Kéo thả</b> để sắp xếp menu, <b>xổ xuống</b> để sửa:</h3>
            {@list}
        </div>
    </section>
</section>
{@script}
