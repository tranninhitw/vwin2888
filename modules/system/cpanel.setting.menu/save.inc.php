<?php
    if( isset( $_POST['data'] ) ) {

        //Embed class
        $exec = new Exec( HOST, USER, PASS, DBNAME );

        //Save data
        foreach( $_POST['data'] as $key => $arr ) {

            //Get data and remove all special characters
            $cateName = $arr['cate_name'];
            $cateParent = $arr['cate_parent'];
            $dataCateId = $arr['data_cate_id'];
            $cateId = $arr['cate_id'];
            $cateUrl = $arr['cate_seo_url'];
            $cateIsSerie = $arr['cate_is_serie'];
            $cateOrder = $arr['cate_order'];

            $cateName = strip_tags( $cateName );
            $cateUrl = strip_tags( $cateUrl );
            $cateParent = (int)$cateParent;
            $dataCateId = (int)$dataCateId;
            $cateId = (int)$cateId;

            //Insert when $cateId === 0
            $data = array(
                ':menu_id' => $cateId,
                ':menu_text' => $cateName,
                ':menu_link' => $cateUrl,
                ':menu_hidden' => $cateIsSerie,
                ':menu_order' => $cateOrder,
                ':menu_parent_id' => $cateParent
            );

            if( $dataCateId == 0 ) {
                $sql = "INSERT INTO tp_menu( menu_id, menu_text, menu_parent_id, menu_link, menu_hidden, menu_order ) VALUES (:menu_id, :menu_text, :menu_parent_id, :menu_link, :menu_hidden, :menu_order);";
                $r = $exec -> add( $sql, $data );
            }
            //Update when $cateId <> 0
            else {
                $sql = "UPDATE tp_menu SET menu_id = :menu_id, menu_text = :menu_text, menu_parent_id = :menu_parent_id, menu_link = :menu_link, menu_hidden = :menu_hidden, menu_order = :menu_order WHERE menu_id = :menu_id ;";
                $r = $exec -> exec( $sql, $data );
            }
        }

        if( !$r )
            echo '0|Trùng tên menu';
        else
            echo '1|Lưu thành công';
    }
?>
