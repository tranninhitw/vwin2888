<?php
    $exec = new Exec( HOST, USER, PASS, DBNAME );

    if( isset( $_POST['offset'] ) ) {
        $offset = (int)$_POST['offset'];
    }
    else $offset = 0;

    $html = '';
    $sql = "SELECT * FROM tp_marketing_email ORDER BY marketing_email_time DESC LIMIT 5 OFFSET :offset;";
    $emails = $exec -> get( $sql, array(
        ':offset' => $offset
    ), true );

    if( count( $emails ) == 0 ) {
        $html = '
            <tr>
                <td colspan="4" style="text-align: center;">Hiện chưa có email</td>
            </tr>
        ';
    }

    foreach( $emails as $key => $value ) {

        $html .= '
            <tr>
                <td>' . $value['marketing_email_subject'] . '</td>
                <td>' . substr($value['marketing_email_contents'], 0, 200) . '</td>
                <td>' . date( 'H:i:s d/m/Y', $value['marketing_email_time'] ) . '</td>
                <td>
                    <button data-id="' . $value['marketing_email_id'] . '" class="mini-buttons normal-buttons edit-email" title="Sửa"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                    <button data-id="' . $value['marketing_email_id'] . '" class="mini-buttons cancel-buttons delete-email" title="Xóa"><i class="fa fa-trash" aria-hidden="true"></i></button>
                </td>
            </tr>
        ';
    }
    echo $html;
?>
