$( document ).ready( function() {

    /*WYSIWYG init*/
    DOM.initWysiwyg();

    var PARAMS = ULTI.queryString();

    /*Click addnew button*/
    $( document ).off( 'click', '.email-addnew-btn' ).on( 'click', '.email-addnew-btn', function() {
        MODULE.cmd( 'addnew', $( '.email-cancel-btn' ) );
    } );

    /*Validate form*/
    $( document ).on( 'change input', '.email-textbox', function() {
        var allDone = ULTI.validate( $( '.module-addnew .required-inputs' ), 'empty' );
        if( allDone ) {
            $('.email-save-btn, .email-send-all-btn, .email-send-test-btn').removeClass( 'disabled-buttons' ).addClass( 'save-buttons' );
            ULTI.updateChangeState( true );
        }
        else {
            $('.email-save-btn, .email-send-all-btn, .email-send-test-btn').addClass( 'disabled-buttons' ).removeClass( 'save-buttons' );
            ULTI.updateChangeState( true );
        }
    } );

    /*Click cancel*/
    $( document ).off( 'click', '.email-cancel-btn' ).on( 'click', '.email-cancel-btn', function() {
        MODULE.cmd( 'cancel', $( '.email-cancel-btn' ) );
        DOM.loadTplThisEl( _WWW + 'admin/noidung/emailmarketing' );
    } );


    /*Send mail / save email / test mail*/
    $( document ).off( 'click', '.email-send-all-btn, .email-save-btn, .email-send-test-btn' ).on( 'click', '.email-send-all-btn, .email-save-btn, .email-send-test-btn', function() {
        var defaultHtml = $( this ).html();
        var url = '';
        var data = $( '.module-addnew .email-textbox' ).serialize();
        switch(true) {
            case $(this).hasClass('email-send-all-btn'):
                url = 'include/10';
                $( this ).addClass( 'saving' ).html( '<i class="fa fa-spinner fa-spin fa-fw"></i> Đang gửi' );
                break;
            case $(this).hasClass('email-send-test-btn'):
                $( this ).addClass( 'saving' ).html( '<i class="fa fa-spinner fa-spin fa-fw"></i> Đang gửi' );
                url = 'include/11';
                break;
            case $(this).hasClass('email-save-btn'):
                $( this ).addClass( 'saving' ).html( '<i class="fa fa-spinner fa-spin fa-fw"></i> Đang lưu' );
                url = 'include/12';
                break;
        }

        // When edit email
        if( PARAMS.action == 'edit' ) {
            data += '&action=edit&id=' + PARAMS.id;
        }

        var callback = function( html ) {
            console.log(html);
            var htmlArr = html.split( '|' );
            if( htmlArr[0] == '1' ) {
                ULTI.done( htmlArr[1] );
            }
            else {
                DOM.showStatus( htmlArr[1], parseInt( htmlArr[0] ) );
                $( '.email-send-all-btn' ).removeClass( 'saving' ).html(defaultHtml);
            }
        };
        AJAX.post( url, data, callback );
    } );

    /*When click edit*/
    $( document ).off( 'click', '.edit-email' ).on( 'click', '.edit-email', function() {
        var id = $( this ).data( 'id' );
        DOM.loadTplThisEl( _WWW + 'admin/noidung/emailmarketing?action=edit&id=' + id );
    } );

    /*When edit*/
    $( document ).ready( function() {
        if( PARAMS.action == 'edit' ) {
            $( '.email-addnew-btn' ).click();
        }
    } );

    /*Delete post*/
    $( document ).off( 'click', '.delete-email' ).on( 'click', '.delete-email', function() {
        var deleteFn = function( target ) {
            var id = target.data( 'id' );
            var url = 'include/13';
            var data = {
                'email_id' : id
            };
            var callback = function( html ) {
                var htmlArr = html.split( '|' );
                DOM.showStatus( htmlArr[1], htmlArr[0] );
                DOM.loadTplThisEl( _WWW + 'admin/noidung/emailmarketing' );
            };
            AJAX.post( url, data, callback );
        };
        var text = 'Bạn có chắc chắn xóa email này?';
        DOM.showPopup( text, deleteFn, $( this ) );
    } );


    /*Load more*/
    $( document ).off( 'click', '.loadmore-btn' ).on( 'click', '.loadmore-btn', function() {
        $( this ).addClass( 'saving' ).html( '<i class="fa fa-spinner fa-spin fa-fw" aria-hidden="true"></i> Đang tải' );
        var offset =  $( this ).data( 'offset' );
        offset = parseInt( offset ) + 5;
        var url = 'include/30';
        var data = {
            'offset': offset
        };
        var _this = $( this );
        var tbl = $( '.post-table' );
        var callback = function( html ) {
            $( '.loadmore-btn' ).removeClass( 'saving' ).html( 'Tải thêm' );
            if( html.search( 'colspan="5"' ) === -1 ) {
                tbl.find( 'tbody' ).append( html );
                _this.data( 'offset', offset );
                var row = tbl.find( 'tr' ).length;
                row = parseInt( row ) - 1;
                $( '.viewing' ).text( row );
            }
            else {
                var emptyRow = '<tr><td colspan="5">Đã tải hết tất cả</td></tr>';
                tbl.find( 'tr:last-of-type' ).after( emptyRow );
                _this.remove();
            }
        };
        AJAX.post( url, data, callback );
    } );
} );
