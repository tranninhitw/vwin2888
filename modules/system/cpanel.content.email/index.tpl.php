{@style}
<section class="module-wrapper">
    <section class="module-toolbar">
        <h3 class="module-heading">Nội dung / Email <small>Cpanel</small></h3>
        <div class="module-button-area">
            <span class="buttons normal-buttons email-addnew-btn"><i class="fa fa-plus" aria-hidden="true"></i> Thêm</span>
            <span class="buttons disabled-buttons email-cancel-btn"><i class="fa fa-undo" aria-hidden="true"></i> Hủy</span>
        </div>
    </section>
    <section class="module-content">
        <div class="tabs module-addnew">
            <h3>Thiết kế giao diện email <a href="https://stripo.email/" target="_blank">tại đây</a>. Sau đó sao chép mã HTML dán vào bên dưới:</h3>
            <table class="form-tables form-tables-large">
                {@addnew-form}
            </table>
        </div>
        <div class="post-list-wrapper">
            <table class="tables post-table">
                <col width="30%">
                <col width="40%">
                <col width="20%">
                <col width="10%">
                <tr class="title-rows">
                    <td>Tiêu đề</td>
                    <td>Nội dung</td>
                    <td>Ngày tạo</td>
                    <td>Biên tập</td>
                </tr>
                {@list}
            </table>
        </div>
        {@pager}
    </section>
</section>
{@script}
