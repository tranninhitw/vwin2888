<?php
    if( isset( $_POST['email_id'] ) ) {
        $exec = new Exec( HOST, USER, PASS, DBNAME );

        $sql = "DELETE FROM tp_marketing_email WHERE marketing_email_id = :email_id;";
        $r = $exec -> exec( $sql, array(
            ':email_id' => (int)$_POST['email_id']
        ) );

        $r ? print( '1|Xóa email thành công' ) : print( '0|Có lỗi' );
    }
?>
