<?php
    if( isset( $_POST['email_subject'] ) ) {
        //Init OOP
        $exec = new Exec( HOST, USER, PASS, DBNAME );

        //Get data
        foreach( $_POST as $key => $value ) {
            $$key = $value;
        }

        if(isset($action) && $action == 'edit') {
            // Edit email
            $sql = "UPDATE tp_marketing_email SET marketing_email_contents = :content, marketing_email_subject = :subject WHERE marketing_email_id = :id;";
            $r = $exec -> exec($sql, array(
                ':id' => $id,
                ':content' => $email_html,
                ':subject' => $email_subject
            ));
        } else {
            // Save email
            $sql = "INSERT INTO tp_marketing_email(marketing_email_time, marketing_email_contents, marketing_email_subject) VALUES(:time, :content, :subject);";
            $r = $exec -> exec($sql, array(
                ':time' => time(),
                ':content' => $email_html,
                ':subject' => $email_subject
            ));
        }

        $r ? print( '1|Lưu email thành công' ) : print( '0|Lỗi khi lưu' );
    }
?>
