<?php

    $exec = new Exec( HOST, USER, PASS, DBNAME );

    // Đếm số email cần gửi
    $sql = "SELECT COUNT(*) AS person FROM tp_promotion_email;";
    $person = $exec -> get($sql);

    if( isset( $_GET['action'] ) && $_GET['action'] == 'edit' ) {
        $sql = "SELECT * FROM tp_marketing_email WHERE marketing_email_id = :id;";
        $mail = $exec -> get($sql, array(
            ':id' => (int)$_GET['id']
        ));
        $mail = $mail[0];

        $html = '
            <tr>
                <td>Tiêu đề (*):</td>
                <td>
                    <input type="text" name="email_subject" class="inputs required-inputs email-textbox" placeholder="Tiêu đề của mail" value="' . $mail['marketing_email_subject'] . '" />
                </td>
            </tr>
            <tr>
                <td>Nội dung (*):</td>
                <td>
                    <textarea name="email_html" class="inputs required-inputs email-textbox" placeholder="Có thể điền văn bản hoặc HTML">' . $mail['marketing_email_contents'] . '</textarea>
                </td>
            </tr>
            <tr>
                <td>Hành động</td>
                <td>
                    <span class="buttons disabled-buttons email-save-btn" title="Lưu lại gửi sau"><i class="fa fa-floppy-o"></i> Lưu</span>
                    <span class="buttons disabled-buttons email-send-all-btn" title="Gửi không lưu"><i class="fa fa-paper-plane"></i> Gửi ngay (' . $person[0]['person'] . ' người)</span>
                    <span class="buttons disabled-buttons email-send-test-btn" title="Gửi cho chính bạn xem thử"><i class="fa fa-bolt"></i> Gửi xem thử</span>
                </td>
            </tr>
        ';
    } else {
        $html = '
            <tr>
                <td>Tiêu đề (*):</td>
                <td>
                    <input type="text" name="email_subject" class="inputs required-inputs email-textbox" placeholder="Tiêu đề của mail" />
                </td>
            </tr>
            <tr>
                <td>Nội dung (*):</td>
                <td>
                    <textarea name="email_html" class="inputs required-inputs email-textbox" placeholder="Có thể điền văn bản hoặc HTML"></textarea>
                </td>
            </tr>
            <tr>
                <td>Hành động</td>
                <td>
                    <span class="buttons disabled-buttons email-save-btn" title="Lưu lại gửi sau"><i class="fa fa-floppy-o"></i> Lưu</span>
                    <span class="buttons disabled-buttons email-send-all-btn" title="Gửi không lưu"><i class="fa fa-paper-plane"></i> Gửi ngay (' . $person[0]['person'] . ' người)</span>
                    <span class="buttons disabled-buttons email-send-test-btn" title="Gửi cho chính bạn xem thử"><i class="fa fa-bolt"></i> Gửi xem thử</span>
                </td>
            </tr>
        ';
    }
    echo $html;
?>
