{@style}
<section class="module-wrapper">
    <section class="module-toolbar">
        <h3 class="module-heading">Hệ thống / Cài đặt chung <small>Cpanel</small></h3>
    </section>
    <section class="module-content">
        <div class="tabs module">
            {@list}
        </div>
    </section>
</section>
{@script}
