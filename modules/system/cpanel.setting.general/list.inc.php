<?php
    $exec = new Exec( HOST, USER, PASS, DBNAME );

    $sql =  "SELECT * FROM tp_settings;";
    $settings = $exec -> get( $sql );
    foreach( $settings as $key => $value ) {

        if( $value['setting_name'] == 'default_avatar' ) {
            $default_avatar = $value['setting_value'];
        }
        if( $value['setting_name'] == 'company_information' ) {
            $company_information = $value['setting_value'];
        }

    }

    $company_information = json_decode( $company_information, true );
    $avatar = json_decode( $default_avatar, true );

    foreach($company_information as $key => $value) {
        if(is_array(json_decode($value, true))) {
            $url = json_decode($value, 0);
            $company_information[$key] = $url[0];
        }
    }

    $html = '
        <span class="tab-title">Cài đặt chung</span>
        <div class="module-addnew-btn">
            <span class="buttons disabled-buttons general-save-new save-btn"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu</span>
        </div>
        <input type="radio" name="tab" id="tab1" class="tab-radio" checked />
        <label for="tab1" class="tab-label">Logo</label>
        <div class="tab-content">
            <table class="form-tables tab1-tbl">
                <tr>
                    <td>Logo chính:</td>
                    <td>
                        <button class="upload-image hidden">Upload</button>
                        <input name="main_logo" type="text" class="company-information-input inputs hidden general-textbox" readonly />
                        <input type="file" data-image-name="logo_main" class="file-inputs general-textbox picture" id="picture_1" accept="image/*" />
                        <label for="picture_1">
                            <span>Chọn hình ảnh</span>
                            <span>Chưa chọn file...</span>
                        </label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="picture-preview">
                            <img width="200" height="200" src="' . TP_REL_ROOT . 'uploads/public/' . $company_information['main_logo'] . '" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Logo chân trang:</td>
                    <td>
                        <button class="upload-image hidden">Upload</button>
                        <input name="footer_logo" type="text" class="company-information-input inputs hidden general-textbox" readonly />
                        <input type="file" data-image-name="logo_footer" class="file-inputs general-textbox picture" id="picture_2" accept="image/*" />
                        <label for="picture_2">
                            <span>Chọn hình ảnh</span>
                            <span>Chưa chọn file...</span>
                        </label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="picture-preview">
                            <img width="200" height="200" src="' . TP_REL_ROOT . 'uploads/public/' . $company_information['footer_logo'] . '" />
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <input type="radio" name="tab" id="tab2" class="tab-radio" />
        <label for="tab2" class="tab-label">Thông tin</label>
        <div class="tab-content">
            <table class="form-tables tab2-tbl">
                <tr>
                    <td>Nội dung cuối trang:</td>
                    <td>
                        <textarea name="footer_content" class="inputs general-textbox" placeholder="Nội dung">' . $company_information['footer_content'] . '</textarea>
                    </td>
                </tr>
                <tr>
                    <td>Mã Google Analytics:</td>
                    <td>
                        <input name="ga_code" type="text" class="inputs general-textbox" placeholder="Code" value="' . $company_information['ga_code'] . '"/>
                    </td>
                </tr>
            </table>
        </div>
        <input type="radio" name="tab" id="tab3" class="tab-radio" />
        <label for="tab3" class="tab-label">Hình ảnh</label>
        <div class="tab-content">
            <table class="form-tables tab3-tbl full-width">
                <tr>
                    <td>Trò chơi 1:</td>
                    <td>
                        <button class="upload-image hidden">Upload</button>
                        <input name="game_1" type="text" class="company-information-input inputs hidden general-textbox" readonly />
                        <input type="file" data-image-name="game_1" class="file-inputs general-textbox picture" id="picture_3" accept="image/*" />
                        <label for="picture_3">
                            <span>Chọn hình ảnh</span>
                            <span>Chưa chọn file...</span>
                        </label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="picture-preview">
                            <img width="200" height="200" src="' . TP_REL_ROOT . 'uploads/public/' . $company_information['game_1'] . '" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Trò chơi 2:</td>
                    <td>
                        <button class="upload-image hidden">Upload</button>
                        <input name="game_2" type="text" class="company-information-input inputs hidden general-textbox" readonly />
                        <input type="file" data-image-name="game_2" class="file-inputs general-textbox picture" id="picture_8" accept="image/*" />
                        <label for="picture_8">
                            <span>Chọn hình ảnh</span>
                            <span>Chưa chọn file...</span>
                        </label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="picture-preview">
                            <img width="200" height="200" src="' . TP_REL_ROOT . 'uploads/public/' . $company_information['game_2'] . '" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Trò chơi 3:</td>
                    <td>
                        <button class="upload-image hidden">Upload</button>
                        <input name="game_3" type="text" class="company-information-input inputs hidden general-textbox" readonly />
                        <input type="file" data-image-name="game_3" class="file-inputs general-textbox picture" id="picture_5" accept="image/*" />
                        <label for="picture_5">
                            <span>Chọn hình ảnh</span>
                            <span>Chưa chọn file...</span>
                        </label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="picture-preview">
                            <img width="200" height="200" src="' . TP_REL_ROOT . 'uploads/public/' . $company_information['game_3'] . '" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Trò chơi 4:</td>
                    <td>
                        <button class="upload-image hidden">Upload</button>
                        <input name="game_4" type="text" class="company-information-input inputs hidden general-textbox" readonly />
                        <input type="file" data-image-name="game_4" class="file-inputs general-textbox picture" id="picture_6" accept="image/*" />
                        <label for="picture_6">
                            <span>Chọn hình ảnh</span>
                            <span>Chưa chọn file...</span>
                        </label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="picture-preview">
                            <img width="200" height="200" src="' . TP_REL_ROOT . 'uploads/public/' . $company_information['game_4'] . '" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Hình đại diện mặc định:</td>
                    <td>
                        <button class="upload-image hidden">Upload</button>
                        <input name="default_avatar" type="text" class="company-information-input inputs hidden general-textbox" readonly />
                        <input type="file" data-image-name="default_avatar" class="file-inputs general-textbox picture" id="picture_7" accept="image/*" />
                        <label for="picture_7">
                            <span>Chọn hình ảnh</span>
                            <span>Chưa chọn file...</span>
                        </label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="picture-preview">
                            <img width="200" height="200" src="' . TP_REL_ROOT . 'uploads/public/' . $avatar[0] . '" />
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    ';
    echo $html;
?>
