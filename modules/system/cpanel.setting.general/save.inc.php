<?php
    $exec = new Exec( HOST, USER, PASS, DBNAME );

    //Get data
    foreach( $_POST as $key => $value ) {
        $$key = $value;
    }
    $sql = "SELECT * FROM tp_settings;";
    $settings = $exec -> get( $sql );
    foreach( $settings as $key => $value ) {
        if( $value['setting_name'] == 'default_avatar' ) {
            $default_avatar_tbl = $value['setting_value'];
        }
        if( $value['setting_name'] == 'company_information' ) {
            $company_information_tbl = $value['setting_value'];
        }
    }

    $company_information_arr = json_decode( $company_information_tbl, true );
    $company_information_arr['footer_content'] = $footer_content;
    $company_information_arr['ga_code'] = $ga_code;
    $company_information_arr['main_logo'] = $main_logo == '' ? $company_information_arr['main_logo'] : $main_logo;
    $company_information_arr['footer_logo'] = $footer_logo == '' ? $company_information_arr['footer_logo'] : $footer_logo;
    $company_information_arr['game_1'] = $game_1 == '' ? $company_information_arr['game_1'] : $game_1;
    $company_information_arr['game_2'] = $game_2 == '' ? $company_information_arr['game_2'] : $game_2;
    $company_information_arr['game_3'] = $game_3 == '' ? $company_information_arr['game_3'] : $game_3;
    $company_information_arr['game_4'] = $game_4 == '' ? $company_information_arr['game_4'] : $game_4;

    $data_default_avatar = array(
        'setting_value' => $default_avatar == '' ? $default_avatar_tbl : $default_avatar,
        'setting_name' => 'default_avatar'
    );

    $data_company_information = array(
        'setting_value' => json_encode( $company_information_arr ),
        'setting_name' => 'company_information'
    );

    $sql = "UPDATE tp_settings SET setting_value = :setting_value WHERE setting_name = :setting_name;";
    $r = $exec -> exec( $sql, $data_default_avatar );
    $r = $exec -> exec( $sql, $data_company_information );

    $r ? print( '1|Lưu thành công' ) : print( '0|Có lỗi xảy ra' );
?>
