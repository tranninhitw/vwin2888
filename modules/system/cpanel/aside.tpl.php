<span class="aside-button">
    <i class="fa fa-bars" aria-hidden="true"></i>
</span>
<aside>
    <ul class="aside-list">
        <li class="aside-item aside-homepage">
            <i class="fa fa-home" aria-hidden="true"></i>
            <span>Trang chủ</span>
            <a href="{@www}/" target="_blank">Xem trang chủ</a>
        </li>
        <li class="aside-item aside-overview realtime-item" data-type="mail">
            <i class="fa fa-tachometer" aria-hidden="true"></i>
            <span>Tổng quan</span>
            <a href="{@www}/admin">Tổng quan</a>
        </li>
        <li class="aside-item">
            <i class="fa fa-pencil" aria-hidden="true"></i>
            <span>Nội dung</span>
            <a href="">Nội dung</a>
            <span class="aside-child-button">
                <i class="fa fa-caret-down" aria-hidden="true"></i>
            </span>
        </li>
        <li class="aside-child-row">
            <ul class="aside-child-list">
                <li class="aside-child-item main">
                    <i class="fa fa-folder-open" aria-hidden="true"></i>
                    <span>Danh mục</span>
                    <a href="{@www}/admin/noidung/danhmuc">Danh mục</a>
                </li>
                <li class="aside-child-item">
                    <i class="fa fa-files-o" aria-hidden="true"></i>
                    <span>Bài viết</span>
                    <a href="{@www}/admin/noidung/baiviet">Bài viết</a>
                </li>
                <li class="aside-child-item">
                    <i class="fa fa-comments-o" aria-hidden="true"></i>
                    <span>Bình luận</span>
                    <a href="{@www}/admin/noidung/binhluan">Bình luận</a>
                </li>
                <li class="aside-child-item">
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                    <span>Email marketing</span>
                    <a href="{@www}/admin/noidung/emailmarketing">Email marketing</a>
                </li>
            </ul>
        </li>
        <li class="aside-item">
            <i class="fa fa-cogs" aria-hidden="true"></i>
            <span >Hệ thống</span>
            <a href="">Hệ thống</a>
            <span class="aside-child-button">
                <i class="fa fa-caret-down" aria-hidden="true"></i>
            </span>
        </li>
        <li class="aside-child-row">
            <ul class="aside-child-list">
                <li class="aside-child-item main">
                    <i class="fa fa-bars" aria-hidden="true"></i>
                    <span>Menu</span>
                    <a href="{@www}/admin/caidat/menu">Menu</a>
                </li>
                <li class="aside-child-item">
                    <i class="fa fa-cog" aria-hidden="true"></i>
                    <span>Cài đặt chung</span>
                    <a href="{@www}/admin/caidat/caidatchung">Cài đặt chung</a>
                </li>
                <li class="aside-child-item">
                    <i class="fa fa-users" aria-hidden="true"></i>
                    <span>Quản trị viên</span>
                    <a href="{@www}/admin/caidat/taikhoan">Quản trị viên</a>
                </li>
                <li class="aside-child-item">
                    <i class="fa fa-bullhorn" aria-hidden="true"></i>
                    <span>Quảng cáo</span>
                    <a href="{@www}/admin/caidat/quangcao">Quảng cáo</a>
                </li>
            </ul>
        </li>
    </ul>
    <footer>
        <p class="footer-copyrigt">2019 &copy MrRobot</p>
    </footer>
</aside>
