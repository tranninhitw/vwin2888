<header>
    <div class="header-logo">
        <span class="logo-mini">
            <b>CP</b>N
        </span>
        <span class="logo-normal">
            <p>Cpanel</p>
        </span>
    </div>
    <div class="header-profile">
        {@profile}
    </div>
</header>
