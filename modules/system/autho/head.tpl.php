<!DOCTYPE html>
<html>
    <head>
        <title>{@title}</title>
        <meta charset="utf-8" />
        {@base}
        <link href="mini-autho.css" rel="stylesheet" />
        <link href="{@www}/vendors/css/font-awesome.min.css" rel="stylesheet" />
    </head>
    <body>
