<?php
    $db = unserialize( TP_DB_PARAMS );
    $exec = new Exec( $db['DB_HOST'], $db['DB_USERNAME'], $db['DB_PASSWORD'], $db['DB_SCHEMA_NAME'] );
    $hasher = new Password( 8, false );

    if( isset( $_POST['info'] ) ) {
        $info = strip_tags( $_POST['info'] );
        $password = strip_tags( $_POST['password'] );
        $captcha = strip_tags( $_POST['captcha'] );

        //Check captcha
        if(isset($_SESSION['wrong']) && $_SESSION['wrong'] > TP_WRONG_PASS) {
            echo 'Sai thông tin đăng nhập!|<div class="g-recaptcha" data-callback="captchaCallback" data-sitekey="' . TP_CAPTCHA_SITEKEY . '"></div>';

            $captchaResponse = file_get_contents( 'https://www.google.com/recaptcha/api/siteverify?secret=' . TP_CAPTCHA_SECRET . '&response=' . $captcha . '&remoteip=' . $_SERVER['REMOTE_ADDR'] );
            $captchaResponse = json_decode($captchaResponse, true);

            if( $captchaResponse['success'] == false ) {
                echo '<p class="captcha-alert">Vui lòng hoàn thành captcha để tiếp tục...</p>';
                exit;
            }
        }

        //Remove special characters
        $info = preg_replace( '/[^a-zA-Z0-9@\.]+/', '', $info );

        $data = array(
            ':admin_username' => $info,
            ':admin_email' => $info
        );

        $prefix = $db['DB_TABLE_PREFIX'];
        $sql = "SELECT * FROM " . $prefix . "admins WHERE admin_username = :admin_username OR admin_email = :admin_email LIMIT 1;";
        $r = $exec -> get( $sql, $data );
        if( count( $r ) == 1 ) {
            $hash = $r[0]['admin_password'];
            $isRight = $hasher -> CheckPassword( $password, $hash );

            if( $isRight ) {
                $_SESSION['tk_logged'] = true;
                $_SESSION['tk_user_data'] = serialize( $r[0] );
                echo 'success';
            }
            else {
                // Sai mật khẩu
                $_SESSION['wrong'] = isset($_SESSION['wrong']) ? $_SESSION['wrong'] + 1 : 1;

                // Chỉ hiện ra khi chưa sai mật khẩu quá số lần quy định
                if(!isset($_SESSION['wrong']) || $_SESSION['wrong'] < TP_WRONG_PASS + 2) {
                    echo 'Sai thông tin đăng nhập!';
                }

            }
        }
        else {
            // Không tồn tại tài khoản

            $_SESSION['wrong'] = isset($_SESSION['wrong']) ? $_SESSION['wrong'] + 1 : 1;

            // Chỉ hiện ra khi chưa sai mật khẩu quá số lần quy định
            if(!isset($_SESSION['wrong']) || $_SESSION['wrong'] < TP_WRONG_PASS + 2) {
                echo 'Sai thông tin đăng nhập!';
            }
        }
    }
?>
