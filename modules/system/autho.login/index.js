window.onload = function() {
    var loginBtn = document.getElementById( 'login-button' );
    var infoInput = document.getElementById( 'login-email' );
    var passwordInput = document.getElementById( 'login-password' );
    var loginInput = document.getElementsByClassName( 'login-input' );
    var status = document.getElementById( 'login-status' );
    var captchaEl = document.getElementById( 'captcha' );
    var captchaInput = document.getElementById( 'captcha-input' );


    /*Focus input email*/
    infoInput.focus();

    /*Login button click*/
    loginBtn.addEventListener( 'click', function( e ) {
        e.preventDefault();
        this.innerHTML = this.getAttribute( 'data-loading-text' );

        //Ajax
        var info = infoInput.value;
        var password = passwordInput.value;
        var captcha = captchaInput.value;
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if ( xhttp.readyState == 4 && xhttp.status == 200 ) {

                if( xhttp.responseText.search('success') != -1 ) {
                    location.href = _WWW + 'admin';
                }
                else {
                    var res = xhttp.responseText,
                        resArr = res.split('|');

                    if(resArr.length > 1) {
                        // Khi này hiện captcha
                        var error = resArr[0];
                        var captchaHtml = resArr[1];
                        captchaEl.innerHTML = captchaHtml;
                        var script = document.createElement('script');
                        script.src = 'https://www.google.com/recaptcha/api.js';
                        document.querySelector('head').appendChild(script);
                    } else {
                        var error = res;
                    }

                    status.innerHTML = '<i class="fa fa-exclamation-circle" aria-hidden="true"></i>' + error;
                    status.style.display = 'block';
                    loginBtn.innerHTML = 'Đăng nhập';
                }
            }
        };
        xhttp.open( 'POST', 'include/3', true );
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send( 'info=' + info + '&password=' + password + '&captcha=' + captcha );
    }, false );

    /*Enable login button*/
    for( i = 0; i < 2; i++ ) {
        addListenerMulti( loginInput[i], 'keyup change', function() {
            captchaCallback();
        } );
    }
};

function addListenerMulti(el, s, fn) {
  var evts = s.split(' ');
  for (var i=0, iLen=evts.length; i<iLen; i++) {
    el.addEventListener(evts[i], fn, false);
  }
};

function captchaCallback(response) {
    var infoInput = document.getElementById( 'login-email' );
    var passwordInput = document.getElementById( 'login-password' );
    var loginBtn = document.getElementById( 'login-button' );
    var captchaInput = document.getElementById( 'captcha-input' );
    var captchaLen = response ? response.length : 0;
    var info = infoInput.value;
    var password = passwordInput.value;

    if (info != '' && password != '' && captchaLen > 0) {
        captchaInput.value = response;
        loginBtn.disabled = false;

    } else if (info != '' && password != '' && captchaInput.value == '' ) {
        loginBtn.disabled = false;

    } else {
        captchaInput.value = '';
        loginBtn.disabled = true;
    }
};
