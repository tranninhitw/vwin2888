<?php
    if( isset( $_POST['admin_id'] ) ) {
        $exec = new Exec( HOST, USER, PASS, DBNAME );

        $data = array(
            'admin_id' => $_POST['admin_id']
        );
        $sql = "SELECT admin_blocked FROM tp_admins WHERE admin_id = :admin_id;";
        $block = $exec -> get( $sql, $data );
        $block = $block[0]['admin_blocked'];
        $block = $block ? 0 : 1;
        $save = array(
            'admin_id' => $_POST['admin_id'],
            'admin_blocked' => $block
        );
        $sql = "UPDATE tp_admins SET admin_blocked = :admin_blocked WHERE admin_id = :admin_id;";
        $r = $exec -> exec( $sql, $save );
        if( $r ) {
            echo 'Thành công';
        }
    }
?>
