<?php
    if( isset( $_POST['account_username'] ) ) {
        $exec = new Exec( HOST, USER, PASS, DBNAME );
        $hasher = new Password( 8, false );
        $mailer = new Smtp();

        //Get data
        foreach( $_POST as $key => $value ) {
            $$key = $value;
        }

        // Xóa bỏ khoảng cách trong số điện thoại
        $account_mobile = str_replace(' ', '', $account_mobile);

        // Kiểm tra xem có trùng email | username | mobile
        $sql = "SELECT COUNT(*) as admin FROM tp_admins WHERE (admin_id <> :id)  AND (admin_username = :username OR admin_email = :email OR admin_mobile = :mobile);";
        $r = $exec -> get( $sql, array(
            ':username' => $account_username,
            ':email' => $account_email,
            ':mobile' => $account_mobile,
            ':id' => isset($id) ? (int)$id : 0
        ) );

        if($r[0]['admin'] > 0) {
            echo '0|Trùng email, tên đăng nhập hoặc số điện thoại';
            return;
        }

        if( isset( $action ) && $action == 'edit' ) {
            $data = array(
                ':admin_fullname' => $account_fullname,
                ':admin_username' => $account_username,
                ':admin_id' => (int)$id,
                ':admin_email' => $account_email,
                ':admin_mobile' => $account_mobile
            );

            $sql = "UPDATE tp_admins SET admin_fullname = :admin_fullname, admin_username = :admin_username, admin_email = :admin_email, admin_mobile = :admin_mobile WHERE admin_id = :admin_id;";
            $r = $exec -> exec( $sql, $data );
            $r ? print('1|Sửa quản trị viên thành công') : print('0|Không ghi được vào cơ sở dữ liệu');
        }
        else {

            $data = array(
                ':admin_fullname' => $account_fullname,
                ':admin_username' => $account_username,
                ':admin_password' => $hasher -> HashPassword( $account_password ),
                ':admin_email' => $account_email,
                ':admin_mobile' => $account_mobile
            );

            // Gửi mail
            $msg = 'Tài khoản quản trị VWin2888 là: <ul><li>Username: <b>' . $account_username . '</b></li><li>Password: <b>' . $account_password . '</b></li><li>Đăng nhập tại: <a href="' . TP_REL_ROOT . '/admin">' . TP_REL_ROOT . '/admin</a></li>';
            $sent = $mailer -> send($account_email, 'Bạn được thêm vào danh sách quản trị VWin2888', $msg);

            if( $sent ) {
                $sql = "INSERT INTO tp_admins( admin_fullname, admin_username, admin_password, admin_email, admin_mobile ) VALUES( :admin_fullname, :admin_username, :admin_password, :admin_email, :admin_mobile );";
                $r = $exec -> add( $sql, $data );
                $r ? print('1|Tạo quản trị viên thành công') : print('0|Không ghi được vào cơ sở dữ liệu');
            }
            else {
                print('0|Địa chỉ email không khả dụng');
            }
        }
    }
?>
