<?php
    if( isset( $_POST['id'] ) ) {
        $exec = new Exec( HOST, USER, PASS, DBNAME );

        $sql = "DELETE FROM tp_admins WHERE admin_id = :admin_id;";
        $exec -> exec( $sql, array(
            ':admin_id' => (int)$_POST['id']
        ) );
    }
?>
