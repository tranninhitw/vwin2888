<script src="https://www.google.com/recaptcha/api.js"></script>
<section class="resetpass-container">
    <div class="resetpass-logo-wrapper">
        <h3 class="resetpass-logo">Cpanel</h3>
    </div>
    <div class="resetpass-wrapper">
        <h3 class="resetpass-title">PHỤC HỒI MẬT KHẨU</h3>
        <p id="resetpass-status"><i class="fa fa-exclamation-circle" aria-hidden="true"></i></p>
        <form class="resetpass-form" action="" method="post">
            <input class="resetpass-input" id="resetpass-info" type="text" placeholder="Nhập email" name="info" />
            <input class="resetpass-input" id="captcha" type="hidden" name="captcha" />
            {@captcha}
            <button id="resetpass-button" data-loading-text="<i class='fa fa-circle-o-notch fa-spin fa-3x fa-fw'></i>Đang xử lý..." disabled>LẤY MẬT KHẨU</button>
        </form>
    </div>
</section>
{@script}
