<?php
    $db = unserialize( TP_DB_PARAMS );
    $exec = new Exec( $db['DB_HOST'], $db['DB_USERNAME'], $db['DB_PASSWORD'], $db['DB_SCHEMA_NAME'] );
    $mailer = new Smtp();
    if( isset( $_POST['info'] ) ) {

        //Get variables
        $info = strip_tags( $_POST['info'] );
        $captcha = $_POST['captcha'];

        //Check captcha
        $captchaResponse = file_get_contents( 'https://www.google.com/recaptcha/api/siteverify?secret=' . TP_CAPTCHA_SECRET . '&response=' . $captcha . '&remoteip=' . $_SERVER['REMOTE_ADDR'] );
        $captchaResponse = json_decode($captchaResponse, true);

        if( $captchaResponse['success'] == false ) {
            echo 'Vui lòng xác nhận captcha...';
            exit;
        }

        //Remove all special characters
        $info = preg_replace( '/[^a-zA-Z0-9@\.]+/', '', $info );

        //Check info (emai or username)
        $getData = array(
            ':admin_username' => $info,
            ':admin_email' => $info
        );

        $prefix = $db['DB_TABLE_PREFIX'];
        $sql = "SELECT * FROM " . $prefix . "admins WHERE admin_username = :admin_username OR admin_email = :admin_email LIMIT 1;";
        $result = $exec -> get( $sql, $getData );

        if( count( $result ) != 1 ) {
            echo 'Người dùng không tồn tại!';
            exit;
        }

        //Generate resetpass string
        $resetString = str_shuffle( md5( microtime() ) );
        $execData = array(
            ':string' => $resetString,
            ':admin_email' => $info
        );
        $sql2 = "UPDATE " . $prefix . "admins SET admin_resetpass_string = :string WHERE admin_email = :admin_email;";
        $exec -> exec( $sql2, $execData );

        //Send mail
        $subject = '[VWin2888] Phục hồi mật khẩu';
        $msg = 'Click vào đường dẫn bên dưới để đặt lại mật khẩu: <br />' ;
        $resetLink = TP_REL_ROOT . 'nguoidung/doimatkhau?email=' . urlencode( $info ) . '&string=' . urlencode( $resetString );
        $msg .= '<a href="' . $resetLink . '">' . $resetLink . '</a><br />';
        $r = $mailer -> send( $info, $subject, $msg );
        $r ? print( 'success' ) : print( 'failed' );
    }
?>
