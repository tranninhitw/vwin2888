window.onload = function() {
    var infoInput = document.getElementById( 'resetpass-info' );
    var captchaInput = document.getElementById( 'captcha' );
    var status = document.getElementById( 'resetpass-status' );
    var resetBtn = document.getElementById( 'resetpass-button' );

    /*Focus input email*/
    infoInput.focus();

    /*Login button click*/
    resetBtn.addEventListener( 'click', function( e ) {
        e.preventDefault();
        this.innerHTML = this.getAttribute( 'data-loading-text' );

        //Ajax
        var info = infoInput.value;
        var captcha = captchaInput.value;
        var xhttp = new XMLHttpRequest();

        xhttp.onreadystatechange = function() {
            if ( xhttp.readyState == 4 && xhttp.status == 200 ) {

                if( xhttp.responseText == 'success' ) {
                    status.innerHTML = '<i class="fa fa-check" aria-hidden="true"></i>Vui lòng kiểm tra mail để đặt lại mật khẩu.';
                    status.classList.add( 'success' );
                }
                else {
                    status.innerHTML = '<i class="fa fa-exclamation-circle" aria-hidden="true"></i>' + xhttp.responseText;
                }
                resetBtn.innerHTML = 'Lấy mật khẩu';
                status.style.display = 'block';
            }
        };
        xhttp.open( 'POST', 'include/4', true );
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send( 'info=' + info + '&captcha=' + captcha );
    }, false );

    /*Enable login button*/
    addListenerMulti( infoInput, 'keyup change', function() {
        captchaCallback();
    } );
};

function addListenerMulti(el, s, fn) {
  var evts = s.split(' ');
  for (var i=0, iLen=evts.length; i<iLen; i++) {
    el.addEventListener(evts[i], fn, false);
  }
};

function captchaCallback(response) {
    var infoInput = document.getElementById( 'resetpass-info' );
    var resetBtn = document.getElementById( 'resetpass-button' );
    var captchaInput = document.getElementById( 'captcha' );
    var info = infoInput.value;
    var captchaLen = response ? response.length : 0;
    if( info != '' && captchaLen > 0 ) {
        captchaInput.value = response;
        resetBtn.disabled = false;
    }
    else {
        captchaInput.value = '';
        resetBtn.disabled = true;
    }
};
