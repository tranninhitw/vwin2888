<?php
    if( isset( $_POST['post_id'] ) ) {
        $exec = new Exec( HOST, USER, PASS, DBNAME );

        $sql = "DELETE FROM tp_posts WHERE post_id = :post_id;";
        $r = $exec -> exec( $sql, array(
            ':post_id' => (int)$_POST['post_id']
        ) );

        $r ? print( '1|Xóa bài viết thành công' ) : print( '0|Có lỗi khi cố xóa bài viết' );
    }
?>
