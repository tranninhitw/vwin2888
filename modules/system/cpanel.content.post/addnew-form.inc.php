<?php

    $exec = new Exec( HOST, USER, PASS, DBNAME );

    // Get all cates
    $sql = "SELECT * FROM tp_cates";
    $cates = $exec -> get($sql);
    $cateList = '';
    foreach($cates as $key => $cate) {
        $cateList .= '<span data-id="' . $cate['cate_id'] . '">' . $cate['cate_name'] . '</span>';
    }


    if( isset( $_GET['action'] ) && $_GET['action'] == 'edit' ) {
        $id = (int)$_GET['id'];

        $sql = "SELECT * FROM tp_posts WHERE post_id = :id;";
        $post = $exec -> get( $sql, array(
            ':id' => $id
        ) );
        $post = $post[0];

        $images = json_decode( $post['post_thumbnail'], true );
        $images = $images[0];

        $checked = $post['post_allow_comment'] == '1' ? ' has-checked" checked' : '"';
        $checked2 = $post['post_automatic'] == '1' ? ' has-checked" checked' : '"';
        $displayDatePicker = $post['post_automatic'] == '1' ? '' : 'display: none;';

        $chosenCates = '';
        $cateIds = array_filter( explode( ',', $post['cate_id'] ), 'strlen' );
        foreach( $cateIds as $id ) {
            $text = '';
            foreach($cates as $key => $cate) {
                if($cate['cate_id'] == $id) {
                    $text = $cate['cate_name'];
                }
            }
            $chosenCates .= '
                <span data-id="' . $id . '">' . $text . ' <i class="fa fa-times" aria-hidden="true"></i></span>
            ';
        }

        $timeToPost = date('d-m-Y H:i', $post['post_time_to_post']);

        $html = '
            <table class="form-tables tab1-tbl">
                <tr>
                    <td>Tiêu đề (*)</td>
                    <td>
                        <input value="' . $post['post_title'] . '" type="text" name="post_title" class="inputs post-textbox required-inputs post-title" placeholder="Tiêu đề" />
                    </td>
                </tr>
                <tr>
                    <td>Mô tả ngắn (*)</td>
                    <td>
                        <textarea name="post_compact" class="inputs post-textbox required-inputs" placeholder="Mô tả ngắn">' . $post['post_compact'] . '</textarea>
                    </td>
                </tr>
                <tr>
                    <td>Danh mục (*)</td>
                    <td>
                        <div class="suggest-lists">
                            <input type="text" class="inputs suggest-list-textbox" placeholder="Chọn danh mục" />
                            <div class="suggest-list-panel">
                                ' . $cateList . '
                            </div>
                        </div>
                        <div class="post-selected suggest-list-result">
                            <input value="' . $post['cate_id'] . '" name="post_cate" type="text" class="inputs post-textbox required-inputs hidden" readonly />
                            ' . $chosenCates . '
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Nội dung (*)</td>
                    <td>
                        <textarea name="post_content" class="inputs wysiwyg post-textbox required-inputs" placeholder="Nội dung">' . $post['post_contents'] . '</textarea>
                    </td>
                </tr>
            </table>
        </div>
        <input type="radio" name="tab" id="tab2" class="tab-radio" />
        <label for="tab2" class="tab-label">SEO</label>
        <div class="tab-content">
            <table class="form-tables">
                <tr>
                    <td>SEO url (*)</td>
                    <td>
                        <input value="' . $post['post_seo_url'] . '" name="post_url" type="text" class="inputs post-textbox required-inputs post-url" placeholder="Seo url" />
                    </td>
                </tr>
                <tr>
                    <td>Page title (*)</td>
                    <td>
                        <input value="' . $post['post_page_title'] . '" name="post_page_title" type="text" class="inputs post-textbox required-inputs" placeholder="Meta title" />
                    </td>
                </tr>
                <tr>
                    <td>Meta description (*)</td>
                    <td>
                        <textarea name="post_meta_description" class="inputs post-textbox required-inputs" placeholder="Meta description">' . $post['post_meta_description'] . '</textarea>
                    </td>
                </tr>
                <tr>
                    <td>Meta keywords (*)</td>
                    <td>
                        <textarea name="post_meta_keywords" class="inputs post-textbox required-inputs" placeholder="Meta keywords">' . $post['post_meta_keywords'] . '</textarea>
                    </td>
                </tr>
            </table>
        </div>
        <input type="radio" name="tab" id="tab3" class="tab-radio" />
        <label for="tab3" class="tab-label">Dữ liệu</label>
        <div class="tab-content">
            <table class="form-tables tab2-tbl">
                <tr>
                    <td>Hình đại diện (*)</td>
                    <td>
                        <button class="upload-btn buttons hidden">Upload</button>
                        <input value="' . htmlspecialchars( $post['post_thumbnail'] , ENT_QUOTES ) . '" name="post_avatar" type="text" class="inputs post-textbox hidden post-image" readonly />
                        <input type="file" class="file-inputs post-textbox" id="post-image" />
                        <label for="post-image">
                            <span>Đổi hình ảnh</span>
                            <span>Chưa chọn file...</span>
                        </label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <img class="old-img" src="' . TP_REL_ROOT . 'uploads/public/' . $images . '" />
                    </td>
                </tr>
                <tr>
                    <td>Cho phép bình luận</td>
                    <td>
                        <input name="post_allow_comment" id="allow-cmt" type="checkbox" class="checkboxes post-textbox' . $checked . ' />
                        <label for="allow-cmt"></label>
                    </td>
                </tr>
                <tr>
                    <td>Hẹn giờ đăng</td>
                    <td>
                        <input name="post_automatic" id="set-automatic" type="checkbox" class="js_datepicker_toggle checkboxes post-textbox' . $checked2 . ' />
                        <label for="set-automatic"></label>
                    </td>
                </tr>
                <tr style="' . $displayDatePicker . '">
                    <td>Thời gian đăng</td>
                    <td>
                        <input name="post_time_to_post" type="text" class="js_datepicker datetimepicker inputs post-textbox" placeholder="Chọn ngày giờ" value="' . $timeToPost . '" />
                    </td>
                </tr>
            </table>
        ';
    }
    else {
        $html = '
            <table class="form-tables tab1-tbl">
                <tr>
                    <td>Tiêu đề (*)</td>
                    <td>
                        <input type="text" name="post_title" class="inputs post-textbox required-inputs post-title" placeholder="Tiêu đề" />
                    </td>
                </tr>
                <tr>
                    <td>Mô tả ngắn (*)</td>
                    <td>
                        <textarea name="post_compact" class="inputs post-textbox required-inputs" placeholder="Mô tả ngắn"></textarea>
                    </td>
                </tr>
                <tr>
                    <td>Danh mục (*)</td>
                    <td>
                        <div class="suggest-lists">
                            <input type="text" class="inputs suggest-list-textbox" placeholder="Chọn danh mục" />
                            <div class="suggest-list-panel">
                                ' . $cateList . '
                            </div>
                        </div>
                        <div class="post-selected suggest-list-result">
                            <input name="post_cate" type="text" class="inputs post-textbox required-inputs hidden" readonly />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Nội dung (*)</td>
                    <td>
                        <textarea name="post_content" class="inputs wysiwyg post-textbox required-inputs" placeholder="Nội dung"></textarea>
                    </td>
                </tr>
            </table>
        </div>
        <input type="radio" name="tab" id="tab2" class="tab-radio" />
        <label for="tab2" class="tab-label">SEO</label>
        <div class="tab-content">
            <table class="form-tables">
                <tr>
                    <td>SEO url (*)</td>
                    <td>
                        <input name="post_url" type="text" class="inputs post-textbox required-inputs post-url" placeholder="Seo url" />
                    </td>
                </tr>
                <tr>
                    <td>Page title (*)</td>
                    <td>
                        <input name="post_page_title" type="text" class="inputs post-textbox required-inputs" placeholder="Meta title" />
                    </td>
                </tr>
                <tr>
                    <td>Meta description (*)</td>
                    <td>
                        <textarea name="post_meta_description" class="inputs post-textbox required-inputs" placeholder="Meta description"></textarea>
                    </td>
                </tr>
                <tr>
                    <td>Meta keywords (*)</td>
                    <td>
                        <textarea name="post_meta_keywords" class="inputs post-textbox required-inputs" placeholder="Meta keywords"></textarea>
                    </td>
                </tr>
            </table>
        </div>
        <input type="radio" name="tab" id="tab3" class="tab-radio" />
        <label for="tab3" class="tab-label">Dữ liệu</label>
        <div class="tab-content">
            <table class="form-tables tab2-tbl">
                <tr>
                    <td>Hình đại diện (*)</td>
                    <td>
                        <button class="upload-btn buttons hidden">Upload</button>
                        <input name="post_avatar" type="text" class="inputs post-textbox hidden post-image" readonly />
                        <input type="file" class="file-inputs post-textbox required-inputs" id="post-image" />
                        <label for="post-image">
                            <span>Chọn hình ảnh</span>
                            <span>Chưa chọn file...</span>
                        </label>
                    </td>
                </tr>
                <tr>
                    <td>Cho phép bình luận</td>
                    <td>
                        <input name="post_allow_comment" id="allow-cmt" type="checkbox" class="checkboxes post-textbox" checked />
                        <label for="allow-cmt"></label>
                    </td>
                </tr>
                <tr>
                    <td>Hẹn giờ đăng</td>
                    <td>
                        <input name="post_automatic" id="set-automatic" type="checkbox" class="js_datepicker_toggle checkboxes post-textbox" />
                        <label for="set-automatic"></label>
                    </td>
                </tr>
                <tr style="display: none;">
                    <td>Thời gian đăng</td>
                    <td>
                        <input name="post_time_to_post" type="text" class="datetimepicker inputs post-textbox" placeholder="Chọn ngày giờ" />
                    </td>
                </tr>
            </table>
        ';
    }
    echo $html;
?>
