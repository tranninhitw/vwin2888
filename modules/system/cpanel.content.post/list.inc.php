<?php
    $exec = new Exec( HOST, USER, PASS, DBNAME );

    if( isset( $_POST['offset'] ) ) {
        $offset = (int)$_POST['offset'];
    }
    else $offset = 0;

    $html = '';
    $sql = "SELECT * FROM tp_posts ORDER BY post_time DESC LIMIT 5 OFFSET :offset;";
    $posts = $exec -> get( $sql, array(
        ':offset' => $offset
    ), true );

    if( count( $posts ) == 0 ) {
        $html = '
            <tr>
                <td colspan="5" style="text-align: center;">Hiện chưa có bài viết</td>
            </tr>
        ';
    }

    foreach( $posts as $key => $value ) {

        // Count comments
        $sql = "SELECT COUNT(*) AS amount FROM tp_comments WHERE comment_url = :comment_url;";
        $comment = $exec -> get( $sql, array(
            ':comment_url' => $value['post_seo_url']
        ) );

        $cate = '';
        $cateArr = array_filter( explode( ',', $value['cate_id'] ), 'strlen' );
        foreach( $cateArr as $value2 ) {
            $sql = "SELECT cate_name FROM tp_cates WHERE cate_id = :id;";
            $cate2 = $exec -> get( $sql, array(
                ':id' => $value2
            ) );
            $cate .= isset($cate2[0]) ? $cate2[0]['cate_name'] . ',' : '-,';
        }
        $cate = trim( $cate, ',' );

        $html .= '
            <tr>
                <td><a href="' . TP_REL_ROOT . $value['post_seo_url'] . '" target="_blank">' . ucfirst( mb_convert_case( $value['post_title'], MB_CASE_LOWER, 'UTF-8' ) ) . '</a></td>
                <td>' . date( 'H:i:s d/m/Y', $value['post_time'] ) . '</td>
                <td>
                    ' . $cate . '
                </td>
                <td>
                    <a href="' . TP_REL_ROOT . 'admin/noidung/binhluan?filter=1&url=' . $value['post_seo_url'] . '">' . $comment[0]['amount'] . '</a>
                </td>
                <td>
                    <button data-id="' . $value['post_id'] . '" class="mini-buttons normal-buttons edit-post" title="Sửa"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                    <button data-id="' . $value['post_id'] . '" class="mini-buttons cancel-buttons delete-post" title="Xóa"><i class="fa fa-trash" aria-hidden="true"></i></button>
                </td>
            </tr>
        ';
    }
    echo $html;
?>
