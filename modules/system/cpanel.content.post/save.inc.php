<?php
    if( isset( $_POST['post_title'] ) ) {
        //Init OOP
        $exec = new Exec( HOST, USER, PASS, DBNAME );

        //Get data
        foreach( $_POST as $key => $value ) {
            $$key = $value;
        }

        //Get admin id
        $userData = unserialize( $_SESSION['tk_user_data'] );

        $data = array(
            ':admin_id' => $userData['admin_id'],
            ':cate_id' => $post_cate,
            ':post_title' => $post_title,
            ':post_page_title' => $post_page_title,
            ':post_contents' => $post_content,
            ':post_compact' => $post_compact,
            ':post_meta_description' => $post_meta_description,
            ':post_meta_keywords' => $post_meta_keywords,
            ':post_seo_url' => preg_replace( '/[^0-9a-zA-Z- ]/', '', $post_url ),
            ':post_allow_comment' => isset( $post_allow_comment ) ? 1 : 0,
            ':post_automatic' => isset( $post_automatic ) ? 1 : 0,
            ':post_thumbnail' => $post_avatar,
            ':post_time' => time(),
            ':post_time_to_post' => empty($post_time_to_post) ? NULL : strtotime($post_time_to_post)
        );

        if( isset( $action ) && $action == 'edit' ) {
            $data[':post_id'] = (int)$id;
            $sql = "UPDATE tp_posts SET admin_id = :admin_id, cate_id = :cate_id, post_title = :post_title, post_contents = :post_contents, post_thumbnail = :post_thumbnail, post_compact = :post_compact, post_meta_description = :post_meta_description, post_meta_keywords = :post_meta_keywords, post_page_title = :post_page_title, post_seo_url = :post_seo_url, post_allow_comment = :post_allow_comment, post_automatic = :post_automatic, post_time = :post_time, post_time_to_post = :post_time_to_post WHERE post_id = :post_id;";
            $r = $exec -> exec( $sql, $data );
        }
        else {
            $sql = "INSERT INTO tp_posts ( admin_id, cate_id, post_title, post_page_title, post_contents, post_compact, post_meta_description, post_meta_keywords, post_seo_url, post_allow_comment, post_automatic, post_thumbnail, post_time, post_time_to_post ) VALUES( :admin_id, :cate_id, :post_title, :post_page_title, :post_contents, :post_compact, :post_meta_description, :post_meta_keywords, :post_seo_url, :post_allow_comment, :post_automatic, :post_thumbnail, :post_time, :post_time_to_post )";
            $r = $exec -> add( $sql, $data );
        }
        $r ? print( '1|Lưu bài viết thành công' ) : print( '0|Trùng URL' );
    }
?>
