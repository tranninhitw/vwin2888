<?php
    $exec = new Exec( HOST, USER, PASS, DBNAME );

    if( isset( $_POST['offset'] ) ) {
        $offset = (int)$_POST['offset'];
    }
    else $offset = 0;

    $html = '';

    //Get comment

    if(isset($_GET['filter']) && isset($_GET['url'])) {
        // Khi lấy comment của 1 bài viết
        $sql = "SELECT * FROM tp_comments WHERE comment_url = :url ORDER BY comment_id DESC LIMIT 5 OFFSET :offset;";
        $comments = $exec -> get( $sql, array(
            ':offset' => $offset,
            ':url' => $_GET['url']
        ), true );
    } else {
        // Khi lấy tất cả comment
        $sql = "SELECT * FROM tp_comments ORDER BY comment_id DESC LIMIT 5 OFFSET :offset;";
        $comments = $exec -> get( $sql, array(
            ':offset' => $offset
        ), true );
    }

    if( count( $comments ) !== 0 ) {
        foreach( $comments as $key => $comment ) {
            $url = $comment['comment_url'];
            $contents = $comment['comment_contents'];
            $time = date( 'H:i:s d/m/Y', $comment['comment_time'] );
            $block = $show = '';
            if( !$comment['comment_publish'] ) {
                $show = 'fa-eye-slash';
                $block = 'class="block"';
            }
            $html .= '
                <tr ' . $block . ' >
                    <td><a href="' . TP_REL_ROOT . $url . '" target="blank">' . $url . '</a></td>
                    <td>' . $contents . '</td>
                    <td>' . $time . '</td>
                    <td>
                        <button class="mini-buttons normal-buttons block-comment" data-id="' . $comment['comment_id'] . '"><i class="fa fa-eye ' . $show . '" aria-hidden="true"></i></button>
                    </td>
                </tr>
            ';
        }
    } else {
        $html .= '
            <tr>
                <td colspan="4">Chưa có bình luận nào</td>
            </tr>
        ';
    }

    echo $html;
?>
