<?php
    if( isset( $_POST['comment_id'] ) ) {
        $exec = new Exec( HOST, USER, PASS, DBNAME );

        $data = array(
            'comment_id' => $_POST['comment_id']
        );
        $sql = "SELECT comment_publish FROM tp_comments WHERE comment_id = :comment_id;";
        $block = $exec -> get( $sql, $data );
        $block = $block[0]['comment_publish'];
        $block = $block ? 0 : 1;
        $save = array(
            'comment_id' => $_POST['comment_id'],
            'comment_publish' => $block
        );
        $sql = "UPDATE tp_comments SET comment_publish = :comment_publish WHERE comment_id = :comment_id;";
        $r = $exec -> exec( $sql, $save );
        if( $r ) {
            echo 'Thành công';
        } else {
            echo "Thất bại";
        }
    }
?>
