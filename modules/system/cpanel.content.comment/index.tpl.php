{@style}
<section class="module-wrapper">
    <section class="module-toolbar">
        <h3 class="module-heading">Nội dung / Bình luận <small>Cpanel</small></h3>
    </section>
    <section class="module-content">
        <div class="comment-list-wrapper">
            <table class="tables module comment-table">
                <tr class="title-rows">
                    <col width="30%">
                    <col width="40%">
                    <col width="20%">
                    <col width="10%">
                    <td>URL</td>
                    <td>Nội dung</td>
                    <td>Thời gian</td>
                    <td>Biên tập</td>
                </tr>
                {@list}
            </table>
        </div>
        {@pager}
    </section>
</section>
{@script}
