<?php
    $exec = new Exec( HOST, USER, PASS, DBNAME );

    $sql = "SELECT COUNT(*) AS post_amount FROM tp_posts;";
    $postAmount = $exec -> get($sql);
    $postAmount = $postAmount[0]['post_amount'];

    $sql = "SELECT COUNT(*) AS comment_amount FROM tp_comments;";
    $commentAmount = $exec -> get($sql);
    $commentAmount = $commentAmount[0]['comment_amount'];

    $sql = "SELECT COUNT(*) AS conversation_amount FROM tp_conversations;";
    $conversationAmount = $exec -> get($sql);
    $conversationAmount = $conversationAmount[0]['conversation_amount'];

    $html = '
        <div class="panel-1 status-panel">
            <span class="status-number">' . $postAmount . '</span>
            <p class="status-title">Bài viết</p>
            <p class="status-link">
                <a href="{@www}/admin/noidung/baiviet">Xem tất cả</a>
            </p>
        </div>
        <div class="panel-2 status-panel">
            <span class="status-number">' . $commentAmount . '</span>
            <p class="status-title">Bình luận</p>
            <p class="status-link">
                <a href="{@www}/admin/noidung/binhluan">Xem tất cả</a>
            </p>
        </div>
        <div class="panel-3 status-panel">
            <span class="status-number">' . $conversationAmount . '</span>
            <p class="status-title">Tin nhắn</p>
            <p class="status-link">
                <a href="{@www}/admin">Xem tất cả</a>
            </p>
        </div>
    ';
    echo $html;
?>
