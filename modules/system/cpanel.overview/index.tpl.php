{@style}
<section class="module-wrapper">
    <section class="module-toolbar">
        <h3 class="module-heading">Trang tổng quan <small>Cpanel</small></h3>
    </section>
    <section class="module-content">
        <div class="overview-panel">
            {@dashboard}
        </div>
        <div class="chat">
            <div class="aside">
                <div class="friends_list ok">
                    <div class="friend active fcenter">
                        <div class="avatarbox">
                            <div class="avatar_overlay"><img src="https://s-media-cache-ak0.pinimg.com/736x/c9/b8/87/c9b8873c63d378702f5b932d6acfa28b.jpg"/></div>
                        </div>
                        <div class="namemsg">
                            <p class="b">Gato catzera</p>
                            <p class="msg">is typing...</p>
                        </div>
                        <div class="timeago">
                            <p>20 mins ago</p>
                        </div>
                    </div>
                    <div class="friend fcenter">
                        <div class="avatarbox">
                            <div class="avatar_overlay"><img src="https://smilingpaws.files.wordpress.com/2014/01/british-shorthair-cat.jpg"/></div>
                        </div>
                        <div class="namemsg">
                            <p class="b">Crazy catzera</p>
                            <p class="msg">Hey dude.</p>
                        </div>
                        <div class="timeago">
                            <p>11 July</p>
                        </div>
                    </div>
                    <div class="friend fcenter">
                        <div class="avatarbox">
                            <div class="avatar_overlay"><img src="https://s-media-cache-ak0.pinimg.com/236x/94/16/dc/9416dc9dd2b803dddea668fd5100e618.jpg"/></div>
                        </div>
                        <div class="namemsg">
                            <p class="b">Crazy doge</p>
                            <p class="msg">Is it a ball on your avatar?</p>
                        </div>
                        <div class="timeago">
                            <p>11 July</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="midcont">
                <div class="head">
                    <div class="avatarbox">
                        <div class="avatar_overlay"><img src="https://s-media-cache-ak0.pinimg.com/736x/c9/b8/87/c9b8873c63d378702f5b932d6acfa28b.jpg"/></div>
                    </div>
                    <h4>Gato catzera <small>abc@gmail.com</small></h4>
                    <div class="configschat"><i class="fa fa-ellipsis-v"></i></div>
                </div>
                <div class="messagescont">
                    <div class="msg">
                        <div class="user me t">
                            <div class="avatarbox">
                                <div class="avatar_overlay"><img src="https://s-media-cache-ak0.pinimg.com/736x/c9/b8/87/c9b8873c63d378702f5b932d6acfa28b.jpg"/></div>
                            </div>
                            <div class="text">
                                Did you eat all my tuna?
                                I had like 10 cans saved for the shark week marathon.  🍣🍣🍣🍣
                            </div>
                        </div>
                    </div>
                    <div class="msg">
                        <div class="user friend t">
                            <div class="avatarbox">
                                <div class="avatar_overlay"><img src="https://smilingpaws.files.wordpress.com/2014/01/british-shorthair-cat.jpg"/></div>
                            </div>
                            <div class="text">
                                Sorry Gato
                                <br>I made a tuna pasta with tomatoes and catnip.
                                <br>Delicious... #chef #cat
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bottomchat">
                    <textarea class="text" placeholder="Nhập tin nhắn..."></textarea>
                    <i class="fa fa-paper-plane"></i>
                </div>
            </div>
        </div>
    </section>
</section>
{@script}
