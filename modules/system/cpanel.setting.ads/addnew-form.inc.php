<?php
    $exec = new Exec( HOST, USER, PASS, DBNAME );

    if( isset( $_GET['action'] ) && $_GET['action'] == 'edit' ) {
        // Get all cates
        $sql = "SELECT * FROM tp_advertises WHERE advertise_id = :id;";
        $ads = $exec -> get($sql, array(
            ':id' => (int)$_GET['id']
        ));
        $ad = $ads[0];

        $target = $ad['advertise_target'] == '_blank' ? ' has-checked" checked' : '"';
        $image = json_decode($ad['advertise_image'], true);

        $html = '
            <table class="form-tables addnew-slide">
                <tr>
                    <td>Tên quảng cáo (*)</td>
                    <td>
                        <input type="hidden" class="inputs slider-image-url-addnew" name="slider_image" value="' . htmlspecialchars($ad['advertise_image'], ENT_QUOTES) . '" readonly />
                        <input type="text" class="inputs required-inputs" name="slider_name" value="' . $ad['advertise_name'] . '" />
                    </td>
                </tr>
                <tr>
                    <td>Đường dẫn (*)</td>
                    <td>
                        <input type="text" class="inputs required-inputs" name="slider_url" value="' . $ad['advertise_link'] . '" />
                    </td>
                </tr>
                <tr>
                    <td>Nội dung khi rê chuột vào (*)</td>
                    <td>
                        <input type="text" class="inputs slider-title required-inputs" name="slider_title" value="' . $ad['advertise_title'] . '" />
                    </td>
                </tr>
                <tr>
                    <td>Mở ở tab mới</td>
                    <td>
                        <input id="target" type="checkbox" class="checkboxes inputs slider-target' . $target . ' name="slider_target" />
                        <label for="target"></label>
                    </td>
                </tr>
                <tr>
                    <td>Hình ảnh (*)</td>
                    <td>
                        <input type="file" class="file-inputs" id="slider-image" accept="image/*" />
                        <label for="slider-image">
                            <span>Chọn hình ảnh</span>
                            <span>Chưa chọn file...</span>
                        </label>
                    </td>
                </tr>
                <tr>
                    <td>Hình hiện tại</td>
                    <td>
                        <img class="old-img" src="' . TP_REL_ROOT . 'uploads/public/' . $image[0] . '" />
                    </td>
                </tr>
            </table>
        ';

        echo $html;
        return;
    }
?>

<table class="form-tables addnew-slide">
    <tr>
        <td>Tên quảng cáo (*)</td>
        <td>
            <input type="hidden" class="inputs slider-image-url-addnew" name="slider_image" readonly />
            <input type="text" class="inputs required-inputs" name="slider_name" />
        </td>
    </tr>
    <tr>
        <td>Đường dẫn (*)</td>
        <td>
            <input type="text" class="inputs required-inputs" name="slider_url" />
        </td>
    </tr>
    <tr>
        <td>Nội dung khi rê chuột vào (*)</td>
        <td>
            <input type="text" class="inputs slider-title required-inputs" name="slider_title" />
        </td>
    </tr>
    <tr>
        <td>Mở ở tab mới</td>
        <td>
            <input id="target" type="checkbox" class="checkboxes inputs slider-target" name="slider_target" />
            <label for="target"></label>
        </td>
    </tr>
    <tr>
        <td>Hình ảnh (*)</td>
        <td>
            <input type="file" class="file-inputs required-inputs" id="slider-image" accept="image/*" />
            <label for="slider-image">
                <span>Chọn hình ảnh</span>
                <span>Chưa chọn file...</span>
            </label>
        </td>
    </tr>
</table>
