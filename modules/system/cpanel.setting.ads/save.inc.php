<?php
    if( isset( $_POST ) ) {
        $exec = new Exec( HOST, USER, PASS, DBNAME );

        //Get data
        foreach( $_POST as $key => $value ) {
            $$key = $value;
        }

        // When edit
        if( isset( $action ) && $action == 'edit' ) {
            $sql = "UPDATE tp_advertises SET advertise_name = :advertise_name, advertise_link = :advertise_link, advertise_target = :advertise_target, advertise_title = :advertise_title, advertise_start_time = :advertise_start_time, advertise_image = :advertise_image WHERE advertise_id = :advertise_id;";
            $r = $exec -> exec($sql, array(
                ':advertise_name' => $slider_name,
                ':advertise_link' => $slider_url,
                ':advertise_target' => isset($slider_target) ? '_blank' : '_self',
                ':advertise_title' => $slider_title,
                ':advertise_start_time' => time(),
                ':advertise_image' => $slider_image,
                ':advertise_id' => (int)$id
            ));

            $r ? print( '1|Lưu thành công' ) : print( '0|Trùng tên quảng cáo' );

            return;
        }

        //add new
        $sql = "INSERT INTO tp_advertises(advertise_name, advertise_link, advertise_target, advertise_title, advertise_start_time, advertise_image) VALUES (:advertise_name, :advertise_link, :advertise_target, :advertise_title, :advertise_start_time, :advertise_image);";
        $r = $exec -> exec( $sql, array(
            ':advertise_name' => $slider_name,
            ':advertise_link' => $slider_url,
            ':advertise_target' => isset($slider_target) ? '_blank' : '_self',
            ':advertise_title' => $slider_title,
            ':advertise_start_time' => time(),
            ':advertise_image' => $slider_image
        ) );

        $r ? print( '1|Lưu thành công' ) : print( '0|Trùng tên quảng cáo' );
    }
?>
