$( document ).ready( function() {

    var PARAMS = ULTI.queryString();

    /*Click edit button*/
    $( document ).off( 'click', '.edit-slider' ).on( 'click', '.edit-slider', function() {
        DOM.loadTplThisEl( _WWW + 'admin/caidat/quangcao?action=edit&id=' + $( this ).data( 'id' ) );
    } );

    /*When edit*/
    $( document ).ready( function() {
        if( PARAMS.action == 'edit' ) {
            //Fade In addnew form
            $( '.slider-addnew-btn' ).click();
        }
    } );

    /*When click addnew*/
    $( document ).on( 'click', '.slider-addnew-btn', function() {
        MODULE.cmd( 'addnew', $( '.slider-cancel-btn' ) );
    } );

    /*When click cancel*/
    $( document ).on( 'click', '.slider-cancel-btn', function() {
        MODULE.cmd( 'cancel', $( '.slider-cancel-btn' ) );
    } );

    /*When input textbox of addnew*/
    $( document ).off( 'input change', '.addnew-slide input' ).on( 'input change', '.addnew-slide input', function() {
        var condition = ULTI.validate( $( '.addnew-slide .required-inputs' ), 'empty' );
        if( condition ) {
            DOM.enableSaveBtn();
        }
        else {
            DOM.disableSaveBtn();
        }
    } );

    /*When click on save new slide*/
    $( document ).off( 'click', '.insert-new-btn' ).on( 'click', '.insert-new-btn', function(){
        //Upload image
        var title = $( '.slider-title' ).val().latinise();
        var callback = function( html ) {
            $( '.slider-image-url-addnew' ).val( html );
        };
        var file = $( '#slider-image' )[0].files;
        if( file.length > 0 ) {
            ULTI.upload( file, 'public', title, callback );
        }

        var data = $( '.addnew-slide input' ).serialize();

        if( PARAMS.action == 'edit' ) {
            data += '&action=edit&id=' + PARAMS.id;
        }

        var url = 'include/40';
        var callback2 = function( html ) {
            console.log(html);
            var htmlArr = html.split( '|' );
            if( htmlArr[0] == '1' ) {
                ULTI.done( htmlArr[1] );
            }
            else {
                DOM.showStatus( htmlArr[1], parseInt( htmlArr[0] ) );
            }
        };
        AJAX.post( url, data, callback2 );
    } );

    /*Delete slide*/
    $( document ).off( 'click', '.slide-delete' ).on( 'click', '.slide-delete', function() {
        var deleteFn = function( target ) {
            var id = target.data( 'id' );
            var url = 'include/39';
            var data = {
                'id': id
            };
            var callback = function( html ) {
                ULTI.done( 'Xóa thành công' );
            };
            AJAX.post( url, data, callback );
        };
        var text = 'Bạn có chắc chắn xóa quảng cáo này?';
        DOM.showPopup( text, deleteFn, $( this ) );
    } );
} );
