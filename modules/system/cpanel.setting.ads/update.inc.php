<?php 
    if( isset( $_POST['data'] ) ) {
        $exec = new Exec( HOST, USER, PASS, DBNAME );
        $sql = new Sql();
        
        $data = json_decode( $_POST['data'], true );
        
        //add new slide into db
        $oldSlider = $exec -> get( $sql -> get( 180 ) , array( 
            ':setting_name' => 'sliders'
        ) );
        
        $oldSlider = json_decode( $oldSlider[0]['setting_value'], true );
        
        //Replace new slide
        $oldSlider[0]['slider_item'] = json_decode( $_POST['data'], true );
        
        $r = $exec -> exec( $sql -> get( 158 ), array(
            ':setting_value' => json_encode( $oldSlider ),
            ':setting_name' => 'sliders'
        ) );
        
        $r ? print( '1|Lưu thành công' ) : print( '0|Có lỗi xảy ra' );
        
    }
?>