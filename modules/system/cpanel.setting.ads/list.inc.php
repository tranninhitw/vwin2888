<?php
    $exec = new Exec( HOST, USER, PASS, DBNAME );

    $html = '';
    $sql = "SELECT * FROM tp_advertises;";
    $ads = $exec -> get( $sql );

    if( count( $ads ) == 0 ) {
        $html = '
            <tr>
                <td colspan="7" style="text-align: center;">Hiện chưa có quảng cáo</td>
            </tr>
        ';
    }

    foreach( $ads as $key => $ad ) {
        $image = json_decode($ad['advertise_image'], true);
        $html .= '
            <tr>
                <td>' . $ad['advertise_name'] . '</td>
                <td><img src="' . TP_REL_ROOT . 'uploads/public/' . $image[0] . '" /></td>
                <td><a href="' . $ad['advertise_link'] . '" target="_blank">' . $ad['advertise_link'] . '</a></td>
                <td>' . $ad['advertise_title'] . '</td>
                <td>' . $ad['advertise_click_amount'] . '</td>
                <td>' . date('d/m/Y', $ad['advertise_start_time']) . '</td>
                <td>
                    <button data-id="' . $ad['advertise_id'] . '" class="mini-buttons normal-buttons edit-slider" title="Sửa"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                    <button data-id="' . $ad['advertise_id'] . '" class="mini-buttons cancel-buttons slide-delete" title="Xóa"><i class="fa fa-trash" aria-hidden="true"></i></button>
                </td>
            </tr>
        ';
    }
    echo $html;
?>
