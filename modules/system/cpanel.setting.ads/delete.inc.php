<?php
    if( isset( $_POST['id'] ) ) {
        $exec = new Exec( HOST, USER, PASS, DBNAME );

        $sql = "DELETE FROM tp_advertises WHERE advertise_id = :id;";
        $r = $exec -> exec( $sql, array(
            ':id' => (int)$_POST['id']
        ) );

        $r ? print( '1|Xóa quảng cáo thành công' ) : print( '0|Có lỗi khi cố xóa quảng cáo' );
    }
?>
