{@style}
<section class="module-wrapper">
    <section class="module-toolbar">
        <h3 class="module-heading">Hệ thống / Quảng cáo <small>Cpanel</small></h3>
        <div class="module-button-area">
            <span class="buttons normal-buttons slider-addnew-btn"><i class="fa fa-plus" aria-hidden="true"></i> Thêm</span>
            <span class="buttons disabled-buttons slider-cancel-btn"><i class="fa fa-undo" aria-hidden="true"></i> Hủy</span>
        </div>
    </section>
    <section class="module-content">
        <div class="slider-editor module-addnew">
            <div class="tabs">
                <span class="tab-title">Quảng cáo</span>
                <span class="buttons disabled-buttons insert-new-btn save-btn"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu</span>
                {@addnew-form}
            </div>
        </div>
        <div class="slider-list-wrapper">
            <table class="tables slider-table">
                <col width="20%">
                <col width="20%">
                <col width="15%">
                <col width="15%">
                <col width="10%">
                <col width="10%">
                <col width="10%">
                <tr class="title-rows">
                    <td>Tên quảng cáo</td>
                    <td>Hình ảnh</td>
                    <td>Đường dẫn</td>
                    <td>Nội dung</td>
                    <td>Số click</td>
                    <td>Ngày treo</td>
                    <td>Biên tập</td>
                </tr>
                {@list}
            </table>
        </div>
    </section>
</section>
{@script}
