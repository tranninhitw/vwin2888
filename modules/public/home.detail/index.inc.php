<?php
$exec = new Exec( HOST, USER, PASS, DBNAME );
// hàm In hoa chữ cái đầu dùng cho tiếng việt
function mb_ucfirst($string, $encoding)
{
    $strlen = mb_strlen($string, $encoding);
    $firstChar = mb_substr($string, 0, 1, $encoding);
    $then = mb_substr($string, 1, $strlen - 1, $encoding);
    return mb_strtoupper($firstChar, $encoding) . $then;
}

// lấy chi tiết bài viết hiện tại
$param =$_SERVER['QUERY_STRING'];
$param =explode ( '=' , $param);
$data = array(
    ':post_seo_url' => $param[1],
);

// Update view count
$sql = "UPDATE tp_posts SET post_view_count = (post_view_count + 1) WHERE post_seo_url = :post_seo_url;";
$post = $exec->exec($sql, $data);

// Get post data
$sql = "SELECT * FROM tp_posts WHERE post_seo_url = :post_seo_url";
$post = $exec->get($sql, $data);
$post_thumbnail= json_decode($post[0]['post_thumbnail']);

// SEO
$_SESSION['seo'] = array(
    'title' => $post[0]['post_page_title'],
    'description' => $post[0]['post_meta_description'],
    'keywords' => $post[0]['post_meta_keywords']
);

//  lấy danh mục của bài viết đó
$dataPost = array(
    ':cate_id' => ltrim($post[0]['cate_id'], ',')
);
$sqlPostOfCat = 'SELECT * FROM tp_cates WHERE cate_id LIKE :cate_id';
$posts = $exec -> get($sqlPostOfCat, $dataPost);

// bài viết liên quan
$sql_post_cate = 'SELECT * FROM tp_posts WHERE cate_id = :cate_id and post_id != :post_id';
$dataPost_cate  = array(
    ':cate_id' => $post[0]['cate_id'],
    ':post_id' => $post[0]['post_id']
);
$postOrthers = $exec -> get($sql_post_cate, $dataPost_cate);

$html = "
    <div class='container'>
       <a class='mt-10 color-ash' href='".TP_REL_ROOT."'><i class='mr-5 fa fa-home'></i>Trang chủ /</a>
       <a class='mt-10 color-ash' href='".TP_REL_ROOT."{$posts[0]['cate_seo_url']}'>".mb_ucfirst(mb_strtolower($posts[0]['cate_name'], 'UTF-8'), 'UTF-8')." /</a>
       <a class='mt-10 color-ash' href='{$_SERVER['REQUEST_URI']}'>  ".mb_ucfirst(mb_strtolower($post[0]['post_title'], 'UTF-8'), 'UTF-8')."</a>
    </div>";

$html.= "
<div class='container mt-20'>
    <div class='row'>
        <div class='col-md-8 col-lg-8'>
            <h1 class='mb-20 color-primary' style='font-size: 20px; line-height: 30px; font-weight: 600; text-transform: uppercase;'>{$post[0]['post_title']}</h1>
            {$post[0]['post_contents']}

            <div class='mt-40 mt-sm-20'>


                <ul class='mb-30 list-a-bg-grey list-a-hw-radial-35 list-a-hvr-primary list-li-ml-5'>
                <span style='display: inline-block; font-weight: bold; margin-bottom: 10px;'>Chia sẻ bài viết:</span>
                <div class='sharethis-inline-share-buttons st-left  st-inline-share-buttons st-animated' id='st-1'>

                    <div class='st-btn st-first' data-network='facebook' style='display: inline-block;'>
                        <img src='https://platform-cdn.sharethis.com/img/facebook.svg'>

                    </div>
                    <div class='st-btn' data-network='twitter' style='display: inline-block;'>
                        <img src='https://platform-cdn.sharethis.com/img/twitter.svg'>

                    </div>
                    <div class='st-btn' data-network='pinterest' style='display: inline-block;'>
                        <img src='https://platform-cdn.sharethis.com/img/pinterest.svg'>

                    </div>
                    <div class='st-btn st-last' data-network='messenger' style='display: inline-block;'>
                        <img src='https://platform-cdn.sharethis.com/img/messenger.svg'>

                    </div>
                </div>
                </ul>

            </div><!-- float-left-right -->

            <div class='brdr-ash-1 opacty-5'></div>

            <h4 class='p-title mt-50'><b>Xem thêm:</b></h4>
            <div class='row'>
            ";

            foreach ($postOrthers as $key => $postOrther) {
                // lấy tác giả theo bài viết
                $dataAuthorOfPost = array(
                    ':admin_id' => $postOrther['admin_id'],
                );
                $sqlAuthorOfPost = 'SELECT admin_id, admin_username FROM tp_admins WHERE admin_id = :admin_id';
                $authorOfPost = $exec -> get($sqlAuthorOfPost, $dataAuthorOfPost);

                // lấy comment theo bài viết
                $dataCommentOfPost = array(
                    ':comment_url' => $postOrther['post_seo_url'],
                );
                $sqlCommentOfPost = 'SELECT count(*) as amount FROM tp_comments WHERE comment_url = :comment_url';
                $numberComments = $exec -> get($sqlCommentOfPost, $dataCommentOfPost);

                $html .="
                    <div class='col-sm-6'>
                        <img src='". TP_REL_ROOT . "uploads/public/" . $post_thumbnail[0] ."' alt=''>
                        <h4 class='pt-20'><a href='". TP_REL_ROOT ."{$postOrther['post_seo_url']}'><b>{$postOrther['post_title']}</b></a></h4>
                        <ul class='list-li-mr-20 pt-10 mb-30'>
                        <li class='color-lite-black'><i class='fa fa-user'></i> <a href='#'><b>{$authorOfPost[0]['admin_username']}</b></a>&nbsp;&nbsp;<i class='fa fa-calendar'></i>
                        ".date( 'd/m/Y', $postOrther['post_time'] )."</li>
                            <li><i class='mr-5 font-12 fa fa-comment'></i>{$numberComments[0]['amount']}&nbsp;&nbsp;<i class='mr-5 font-12 fa fa-eye'></i> {$postOrther['post_view_count']}</li>
                        </ul>
                    </div><!-- col-sm-6 -->
                ";
            }
            $html .= "
                </div><!-- row -->
            ";

    echo $html;
 ?>
