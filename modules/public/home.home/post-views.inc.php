<?php
    $exec = new Exec( HOST, USER, PASS, DBNAME );

    $sql= 'SELECT * FROM tp_posts ORDER BY post_id DESC LIMIT 5;';

    $posts = $exec -> get( $sql );

    $html = '<div class="row">';

    for($i = 0; $i < count($posts); $i++) {
        $data = array(
            ':post_seo_url' => $posts[$i]['post_seo_url'],
        );
        $sqlCommentOfPost = 'SELECT COUNT(*) FROM tp_comments WHERE comment_url = :post_seo_url';
        $comments = $exec -> get($sqlCommentOfPost, $data);

        $post_thumbnail= json_decode($posts[$i]['post_thumbnail']);

        $admin='';
        $data1 = array(
            ':admin_id' => $posts[$i]['admin_id'],
        );
        $admin='';
        $sqlAdmin = 'SELECT * FROM tp_admins WHERE admin_id = :admin_id ';
        $admin = $exec -> get($sqlAdmin,$data1);
        if($i == 0) {
            $html .= "<div class='col-sm-6 mb-20'>
                        <img src=' ". TP_REL_ROOT . "uploads/public/" . $post_thumbnail[$i] ." ' alt=''>
                        <h4 class='pt-20'><a href='". TP_REL_ROOT ."{$posts[$i]['post_seo_url']}'><b>".mb_ucfirst(mb_strtolower($posts[$i]['post_title'], 'UTF-8'), 'UTF-8')."</b></a></h4>
                        <ul class='list-li-mr-20 pt-10 pb-20'>
                            <li class='color-lite-black'><i class='fa fa-user'></i> <a href='". TP_REL_ROOT ."{$posts[$i]['post_seo_url']}' class='color-black'><b> {$admin[$i]['admin_username']}</b></a> &nbsp; &nbsp;<i class='fa fa-calendar' aria-hidden='true'></i> &nbsp; ".date( 'd/m/Y', $posts[$i]['post_time'] )."</li>
                            <li><i class='fa fa-comments-o' aria-hidden='true'></i><b> {$comments[$i]['COUNT(*)']}</b>&nbsp; &nbsp; <i class='fa fa-eye' aria-hidden='true'></i><b> {$posts[$i]['post_view_count']}</b></li>
                        </ul>
                        <p>{$posts[$i]['post_compact']}</p>
                    </div>
                    <div class='col-sm-6'>
                ";
        } else {
            $html .= "
                <a class='oflow-hidden pos-relative mb-20 dplay-block' href='". TP_REL_ROOT ."{$posts[$i]['post_seo_url']}'>
                    <div class='wh-100x abs-tlr'><img src='  ". TP_REL_ROOT . "uploads/public/"."$post_thumbnail[0]" ." ' alt=''></div>
                    <div class='ml-120 min-h-100x'>
                        <h5><b>".mb_ucfirst(mb_strtolower($posts[$i]['post_title'], 'UTF-8'), 'UTF-8')."</b></h5>
                        <h6 class='color-lite-black pt-10'><span class=''><b> <i class='fa fa-calendar' aria-hidden='true'></i>  &nbsp;</b></span>".date( 'd/m/Y', $posts[$i]['post_time'] )."</h6>
                    </div>
                </a>
            ";
        }
    }

    echo $html . '    </div></div> <!-- row -->';
?>
