<?php
    // $seriPost['post_view_count']
    $exec = new Exec( HOST, USER, PASS, DBNAME );
    // hàm In hoa chữ cái đầu dùng cho tiếng việt


    function mb_ucfirst($string, $encoding)
    {
        $strlen = mb_strlen($string, $encoding);
        $firstChar = mb_substr($string, 0, 1, $encoding);
        $then = mb_substr($string, 1, $strlen - 1, $encoding);
        return mb_strtoupper($firstChar, $encoding) . $then;
    }


    $data = array(
        ':cate_is_serie' => 'yes',
    );
    $sql = 'SELECT * FROM tp_cates WHERE cate_is_serie=:cate_is_serie ORDER BY cate_id DESC LIMIT 6';
    $series = $exec -> get($sql, $data);
    $html = "
        <div class='container'>
            <div class='h-600x h-sm-auto'>
                <div class='h-2-3 h-sm-auto oflow-hidden'>
    ";


    $canShowCates = array();

    // Get all categories have at least one post
    for($i = 0; $i < count($series); $i++) {

        // Get background image (is newest thumbnail post in this category)
        $sql = "SELECT post_thumbnail FROM tp_posts WHERE cate_id LIKE :id ORDER BY post_time DESC LIMIT 1;";
        $backgroundData = $exec -> get($sql, array(
            ':id' => '%' . $series[$i]['cate_id'] . '%'
        ), true);

        if(count($backgroundData) > 0) {
            $backgroundFileName = json_decode($backgroundData[0]['post_thumbnail']);
            array_push($canShowCates, array(
                'info' => $series[$i],
                'background' => TP_REL_ROOT . 'uploads/public/' . $backgroundFileName[0]
            ));
        }
    }

    // Build html
    $order = 0;
    while($order < count($canShowCates)) {
        switch($order) {
            case 0:
                $html .= "
                    <div class='pb-5 pr-5 pr-sm-0 float-left float-sm-none w-2-3 w-sm-100 h-100 h-sm-300x'>
                        <a class='pos-relative h-100 dplay-block' href='".TP_REL_ROOT."{$canShowCates[$order]['info']['cate_seo_url']}'>
                            <div class='img-bg bg-1 bg-grad-layer-6' style='background-image: url(\"{$canShowCates[$order]['background']}\");'></div>
                            <div class='abs-blr color-white p-20 bg-sm-color-7'>
                                <h3 class='mb-15 mb-sm-5 font-sm-13'><b> ".mb_strtoupper($canShowCates[$order]['info']['cate_name'], 'UTF-8')." </b></h3>
                            </div><!--abs-blr -->
                        </a><!-- pos-relative -->
                    </div><!-- w-1-3 -->
                    <div class='float-left float-sm-none w-1-3 w-sm-100 h-100 h-sm-600x'>
                ";
                break;
            case 1:
                $html .= "
                    <div class='pl-5 pb-5 pl-sm-0 ptb-sm-5 pos-relative h-50'>
                        <a class='pos-relative h-100 dplay-block' href='".TP_REL_ROOT."{$canShowCates[$order]['info']['cate_seo_url']}'>

                            <div class='img-bg bg-2 bg-grad-layer-6' style='background-image: url(\"{$canShowCates[$order]['background']}\");'></div>

                            <div class='abs-blr color-white p-20 bg-sm-color-7'>
                                <h4 class='mb-10 mb-sm-5'><b> {$canShowCates[$order]['info']['cate_name']}</b></h4>

                            </div><!--abs-blr -->
                        </a><!-- pos-relative -->
                    </div><!-- w-1-3 -->
                ";
                break;
            case 2:
                $html .= "
                    <div class='pl-5 ptb-5 pl-sm-0 pos-relative h-50'>
                        <a class='pos-relative h-100 dplay-block' href='".TP_REL_ROOT."{$canShowCates[$order]['info']['cate_seo_url']}'>

                            <div class='img-bg bg-3 bg-grad-layer-6' style='background-image: url(\"{$canShowCates[$order]['background']}\");'></div>

                            <div class='abs-blr color-white p-20 bg-sm-color-7'>
                                <h4 class='mb-10 mb-sm-5'><b> {$canShowCates[$order]['info']['cate_name']}</b></h4>

                            </div><!--abs-blr -->
                        </a><!-- pos-relative -->
                    </div><!-- w-1-3 -->
                    </div><!-- float-left -->
                    </div><!-- h-2-3 -->
                    <div class='h-1-3 oflow-hidden'>
                ";
                break;
            case 3:
            case 4:
            case 5:
                $html .= "
                    <div class='pr-5 pr-sm-0 pt-5 float-left float-sm-none pos-relative w-1-3 w-sm-100 h-100 h-sm-300x'>
                        <a class='pos-relative h-100 dplay-block' href='".TP_REL_ROOT."{$canShowCates[$order]['info']['cate_seo_url']}'>

                            <div class='img-bg bg-4 bg-grad-layer-6' style='background-image: url(\"{$canShowCates[$order]['background']}\");'></div>

                            <div class='abs-blr color-white p-20 bg-sm-color-7'>
                                <h4 class='mb-10 mb-sm-5'><b> {$canShowCates[$order]['info']['cate_name']}</b></h4>

                            </div><!--abs-blr -->
                        </a><!-- pos-relative -->
                    </div><!-- w-1-3 -->
                ";
                break;
        }
        $order++;
    }

    $html .= "
                </div><!-- h-2-3 -->
            </div><!-- h-100vh -->
        </div><!-- container -->
    ";
    echo $html;

?>
