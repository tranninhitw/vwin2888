<?php
    $exec = new Exec( HOST, USER, PASS, DBNAME );
    $data = array(
        'cate_parent' => 0,
    );
    $sql = "SELECT cate_id,cate_name FROM tp_cates WHERE cate_parent = :cate_parent;";
    $posts = $exec -> get($sql,$data);

    $html = '';

    foreach ($posts as $key => $post) {

        $dataPost = array(
            ':cate_id' => ','.$post['cate_id'],
        );

        $sqlPost = "SELECT * FROM tp_posts WHERE cate_id = :cate_id LIMIT 4 ;";
        $postOfCate = $exec->get($sqlPost, $dataPost);

        if( count( $postOfCate ) > 0 ) {

            $html .="
                <h4 class='p-title mt-30'><b>{$posts[$key]['cate_name']}</b></h4>
                <div class='row'>";

                foreach ($postOfCate as $key1 => $value) {
                    $post_thumbnail= json_decode($value['post_thumbnail']);

                    // lấy tác giả theo bài viết
                    $dataAuthorOfPost = array(
                        ':admin_id' => $value['admin_id'],
                    );
                    $sqlAuthorOfPost = 'SELECT admin_id, admin_username FROM tp_admins WHERE admin_id = :admin_id';
                    $authorOfPost = $exec -> get($sqlAuthorOfPost, $dataAuthorOfPost);

                    // lấy comment theo bài viết
                    $dataCommentOfPost = array(
                        ':comment_url' => $value['post_seo_url'],
                    );
                    $sqlCommentOfPost = 'SELECT count(*) FROM tp_comments WHERE comment_url = :comment_url';
                    $numberComments = $exec -> get($sqlCommentOfPost, $dataCommentOfPost);

                    $html.="
                        <div class='col-sm-6'>
                            <img src='". TP_REL_ROOT . "uploads/public/" . $post_thumbnail[0] ."' alt=''>
                            <h4 class='pt-20'><a href='". TP_REL_ROOT ."{$value['post_seo_url']}'><b>".mb_ucfirst(mb_strtolower($value['post_title'], 'UTF-8'), 'UTF-8')."</b></a></h4>
                            <ul class='list-li-mr-20 pt-10 mb-30'>
                                <li class='color-lite-black'> <i class='fa fa-user'></i> <a href='' class='color-black'><b>{$authorOfPost[0]['admin_username']}</b></a>
                                &nbsp; &nbsp;<i class='fa fa-calendar' aria-hidden='true'></i> &nbsp; ".date( ' d/m/Y', $value['post_time'] )."</li>
                                <li><i class='fa fa-comments-o'></i> {$numberComments[0]['count(*)']}&nbsp; &nbsp;<i class='fa fa-eye'></i> {$value['post_view_count']}</li>
                            </ul>
                            </div><!-- col-sm-6 -->
                    ";
                }
                $html.="
                    </div><!-- row -->
                ";
            }
        }
    echo $html;
?>
