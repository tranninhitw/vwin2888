<?php
    $exec = new Exec( HOST, USER, PASS, DBNAME );

    $sql =  "SELECT * FROM tp_settings;";
    $settings = $exec -> get( $sql );
    foreach( $settings as $key => $value ) {

        if( $value['setting_name'] == 'default_avatar' ) {
            $default_avatar = $value['setting_value'];
        }
        if( $value['setting_name'] == 'company_information' ) {
            $company_information = $value['setting_value'];
        }

    }

    $company_information = json_decode( $company_information, true );
    $avatar = json_decode( $default_avatar, true );

    foreach($company_information as $key => $value) {
        if(is_array(json_decode($value, true))) {
            $url = json_decode($value, 0);
            $company_information[$key] = $url[0];
        }
    }

    $html ='

        <div class="col-12">
            <div class="mb-30">
                <a href="#"><img src="' . TP_REL_ROOT . 'uploads/public/' . $company_information['footer_logo'] . '"></a>
                <p class="mtb-20 color-ccc">' . $company_information['footer_content'] . '.</p>
                <p class="color-ash">Copyright &copy; 2019 VWin2888</p>
            </div><!-- mb-30 -->
        </div><!-- col-12 -->

    ';
    echo $html;
?>
