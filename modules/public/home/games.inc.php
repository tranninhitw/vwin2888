<?php
    $exec = new Exec( HOST, USER, PASS, DBNAME );

    $sql =  "SELECT * FROM tp_settings;";
    $settings = $exec -> get( $sql );
    foreach( $settings as $key => $value ) {

        if( $value['setting_name'] == 'default_avatar' ) {
            $default_avatar = $value['setting_value'];
        }
        if( $value['setting_name'] == 'company_information' ) {
            $company_information = $value['setting_value'];
        }

    }

    $company_information = json_decode( $company_information, true );
    $avatar = json_decode( $default_avatar, true );

    foreach($company_information as $key => $value) {
        if(is_array(json_decode($value, true))) {
            $url = json_decode($value, 0);
            $company_information[$key] = $url[0];
        }
    }

    $html ='
        <a class="oflow-hidden pos-relative mb-20 dplay-block" href="' . TP_REF_LINK . '" >
            <div class="wh-100x abs-tlr"><img src="' . TP_REL_ROOT . 'uploads/public/' . $company_information['game_1'] . '" alt=""></div>
            <div class="ml-120 min-h-100x">
                <h5><b>LÔ ĐỀ ONLINE</b></h5>
                <button type="button" class="btn btn-sm btn-warning mt-20">Chơi ngay</button>
            </div>
        </a><!-- oflow-hidden -->

        <a class="oflow-hidden pos-relative mb-20 dplay-block" href="' . TP_REF_LINK . '">
            <div class="wh-100x abs-tlr"><img src="' . TP_REL_ROOT . 'uploads/public/' . $company_information['game_2'] . '" alt=""></div>
            <div class="ml-120 min-h-100x">
                <h5><b>CÁ CƯỢC THỂ THAO</b></h5>
                <button type="button" class="btn btn-sm btn-warning mt-20">Chơi ngay</button>
            </div>
        </a><!-- oflow-hidden -->

        <a class="oflow-hidden pos-relative mb-20 dplay-block" href="' . TP_REF_LINK . '">
            <div class="wh-100x abs-tlr"><img src="' . TP_REL_ROOT . 'uploads/public/' . $company_information['game_3'] . '" alt=""></div>
            <div class="ml-120 min-h-100x">
                <h5><b>ĐÁNH BÀI ONLINE</b></h5>
                <button type="button" class="btn btn-sm btn-warning mt-20">Chơi ngay</button>

            </div>
        </a><!-- oflow-hidden -->
    ';
    echo $html;
?>
