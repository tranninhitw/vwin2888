<?php
    $exec = new Exec( HOST, USER, PASS, DBNAME );
    $sql = "SELECT * FROM tp_menu WHERE menu_hidden = 0 ORDER BY menu_order DESC ;";

    $cates = $exec -> get( $sql );

    $cates = find_children( $cates );

    $html = "<ul class='main-menu' id='main-menu'>";

    function find_children( $cates, $parentId = 0 ) {
        // $html = '';
        $tree = array();
        foreach( $cates as $key => $cate ) {
            if( $cate['menu_parent_id'] == $parentId ) {
                array_push( $tree, array(
                    'cate_info' => $cate,
                    'cate_children' => find_children( $cates, $cate['menu_id'] )
                ) );
            }
        }
        return $tree;
    }

    foreach ($cates as $key => $cat) {

        $sql = 'SELECT * FROM tp_menu ORDER BY menu_id DESC ;';
        $series = $exec -> get($sql);


        if(count($cat['cate_children']) > 0) {
            $caret = "<i class='fa fa-caret-down'></i>";
            $html .="
                <li class='drop-down'><a href='".TP_REL_ROOT."{$cat['cate_info']['menu_link']}'>{$cat['cate_info']['menu_text']}$caret</a>
                    <ul class='drop-down-menu drop-down-inner'>
            ";
            foreach($cat['cate_children'] as $key => $child) {
                $html .= "<li><a href='".TP_REL_ROOT."{$child['cate_info']['menu_link']}'>{$child['cate_info']['menu_text']}</a></li>";
            }
            $html .= "</ul>";
        } else {
            $html .= "<li class='drop-down'><a href='".TP_REL_ROOT."{$cat['cate_info']['menu_link']}'>{$cat['cate_info']['menu_text']}</a>";
        }

        $html .= '</li>';
    }

    echo $html . '</ul>';

?>
