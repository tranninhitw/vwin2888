</div><!-- row -->
</div><!-- container -->
</section>

<footer class="bg-191 color-ccc">

    <div class="container">
        <div class="pt-50 pb-20 pos-relative text-center">
            <div class="abs-tblr pt-50 z--1 text-center">
                <div class="h-80 pos-relative"><img class="opacty-1 h-100 w-auto" src="assets/images/map.png" alt=""></div>
            </div>
            <div class="row">

                {@logofooter}

            </div><!-- row -->
        </div><!-- ptb-50 -->

        <div class="brdr-ash-1 opacty-2"></div>

    </div><!-- container -->
</footer>
