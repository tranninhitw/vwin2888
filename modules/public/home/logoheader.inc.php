<?php
    $exec = new Exec( HOST, USER, PASS, DBNAME );

    $sql =  "SELECT * FROM tp_settings;";
    $settings = $exec -> get( $sql );
    foreach( $settings as $key => $value ) {

        if( $value['setting_name'] == 'default_avatar' ) {
            $default_avatar = $value['setting_value'];
        }
        if( $value['setting_name'] == 'company_information' ) {
            $company_information = $value['setting_value'];
        }

    }

    $company_information = json_decode( $company_information, true );
    $avatar = json_decode( $default_avatar, true );

    foreach($company_information as $key => $value) {
        if(is_array(json_decode($value, true))) {
            $url = json_decode($value, 0);
            $company_information[$key] = $url[0];
        }
    }

    $html ='<a class="logo" href="'.TP_REL_ROOT.'"><img src="' . TP_REL_ROOT . 'uploads/public/' . $company_information['main_logo'] . '" alt="Logo"></a>';
    echo $html;
?>
