<div class="d-none d-sm-block d-md-none col-md-3"></div>
<div class="col-md-4 col-lg-4">
    <div class="pl-20 pl-md-0">
        <div class="">
            <h4 class="p-title"><b>Chia sẻ</b></h4>
            <div class="sharethis-inline-share-buttons"></div>
        </div><!-- mtb-50 -->

        <div class="mtb-50 mb-md-0">
            <h4 class="p-title"><b>Nhận tin khuyến mãi</b></h4>
            <p class="mb-20">Nhận tin khuyến mãi mỗi tuần qua email</p>
            {@sendmail}
            <form class="nwsltr-primary-1" method="post" action="">
                <input type="text" name="email" placeholder="Nhập email"/>
                <button type="submit" name="submit" ><i class="fa fa-send-o"></i></button>
            </form>
        </div><!-- mtb-50 -->

        <div class="mtb-50">
            <h4 class="p-title"><b>Chơi game nhận thưởng</b></h4>
            {@games}
        </div><!-- mtb-50 -->

        {@ads}
    </div><!--  pl-20 -->
</div><!-- col-md-3 -->
