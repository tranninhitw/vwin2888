<header>
    <div class="container">
        {@logoheader}
        <a class="menu-nav-icon" data-menu="#main-menu" href="#">
            <i class="fa fa-bars"></i>
        </a>
        {@menu}
        <div class="clearfix"></div>
    </div><!-- container -->
</header>
