<?php
    if(isset( $_POST['submit'] )){

        $exec = new Exec( HOST, USER, PASS, DBNAME );

        // Validate
        if(empty($_POST['email'])) {
            echo '<p class="alert alert-danger">Vui lòng điền địa chỉ email của bạn</p>';
            return;
        }

        if(!validateEmail($_POST['email'])) {
            echo '<p class="alert alert-danger">Địa chỉ email không hợp lệ</p>';
            return;
        }

        $data = array(
            ':email' => $_POST['email']
        );
        $sql = "INSERT INTO tp_promotion_email (mail_name) VALUES (:email);";

        $exec -> add( $sql, $data );

        echo '<p class="alert alert-success">Cám ơn bạn đã đăng ký nhận tin</p>';
    }

    function validateEmail($email) {
        $pattern = '/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/';
        return preg_match($pattern, strtolower($email)) === 1;
    }
?>
