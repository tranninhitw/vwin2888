<?php
    $exec = new Exec( HOST, USER, PASS, DBNAME );

    $html = '';
    $sql = "SELECT * FROM tp_advertises;";
    $ads = $exec -> get( $sql );

    foreach( $ads as $key => $ad ) {
        $image = json_decode($ad['advertise_image'], true);
        $html .= '
            <div class="mtb-50 pos-relative" title="' . $ad['advertise_title'] . '">
                <a href="' . $ad['advertise_link'] . '" target="' . $ad['advertise_target'] . '">
                    <img src="' . TP_REL_ROOT . 'uploads/public/' . $image[0] . '" alt="' . $ad['advertise_title'] . '">
                    <div class="abs-tblr text-center color-white">
                        <div class="dplay-tbl">
                            <div class="dplay-tbl-cell">
                                <h4><b>' . $ad['advertise_name'] . '</b></h4>
                            </div><!-- dplay-tbl-cell -->
                        </div><!-- dplay-tbl -->
                    </div><!-- abs-tblr -->
                </a>
            </div><!-- mtb-50 -->
        ';
    }
    echo $html;
?>
