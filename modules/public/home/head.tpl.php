<!DOCTYPE HTML>
<html lang="en">
<head>
    {#plu_seo}
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">
    {@base}

    <!-- Stylesheets -->

    <link href="{@www}/vendors/css/bootstrap.css" rel="stylesheet">
    <link href="{@www}/vendors/css/font-awesome.min.css" rel="stylesheet">

    <link href="assets/css/style.css" rel="stylesheet">

    <script src="{@www}/vendors/js/jquery-3.0.0.min.js"></script>
</head>
<body>
