<?php
    $exec = new Exec( HOST, USER, PASS, DBNAME );

    $sql =  "SELECT * FROM tp_settings WHERE setting_name = :name;";
    $settings = $exec -> get( $sql, array(
        ':name' => 'company_information'
    ) );
    $info = json_decode($settings[0]['setting_value'], true);
    $code = $info['ga_code'];

    $html = "
        <script async src=\"https://www.googletagmanager.com/gtag/js?id=$code\"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', '$code');
        </script>
    ";

    echo $html;
?>
