<?php
    $exec = new Exec( HOST, USER, PASS, DBNAME );
    // hàm In hoa chữ cái đầu dùng cho tiếng việt
    function mb_ucfirst($string, $encoding)
    {
        $strlen = mb_strlen($string, $encoding);
        $firstChar = mb_substr($string, 0, 1, $encoding);
        $then = mb_substr($string, 1, $strlen - 1, $encoding);
        return mb_strtoupper($firstChar, $encoding) . $then;
    }

    // lấy chi tiết danh mục hiện tại
    $data = array(
        ':cate_seo_url' => $_GET['params'],
    );
    $sql = "SELECT cate_id, cate_name, cate_seo_url FROM tp_cates WHERE cate_seo_url = :cate_seo_url";
    $singlePost = $exec->get($sql, $data);

    // bài viết theo danh mục
    $page = isset($_GET['p']) ? (int)$_GET['p'] - 1 : 0;
    $dataPost = array(
        ':cate_id' => ','.$singlePost[0]['cate_id'],
        ':post' => 10,
        ':page' => $page
    );
    $sqlPostOfCat = 'SELECT * FROM tp_posts WHERE cate_id = :cate_id LIMIT :post OFFSET :page;';
    $posts = $exec -> get($sqlPostOfCat, $dataPost, true);

    // danh mục series liên quan
    $dataCat = array(
        ':cate_seo_url' => $_GET['params'],
    );
    $sqlCat = "SELECT cate_id, cate_name, cate_seo_url FROM tp_cates WHERE cate_seo_url != :cate_seo_url";
    $catOrthers = $exec->get($sqlCat, $dataCat);

    $html= "
        <div class='container'>
           <a class='mt-10 color-ash' href='".TP_REL_ROOT."'><i class='mr-5 fa fa-home'></i>Trang chủ /</a>
           <a class='mt-10 color-ash' href='{$_SERVER['REQUEST_URI']}'> ".mb_ucfirst(mb_strtolower($singlePost[0]['cate_name'], 'UTF-8'), 'UTF-8')."</a>
        </div>
        <div class='container mt-20'>
            <div class='row'>

                <div class='col-md-12 col-lg-8'>
                    <h4 class='p-title'><b>DANH MỤC : {$singlePost[0]['cate_name']}</b></h4>
                    <div class='row'>
    ";



        if (count($posts) > 0) {
            foreach ($posts as $key => $post) {
                $post_thumbnail= json_decode($posts[$key]['post_thumbnail']);

                // lấy comment theo bài viết
                $dataCommentOfPost = array(
                    ':comment_url' => $post['post_seo_url'],
                );
                $sqlCommentOfPost = 'SELECT count(*) FROM tp_comments WHERE comment_url = :comment_url';
                $numberComments = $exec -> get($sqlCommentOfPost, $dataCommentOfPost);

                foreach ($numberComments as $key => $numberComment) {
                    // lấy tác giả theo bài viết
                    $dataAuthorOfPost = array(
                        ':admin_id' => $post['admin_id'],
                    );
                    $sqlAuthorOfPost = 'SELECT admin_id, admin_username FROM tp_admins WHERE admin_id = :admin_id';
                    $authorOfPost = $exec -> get($sqlAuthorOfPost, $dataAuthorOfPost);
                    $html .= "
         						<div class='col-sm-6 mb-20'>
         							<a href='".TP_REL_ROOT."{$post['post_seo_url']}' class='text-center' style='display: block;'>
                                        <img src='".TP_REL_ROOT."/uploads/public/$post_thumbnail[0]' alt='{$post['post_title']}'>
                                    </a>
         							<h4 class='pt-20'><a href='".TP_REL_ROOT."{$post['post_seo_url']}'><b> {$post['post_title']}</b></a></h4>
         							<ul class='list-li-mr-20 pt-10 mb-30'>
         								<li><i class='fa fa-user'></i> <a href='#'><b>{$authorOfPost[$key]['admin_username']}</b></a>&nbsp;&nbsp; <i class='fa fa-calendar'></i>
         								".date( 'd/m/Y', $post['post_time'] )."</li>
         								<li><i class='mr-5 font-12 fa fa-comment'></i>{$numberComment['count(*)']} &nbsp; &nbsp; <i class='mr-5 font-12 fa fa-eye'></i> {$post['post_view_count']}</li>
         							</ul>
                                    <p>{$post['post_compact']}</p>
         						</div><!-- col-sm-6 -->

                    ";
                }
            }
        } else {
            $html .= "
                <div class='col-sm-12'>
                    <h4>Hiện chưa có bài viết cho danh mục này<h4>
                </div>
            ";
        }

    $html .= "
        </div>
        <hr />
        <div class='row'>
        <div class='col-12 mt-50 mb-20'>
            <div class='panel-group' id='accordion'>
                <div class='panel panel-default'>
                    <div class='panel-heading'>
                        <h4 class='panel-title'>
                            <a data-toggle='collapse' data-parent='#accordion' href='#collapseOne'>
                                LOẠT BÀI NÊN ĐỌC
                            </a>
                        </h4>
                    </div>
                    <div id='collapseOne' class='panel-collapse collapse show'>
                        <div class='panel-body'>
                            <table class='table'>";

foreach ($catOrthers as $key => $catOrther) {

    $html .= "
                                        <tr>
                                            <td>
                                                <a href='".TP_REL_ROOT."{$catOrther['cate_seo_url']}'>".mb_strtoupper($catOrther['cate_name'], 'UTF-8')."</a>
                                            </td>
                                        </tr>
                                    ";
}

$html .= "
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    ";

    echo $html;

 ?>
