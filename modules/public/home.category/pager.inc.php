<?php
    $exec = new Exec( HOST, USER, PASS, DBNAME );

    $sql = "SELECT cate_id, cate_seo_url FROM tp_cates WHERE cate_seo_url = :url;";
    $cate = $exec -> get($sql, array(
        ':url' => strip_tags($_GET['params'])
    ));

    $sql = "SELECT COUNT(*) AS post_amount FROM tp_posts WHERE cate_id LIKE :cate_id;";
    $amount = $exec -> get($sql, array(
        ':cate_id' => '%' . $cate[0]['cate_id'] . '%'
    ));

    $postPerPage = 10;
    $pages = ceil($amount[0]['post_amount'] / $postPerPage);
    if ($pages > $postPerPage ) {
        $html = '';
        for($page = 0; $page < $pages; $page++) {
            switch($page) {
                case 0:
                    $html .= '<li class="page-item"><a class="page-link color-primary" href="' . TP_REL_ROOT . strip_tags($_GET['params']) . '?p=1">Trang đầu</a></li>';
                    break;
                case $pages - 1:
                    $html .= '<li class="page-item"><a class="page-link color-primary" href="' . TP_REL_ROOT . strip_tags($_GET['params']) . '?p=' . $pages . '">Trang cuối</a></li>';
                    break;
                default:
                    $html .= '
                        <li class="page-item"><a class="page-link color-primary" href="' . TP_REL_ROOT . strip_tags($_GET['params']) . '?p=' . ($page + 1) . '">' . ($page + 1) . '</a></li>
                    ';
                    break;
            }
        }
        echo $html;
    }

?>
