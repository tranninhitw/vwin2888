<?php
    /**
     * TAKA+ v1.0
     * @package plugin
     * @date 12.1.2016
     * @version 0.0.1
     * @website
     *
     * TẠO MỘT PLUGIN THÌ VIỆC ĐẦU TIÊN LÀ ...
     * ĐĂNG KÝ TÊN TUỔI CỦA NÓ BÊN DƯỚI
     *
    */
    $plugin = new Plugin();


    /**
     * Khai báo tên các plugin bên dưới
    */

    //Comment
    $plugin -> register_plugin( 'Comment plugin', 'comment', array(
        'plu_comment',
    ) );

    //Visitor
    $plugin -> register_plugin( 'SEO plugin', 'seo', array(
        'plu_seo',
    ) );


    /**
     * Kết thúc phần plugin
     *
     * THINK LESS. CODE MORE.
    */
?>
